Vue.component("v-pedidos", {
    template:/*html*/ `
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12 text-center">
            <h3>Pedidos y detalle</h3>
        </div>
        <div class="col-12 col-md-12 col-lg-12 mb-2">
            <div class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" v-model="buscar" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit" @click="getBuscar()">Search</button>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">
                            #
                            <!--<button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#addPro" 
                            @click="getM(),getC(),add=0, d1 = 'Elegir...', d2= 'Elegir...', d3='',
                             d4='', d5 = '', d6 = '',d7 ='', d8='Elegir...', d9=''">
                                +
                            </button>-->
                        </th>
                        <th scope="col">Cliente</th>
                        <th scope="col">Precio Total</th>
                        <th scope="col">Fecha</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Detalle</th>
                        <th scope="col">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(datos,index) in filtros" :key="index">
                        <th scope="row">
                            {{datos.id}}
                        </th>
                        <td>
                            {{ datos.nombre}}
                        </td>
                        <td>
                            {{datos.precio}}
                        </td>
                        <td>
                            {{datos.fecha}}
                        </td>
                        <td>
                            {{datos.estado}}
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-primary" 
                                data-toggle="modal" data-target="#ModDetalle"
                                @click="getDetalle(datos.id)">
                                <i class="fas fa-eye"></i>
                            </button>
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#eliminarPedi"
                                @click="id=datos.id">
                                <i class="fas fa-trash-alt"></i>
                            </button>

                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="alert alert-light" role="alert" v-if="pedidox_exit==false">
            No se ha encontrado ningun resutado de: {{buscar_none}}
        </div>
        <!--
            Modal de modificar proveedor
        -->
        <div class="modal fade" id="ModDetalle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="add=false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-md-12 col-lg-12 mb-2">
                                    <div class="form-inline my-2 my-lg-0">
                                        <input class="form-control mr-sm-2" type="search" placeholder="Search" v-model="buscar2" aria-label="Search">
                                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit" @click="getBuscar2()">Search</button>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">
                                                    #
                                                    <!--<button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#addPro" 
                                                    @click="getM(),getC(),add=0, d1 = 'Elegir...', d2= 'Elegir...', d3='',
                                                    d4='', d5 = '', d6 = '',d7 ='', d8='Elegir...', d9=''">
                                                        +
                                                    </button>-->
                                                </th>
                                                <th scope="col">Producto</th>
                                                <th scope="col">Cantidad</th>
                                                <th scope="col">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(datos,index) in filtros2" :key="index">
                                                <th scope="row">
                                                    {{datos.id}}
                                                </th>
                                                <td>
                                                    {{ datos.nombre}}
                                                </td>
                                                <td>
                                                    {{datos.cantidad}}
                                                </td>
                                                <td>
                                                    {{datos.total}}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="alert alert-light" role="alert" v-if="pedidox_exit2==false">
                                    No se ha encontrado ningun resutado de: {{buscar_none2}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="add=false">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal para eliminar el proveedor-->
        <div class="modal fade" id="eliminarPedi" tabindex="-1" role="dialog" aria-labelledby="eliminarPediLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="eliminarPedi">¿Quieres Eliminar?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-success" role="alert" v-if="add==1">
                            {{error}}
                        </div>
                        <div class="alert alert-danger" role="alert" v-if="add==2">
                            {{error}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="add=0, can=0">Cancelar</button>
                        <button type="button" class="btn btn-danger" @click="eliUsu()" v-if="can==0">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `,data() {
        return {
            //Datos de modelo
            id: 0,
            d1:'',
            d2:'',
            d3: '',
            d4:'',
            marca: [],
            categoriax: [],
            add: 0,
            error: '',
            can: 0,
            //Busqueda
            buscar: '',
            pedidox_exit: true,
            buscar_none: '',
            buscar2: '',
            pedidox_exit2: true,
            buscar_none2: '',
            //Datos de la base de datos
            pedidox: [],
            filtros: [],
            pedidox2: [],
            filtros2: [],
            //fecha
            ano: new Date().getFullYear(),
            mes: new Date().getMonth(),
            day: new Date().getDate(),
        }
    },
    mounted() {
        this.getCli();
    },
    methods:{
        getC(){
            axios.post('./../../core/controllers/php/dashboard.php?tabla=cliente&action=leer')
                .then(res => {
                    console.log(res)
                    this.categoriax = res.data.cliente
                })
        },
        getBuscar() {
            var comp = this.buscar;
            this.filtros = this.pedidox.filter(function (elem) {
                let el1 = elem.nombre.toLowerCase();
                if (comp === '') return true;
                else return el1.indexOf(comp.toLowerCase()) > -1;

            })
            if (this.filtros.length <= 0) {
                this.buscar_none = this.buscar
                this.pedidox_exit = false;
            }
            else {
                this.pedidox_exit = true
            }
            this.buscar = ''
        },
        getCli() {
            axios.post('./../../core/controllers/php/dashboard.php?tabla=pedido&action=leer')
                .then(res => {
                    console.log(res)
                    this.pedidox = res.data.pedidos
                    this.filtros = this.pedidox
                })
        },
        getBuscar2() {
            var comp = this.buscar2;
            this.filtros2 = this.pedidox2.filter(function (elem) {
                let el1 = elem.nombre.toLowerCase();
                if (comp === '') return true;
                else return el1.indexOf(comp.toLowerCase()) > -1;

            })
            if (this.filtros2.length <= 0) {
                this.buscar_none2 = this.buscar
                this.pedidox_exit2 = false;
            }
            else {
                this.pedidox_exit2 = true
            }
            this.buscar2 = ''
        },
        getDetalle(id) {
            axios.post('./../../core/controllers/php/dashboard.php?tabla=detalle&action=leer&id='+id)
                .then(res => {
                    console.log(res)
                    this.pedidox2 = res.data.dpedidos
                    this.filtros2 = this.pedidox2
                })
        },
        /*
        addCli(e){
            axios.post('./../../core/controllers/php/dashboard.php?tabla=pedido&action=agregar', new FormData(e.target))
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.can = 1;
                    this.add = 1;
                    this.getCli();
                }
                else if(vali[0].val == 1){
                    this.add = 2;
                    this.error = "Error al ingresar, verificar correo y/o telefono"
                }
                else if(vali[0].val == 2){
                    this.add = 2;
                    this.error = vali[0].mensaje;
                }
            })
        },
        modUsu(e){
            axios.post('./../../core/controllers/php/dashboard.php?tabla=pedido&action=modificar&id='+this.id, new FormData(e.target))
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.add = 1;
                    this.getCli();
                }
                else if(vali[0].val == 1){
                    this.add = 2;
                    this.error = "Error al ingresar, verificar correo y/o telefono"
                }
                else if(vali[0].val == 2){
                    this.add = 2;
                    this.error = vali[0].mensaje
                }
            })
        },
        */
        eliUsu(){
            console.log(this.id);
            axios.post('./../../core/controllers/php/dashboard.php?tabla=pedido&action=borrar&id='+this.id)
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.add = 1;
                    this.error = vali[0].mensaje
                    this.can = 1;
                    this.getCli();
                }
                if(vali[0].val == 1)
                {
                    this.add = 2;
                    this.error = vali[0].mensaje
                    this.can = 1;
                    this.getCli();
                }
            })
        }
    }
})
              
//Pedidos y detalles
var pyd = new Vue({
    el: '#pyd',
});