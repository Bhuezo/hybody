Vue.component("v-productos", {
    template:/*html*/ `
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12 text-center">
            <h3>Productos</h3>
        </div>
        <div class="col-12 col-md-12 col-lg-12 mb-2">
            <div class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" v-model="buscar" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit" @click="getBuscar()">Search</button>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">
                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#addPro" 
                            @click="getM(),getC(),add=0, d1 = 'Elegir...', d2= 'Elegir...', d3='',
                             d4='', d5 = '', d6 = '',d7 ='', d8='Elegir...', d9=''">
                                +
                            </button>
                        </th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Categoria</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Imagen</th>
                        <th scope="col">Modificar</th>
                        <th scope="col">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(datos,index) in filtros" :key="index">
                        <th scope="row">
                            {{datos.id}}
                        </th>
                        <td>
                            {{ datos.producto}}
                        </td>
                        <td>
                            {{datos.marca}}
                        </td>
                        <td>
                            {{datos.categoria}}
                        </td>
                        <td>
                            {{datos.cantidad}}
                        </td>
                        <td>
                            {{datos.precio}}
                        </td>
                        <td>
                            <img :src="datos.img" width="100" height="100">
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-primary" 
                                data-toggle="modal" data-target="#ModPro"
                                @click="getM(),getC(),add=0,id=datos.id, d1 = datos.ida, d1n = datos.categoria, d2= datos.idm, d2n=datos.marca,
                                d3=datos.producto, d4=datos.descripcion, d5 = datos.precio, d6 = datos.img, d7 = datos.cantidad,
                                d8=datos.estado, d9=datos.ganancias">
                                <i class="fas fa-pen"></i>
                            </button>
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#eliminarPro"
                                @click="id=datos.id">
                                <i class="fas fa-trash-alt"></i>
                            </button>

                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="alert alert-light" role="alert" v-if="productox_exit==false">
            No se ha encontrado ningun resutado de: {{buscar_none}}
        </div>
        <!--
            Modal de modificar proveedor
        -->
        <div class="modal fade" id="ModPro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="add=false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="text-center" @submit.prevent="modUsu">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="nombre" placeholder="Nombre..." v-model="d3" required/>
                            <label>Descripcion</label>
                            <input type="text" class="form-control" name="descp" placeholder="Descripcion..." v-model="d4" required/>
                            
                            <label>Img</label>
                            <img :src="d6" width="100" height="100">
                            <input type="file" class="form-control" name="img">
                            <label>Cantidad</label>
                            <input type="number" class="form-control" name="cantidad" placeholder="Nombre..." v-model="d7" required/>
                            <label>Precio</label>
                            <input type="text" class="form-control" name="precio" placeholder="Descripcion..." v-model="d5" required/>
                            
                            <label>Ganancias</label>
                            <input type="number" class="form-control" name="ganancia" placeholder="Correo..." v-model="d9" required/>
                            
                            <label>Marca</label>
                            <select class="form-control" name="marca" required>
                                <option selected>{{ d2 }} -{{d2n}}</option>
                                <option v-for="(combo, index) in marca" :key="index">
                                    {{combo.id}} -{{combo.nombre}}
                                </option>
                            </select>
                            <label>Categoria</label>
                            <select class="form-control" name="categoria" required>
                                <option selected>{{ d1 }} -{{d1n}}</option>
                                <option v-for="(combo, index) in categoriax" :key="index">
                                    {{combo.id}} -{{combo.nombre}}
                                </option>
                            </select>
                            <label>Estado</label>
                            <select class="form-control" name="estado" required>
                                <option selected>{{ d8 }}</option>
                                <option>1</option>
                                <option>2</option>
                            </select>
                            <div class="p-2">
                                <button type="submit" class="btn btn-info">Agregar</button>
                            </div>
                            <div class="alert alert-success" v-if="add==1" role="alert">
                                Se ha modificado con exito!!
                            </div>
                            <div class="alert alert-danger" v-if="add==2" role="alert">
                                {{error}}
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="add=false">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--
            Modal de Agregar nuevo proveedor
        -->
        <div class="modal fade" id="addPro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="add=false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="text-center" @submit.prevent="addCli">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="nombre" placeholder="Nombre..." v-model="d3" required/>
                            <label>Descripcion</label>
                            <input type="text" class="form-control" name="descp" placeholder="Descripcion..." v-model="d4" required/>
                            
                            <label>Img</label>
                            <img :src="d6" width="100" height="100">
                            <input type="file" class="form-control" name="img">
                            <label>Cantidad</label>
                            <input type="number" class="form-control" name="cantidad" placeholder="Nombre..." v-model="d7" required/>
                            <label>Precio</label>
                            <input type="text" class="form-control" name="precio" placeholder="Descripcion..." v-model="d5" required/>
                            
                            <label>Ganancias</label>
                            <input type="number" class="form-control" name="ganancia" placeholder="Correo..." v-model="d9" required/>
                            
                            <label>Marca</label>
                            <select class="form-control" name="marca" required>
                                <option selected>{{ d2 }} -{{d2n}}</option>
                                <option v-for="(combo, index) in marca" :key="index">
                                    {{combo.id}} -{{combo.nombre}}
                                </option>
                            </select>
                            <label>Categoria</label>
                            <select class="form-control" name="categoria" required>
                                <option selected>{{ d1 }} -{{d1n}}</option>
                                <option v-for="(combo, index) in categoriax" :key="index">
                                    {{combo.id}} -{{combo.nombre}}
                                </option>
                            </select>
                            <label>Estado</label>
                            <select class="form-control" name="estado" required>
                                <option selected>{{ d8 }}</option>
                                <option>1</option>
                                <option>2</option>
                            </select>
                            <div class="p-2">
                                <button type="submit" class="btn btn-info" v-if="can==0">Agregar</button>
                            </div>
                            <div class="alert alert-success" v-if="add==1" role="alert">
                                Se ha agregado exitosamente!!
                            </div>
                            <div class="alert alert-danger" v-if="add==2" role="alert">
                                {{error}}
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="add=0, can=0">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal para eliminar el proveedor-->
        <div class="modal fade" id="eliminarPro" tabindex="-1" role="dialog" aria-labelledby="eliminarProLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="eliminarPro">¿Quieres Eliminar?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-success" role="alert" v-if="add==1">
                            {{error}}
                        </div>
                        <div class="alert alert-danger" role="alert" v-if="add==2">
                            {{error}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="add=0, can=0">Cancelar</button>
                        <button type="button" class="btn btn-danger" @click="eliUsu()" v-if="can==0">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `,data() {
        return {
            //Datos de modelo
            id: 0,
            d1:'',
            d1n:'',
            d2:'',
            d2n:'',
            d3: '',
            d4:'',
            d5:'',
            d6: '',
            d7: '',
            d8: '',
            d9:'',
            marca: [],
            categoriax: [],
            add: 0,
            error: '',
            can: 0,
            //Busqueda
            buscar: '',
            productox_exit: true,
            buscar_none: '',
            //Datos de la base de datos
            productox: [],
            filtros: [],
            //fecha
            ano: new Date().getFullYear(),
            mes: new Date().getMonth(),
            day: new Date().getDate(),
        }
    },
    mounted() {
        this.getCli();
    },
    methods:{
        getM(){
            axios.post('./../../core/controllers/php/dashboard.php?tabla=marca&action=leer')
                .then(res => {
                    console.log(res)
                    this.marca = res.data.marca
                })
        },
        getC(){
            axios.post('./../../core/controllers/php/dashboard.php?tabla=categoria&action=leer')
                .then(res => {
                    console.log(res)
                    this.categoriax = res.data.categoria
                })
        },
        getBuscar() {
            var comp = this.buscar;
            this.filtros = this.productox.filter(function (elem) {
                let el1 = elem.producto.toLowerCase();
                if (comp === '') return true;
                else return el1.indexOf(comp.toLowerCase()) > -1;

            })
            if (this.filtros.length <= 0) {
                this.buscar_none = this.buscar
                this.productox_exit = false;
            }
            else {
                this.productox_exit = true
            }
            this.buscar = ''
        },
        getCli() {
            axios.post('./../../core/controllers/php/dashboard.php?tabla=producto&action=leer')
                .then(res => {
                    console.log(res)
                    this.productox = res.data.productos
                    this.filtros = this.productox
                })
        },
        addCli(e){
            axios.post('./../../core/controllers/php/dashboard.php?tabla=producto&action=agregar', new FormData(e.target))
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.can = 1;
                    this.add = 1;
                    this.getCli();
                }
                else if(vali[0].val == 1){
                    this.add = 2;
                    this.error = "Error al ingresar, verificar correo y/o telefono"
                }
                else if(vali[0].val == 2){
                    this.add = 2;
                    this.error = vali[0].mensaje;
                }
            })
        },
        modUsu(e){
            axios.post('./../../core/controllers/php/dashboard.php?tabla=producto&action=modificar&id='+this.id, new FormData(e.target))
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.add = 1;
                    this.getCli();
                }
                else if(vali[0].val == 1){
                    this.add = 2;
                    this.error = "Error al ingresar, verificar correo y/o telefono"
                }
                else if(vali[0].val == 2){
                    this.add = 2;
                    this.error = vali[0].mensaje
                }
            })
        },
        eliUsu(){
            console.log(this.id);
            axios.post('./../../core/controllers/php/dashboard.php?tabla=producto&action=borrar&id='+this.id)
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.add = 1;
                    this.error = vali[0].mensaje
                    this.can = 1;
                    this.getCli();
                }
                if(vali[0].val == 1)
                {
                    this.add = 2;
                    this.error = vali[0].mensaje
                    this.can = 1;
                    this.getCli();
                }
            })
        }
    }
})
//Productos
var productos = new Vue({
    el: '#pro',
});