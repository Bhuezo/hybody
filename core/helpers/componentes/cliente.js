Vue.component("v-cliente",{
    template:/*html*/ `
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12 text-center">
            <h3>Clientes</h3>
        </div>
        <div class="col-12 col-md-12 col-lg-12 mb-2">
            <div class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" v-model="buscar" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit" @click="getBuscar()">Search</button>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">
                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#addClietes" 
                            @click="add=0 ,nombre='',apellido='',fecha='', correo='',pais='Elegir...',ciudad='Elegir...',
                                    municipio='Elegir...',postal='', direccion='',telefono='',contra='', postal =''">
                                +
                            </button>
                        </th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">Fecha Nac.</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Pais</th>
                        <th scope="col">Modificar</th>
                        <th scope="col">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(datos,index) in filtros" :key="index">
                        <th scope="row">
                            {{datos.id}}
                        </th>
                        <td>
                            {{ datos.nombre}}
                        </td>
                        <td>
                            {{datos.apellido}}
                        </td>
                        <td>
                            {{datos.direccion}}
                        </td>
                        <td>
                            {{datos.fecha}}
                        </td>
                        <td>
                            {{datos.correo}}
                        </td>
                        <td>
                            {{datos.pais}}
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-primary" 
                                data-toggle="modal" data-target="#ModClientes"
                                @click="add=0,id=datos.id,nombre=datos.nombre,apellido=datos.apellido,fecha=datos.fecha,
                                    correo=datos.correo,contra=datos.contra,pais=datos.pais,genero=datos.genero,ciudad=datos.ciudad,
                                    municipio=datos.departamento,postal=datos.postal, 
                                    direccion=datos.direccion,telefono=datos.telefono, postal = datos.postal">
                                <i class="fas fa-pen"></i>
                            </button>
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#eliminarCli"
                                @click="id=datos.id">
                                <i class="fas fa-trash-alt"></i>
                            </button>

                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="alert alert-light" role="alert" v-if="clientex_exit==false">
            No se ha encontrado ningun resutado de: {{buscar_none}}
        </div>
        <!--
            Modal de modificar Usuarios
        -->
        <div class="modal fade" id="ModClientes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="add=false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="text-center" @submit.prevent="modUsu">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="nombre" placeholder="Nombre..." v-model="nombre" required/>
                            <label>Apellido</label>
                            <input type="text" class="form-control" name="apellido" placeholder="Apellido..." v-model="apellido" required/>
                            <label>Genero</label>
                            <select class="form-control" name="genero" required>
                                <option selected>{{ genero }}</option>
                                <option>Masculino</option>
                                <option>Femenino</option>
                            </select>
                            <label>Fecha de Nacimiento</label>
                            <input type="date" class="form-control" name="fecha" placeholder="Fecha Nacimiente..." v-model="fecha" min="1939-01-01" :max="ano+'-0'+(mes+1)+'-'+day" required/>
                            <label>Correo</label>
                            <input type="email" class="form-control" name="correo" placeholder="Correo..." v-model="correo" required/>
                            <label>Contraseña</label>
                            <input type="password" name="contra" placeholder="Contraseña..." v-model="contra" class="form-control" required/>
                            <label>Pais</label>
                            <select class="form-control" name="pais">
                                <option selected>{{pais}}</option>
                                <option>El Salvador</option>
                            </select>
                            <label>Ciudad</label>
                            <select class="form-control" name="ciudad">
                                <option selected>{{ciudad}}</option>
                                <option>San Salvador</option>
                            </select>
                            <label>Provincia/Municipio</label>
                            <select class="form-control" name="municipio">
                                <option selected>{{municipio}}</option>
                                <option>Mejicanos</option>
                            </select>
                            <label>Codigo Postal</label>
                            <input type="text" class="form-control" name="postal" placeholder="Codigo Postal..." v-model="postal" required/>
                            
                            <label>Dirreción</label>
                            <input type="text" class="form-control" name="direccion" placeholder="Direccion..." v-model="direccion" required/>
                            <label>Telefono</label>
                            <input type="tel" class="form-control" name="telefono" placeholder="Ej: 503-1111-2222" v-model="telefono" required/>
                            <div class="p-2">
                                <button type="submit" class="btn btn-info">Agregar</button>
                            </div>
                            <div class="alert alert-success" v-if="add==1" role="alert">
                                Se ha modificado con exito!!
                            </div>
                            <div class="alert alert-danger" v-if="add==2" role="alert">
                                {{error}}
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="add=false">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--
            Modal de Agregar nuevo usuario
        -->
        <div class="modal fade" id="addClietes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="add=false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="text-center" @submit.prevent="addCli">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="nombre" placeholder="Nombre..." v-model="nombre" required/>
                            <label>Apellido</label>
                            <input type="text" class="form-control" name="apellido" placeholder="Apellido..." v-model="apellido" required/>
                            <label>Genero</label>
                            <select class="form-control" name="genero" required>
                                <option selected>Elegir...</option>
                                <option>Masculino</option>
                                <option>Femenino</option>
                            </select>
                            <label>Fecha de Nacimiento</label>
                            <input type="date" class="form-control" name="fecha" placeholder="Fecha Nacimiente..." v-model="fecha" min="1939-01-01" :max="ano+'-0'+(mes+1)+'-'+day" required/>
                            <label>Correo</label>
                            <input type="email" class="form-control" name="correo" placeholder="Correo..." v-model="correo" required/>
                            <label>Contraseña</label>
                            <input type="password" name="contra" placeholder="Contraseña..." v-model="contra" class="form-control" required/>
                            <label>Pais</label>
                            <select class="form-control" name="pais">
                                <option selected>{{pais}}</option>
                                <option>El Salvador</option>
                            </select>
                            <label>Ciudad</label>
                            <select class="form-control" name="ciudad">
                                <option selected>{{ciudad}}</option>
                                <option>San Salvador</option>
                            </select>
                            <label>Provincia/Municipio</label>
                            <select class="form-control" name="municipio"s>
                                <option selected>{{municipio}}</option>
                                <option>Mejicanos</option>
                            </select>
                            <label>Codigo Postal</label>
                            <input type="text" class="form-control" name="postal" placeholder="Codigo Postal..." v-model="postal" required/>
                            
                            <label>Dirreción</label>
                            <input type="text" class="form-control" name="direccion" placeholder="Direccion..." v-model="direccion" required/>
                            <label>Telefono</label>
                            <input type="tel" class="form-control" name="telefono" placeholder="Ej: 503-1111-2222" v-model="telefono" required/>
                            <div class="p-2">
                                <button type="submit" class="btn btn-info" v-if="can==0">Agregar</button>
                            </div>
                            <div class="alert alert-success" v-if="add==1" role="alert">
                                Se ha agregado exitosamente!!
                            </div>
                            <div class="alert alert-danger" v-if="add==2" role="alert">
                                {{error}}
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="add=0, can=0">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal para eliminar el Usuario-->
        <div class="modal fade" id="eliminarCli" tabindex="-1" role="dialog" aria-labelledby="eliminarCliLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="eliminarCli">¿Quieres Eliminar?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-success" role="alert" v-if="add==1">
                            {{error}}
                        </div>
                        <div class="alert alert-danger" role="alert" v-if="add==2">
                            {{error}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="add=0, can=0">Cancelar</button>
                        <button type="button" class="btn btn-danger" @click="eliUsu()" v-if="can==0">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `,data() {
        return {
            //Datos de modelo
            id: 0,
            nombre:'',
            apellido:'',
            fecha: '',
            genero: '',
            correo:'',
            contra: '',
            pais:'',
            ciudad:'',
            municipio:'',
            postal:'',
            direccion:'',
            postal: '',
            telefono:'',
            add: 0,
            error: '',
            can: 0,
            //Busqueda
            buscar: '',
            clientex_exit: true,
            buscar_none: '',
            //Datos de la base de datos
            clientex: [],
            filtros: [],
            //fecha
            ano: new Date().getFullYear(),
            mes: new Date().getMonth(),
            day: new Date().getDate(),
        }
    },
    mounted() {
        this.getCli();
    },
    methods:{
        getBuscar() {
            var comp = this.buscar;
            this.filtros = this.clientex.filter(function (elem) {
                let el1 = elem.nombre.toLowerCase();
                if (comp === '') return true;
                else return el1.indexOf(comp.toLowerCase()) > -1;

            })
            if (this.filtros.length <= 0) {
                this.buscar_none = this.buscar
                this.clientex_exit = false;
            }
            else {
                this.clientex_exit = true
            }
            this.buscar = ''
        },
        getCli() {
            axios.post('./../../core/controllers/php/dashboard.php?tabla=cliente&action=leer')
                .then(res => {
                    console.log(res)
                    this.clientex = res.data.clientes
                    this.filtros = this.clientex
                })
        },
        addCli(e){
            axios.post('./../../core/controllers/php/dashboard.php?tabla=cliente&action=agregar', new FormData(e.target))
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.can = 1;
                    this.add = 1;
                    this.getCli();
                }
                else if(vali[0].val == 1){
                    this.add = 2;
                    this.error = "Error al ingresar, verificar correo y/o telefono"
                }
                else if(vali[0].val == 2){
                    this.add = 2;
                    this.error = vali[0].mensaje
                }
            })
        },
        modUsu(e){
            axios.post('./../../core/controllers/php/dashboard.php?tabla=cliente&action=modificar&id='+this.id, new FormData(e.target))
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.add = 1;
                    this.getCli();
                }
                else if(vali[0].val == 1){
                    this.add = 2;
                    this.error = "Error al ingresar, verificar correo y/o telefono"
                }
                else if(vali[0].val == 2){
                    this.add = 2;
                    this.error = vali[0].mensaje
                }
            })
        },
        eliUsu(){
            console.log(this.id);
            axios.post('./../../core/controllers/php/dashboard.php?tabla=cliente&action=borrar&id='+this.id)
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.add = 1;
                    this.error = vali[0].mensaje
                    this.can = 1;
                    this.getCli();
                }
                if(vali[0].val == 1)
                {
                    this.add = 2;
                    this.error = vali[0].mensaje
                    this.can = 1;
                    this.getCli();
                }
            })
        }
    }
});
//Clientes
var clientes = new Vue({
    el: '#client',
});