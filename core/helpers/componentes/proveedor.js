Vue.component("v-proveedor", {
    template:/*html*/ `
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12 text-center">
            <h3>Proveedor</h3>
        </div>
        <div class="col-12 col-md-12 col-lg-12 mb-2">
            <div class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" v-model="buscar" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit" @click="getBuscar()">Search</button>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">
                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#addProvee" 
                            @click="add=0 ,nombre='',contacto='',img='',telefono='', correo='',estado='Elegir...'">
                                +
                            </button>
                        </th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">Fecha Nac.</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Pais</th>
                        <th scope="col">Modificar</th>
                        <th scope="col">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(datos,index) in filtros" :key="index">
                        <th scope="row">
                            {{datos.id}}
                        </th>
                        <td>
                            {{ datos.nombre}}
                        </td>
                        <td>
                            <img :src="datos.img" width="100" height="100">
                        </td>
                        <td>
                            {{datos.contacto}}
                        </td>
                        <td>
                            {{datos.correo}}
                        </td>
                        <td>
                            {{datos.telefono}}
                        </td>
                        <td>
                            {{datos.estado}}
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-primary" 
                                data-toggle="modal" data-target="#ModProvee"
                                @click="add=0,id=datos.id,nombre=datos.nombre,contacto=datos.contacto, correo=datos.correo ,estado=datos.estado,img=datos.img,telefono=datos.telefono">
                                <i class="fas fa-pen"></i>
                            </button>
                        </td>
                        <td>
                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#eliminarProvee"
                                @click="id=datos.id">
                                <i class="fas fa-trash-alt"></i>
                            </button>

                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="alert alert-light" role="alert" v-if="proveedorx_exit==false">
            No se ha encontrado ningun resutado de: {{buscar_none}}
        </div>
        <!--
            Modal de modificar proveedor
        -->
        <div class="modal fade" id="ModProvee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="add=false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="text-center" @submit.prevent="modUsu">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="nombre" placeholder="Nombre..." v-model="nombre" required/>
                            <label>Contacto</label>
                            <input type="text" class="form-control" name="contacto" placeholder="Contacto..." v-model="contacto" required/>
                            
                            <label>Img</label>
                            <img :src="img" width="100" height="100">
                            <input type="file" class="form-control" name="img">
                            <label>Correo</label>
                            <input type="email" class="form-control" name="correo" placeholder="Correo..." v-model="correo" required/>
                            
                            <label>Telefono</label>
                            <input type="tel" class="form-control" name="telefono" placeholder="Ej: 503-1111-2222" v-model="telefono" required/>
                            <label>Estado</label>
                            <select class="form-control" name="estado" required>
                                <option estado>{{ estado }}</option>
                                <option>1</option>
                                <option>2</option>
                            </select>
                            <div class="p-2">
                                <button type="submit" class="btn btn-info">Agregar</button>
                            </div>
                            <div class="alert alert-success" v-if="add==1" role="alert">
                                Se ha modificado con exito!!
                            </div>
                            <div class="alert alert-danger" v-if="add==2" role="alert">
                                {{error}}
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="add=false">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!--
            Modal de Agregar nuevo proveedor
        -->
        <div class="modal fade" id="addProvee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="add=false">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="text-center" @submit.prevent="addCli">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="nombre" placeholder="Nombre..." v-model="nombre" required/>
                            <label>Contacto</label>
                            <input type="text" class="form-control" name="contacto" placeholder="Contacto..." v-model="contacto" required/>
                            
                            <label>Img</label>
                            <img :src="img" width="100" height="100">
                            <input type="file" class="form-control" name="img" required/>
                            <label>Correo</label>
                            <input type="email" class="form-control" name="correo" placeholder="Correo..." v-model="correo" required/>
                            
                            <label>Telefono</label>
                            <input type="tel" class="form-control" name="telefono" placeholder="Ej: 503-1111-2222" v-model="telefono" required/>
                            <label>Estado</label>
                            <select class="form-control" name="estado" required>
                                <option estado>{{ estado }}</option>
                                <option>1</option>
                                <option>2</option>
                            </select>
                            <div class="p-2">
                                <button type="submit" class="btn btn-info" v-if="can==0">Agregar</button>
                            </div>
                            <div class="alert alert-success" v-if="add==1" role="alert">
                                Se ha agregado exitosamente!!
                            </div>
                            <div class="alert alert-danger" v-if="add==2" role="alert">
                                {{error}}
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="add=0, can=0">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal para eliminar el proveedor-->
        <div class="modal fade" id="eliminarProvee" tabindex="-1" role="dialog" aria-labelledby="eliminarProveeLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="eliminarProvee">¿Quieres Eliminar?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-success" role="alert" v-if="add==1">
                            {{error}}
                        </div>
                        <div class="alert alert-danger" role="alert" v-if="add==2">
                            {{error}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="add=0, can=0">Cancelar</button>
                        <button type="button" class="btn btn-danger" @click="eliUsu()" v-if="can==0">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `,data() {
        return {
            //Datos de modelo
            id: 0,
            nombre:'',
            img:'',
            contacto: '',
            correo: '',
            telefono:'',
            estado: '',
            add: 0,
            error: '',
            can: 0,
            //Busqueda
            buscar: '',
            proveedorx_exit: true,
            buscar_none: '',
            //Datos de la base de datos
            proveedorx: [],
            filtros: [],
            //fecha
            ano: new Date().getFullYear(),
            mes: new Date().getMonth(),
            day: new Date().getDate(),
        }
    },
    mounted() {
        this.getCli();
    },
    methods:{
        getBuscar() {
            var comp = this.buscar;
            this.filtros = this.proveedorx.filter(function (elem) {
                let el1 = elem.nombre.toLowerCase();
                if (comp === '') return true;
                else return el1.indexOf(comp.toLowerCase()) > -1;

            })
            if (this.filtros.length <= 0) {
                this.buscar_none = this.buscar
                this.proveedorx_exit = false;
            }
            else {
                this.proveedorx_exit = true
            }
            this.buscar = ''
        },
        getCli() {
            axios.post('./../../core/controllers/php/dashboard.php?tabla=proveedor&action=leer')
                .then(res => {
                    console.log(res)
                    this.proveedorx = res.data.proveedor
                    this.filtros = this.proveedorx
                })
        },
        addCli(e){
            axios.post('./../../core/controllers/php/dashboard.php?tabla=proveedor&action=agregar', new FormData(e.target))
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.can = 1;
                    this.add = 1;
                    this.getCli();
                }
                else if(vali[0].val == 1){
                    this.add = 2;
                    this.error = "Error al ingresar, verificar correo y/o telefono"
                }
                else if(vali[0].val == 2){
                    this.add = 2;
                    this.error = vali[0].mensaje;
                }
            })
        },
        modUsu(e){
            axios.post('./../../core/controllers/php/dashboard.php?tabla=proveedor&action=modificar&id='+this.id, new FormData(e.target))
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.add = 1;
                    this.getCli();
                }
                else if(vali[0].val == 1){
                    this.add = 2;
                    this.error = "Error al ingresar, verificar correo y/o telefono"
                }
                else if(vali[0].val == 2){
                    this.add = 2;
                    this.error = vali[0].mensaje
                }
            })
        },
        eliUsu(){
            console.log(this.id);
            axios.post('./../../core/controllers/php/dashboard.php?tabla=proveedor&action=borrar&id='+this.id)
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.add = 1;
                    this.error = vali[0].mensaje
                    this.can = 1;
                    this.getCli();
                }
                if(vali[0].val == 1)
                {
                    this.add = 2;
                    this.error = vali[0].mensaje
                    this.can = 1;
                    this.getCli();
                }
            })
        }
    }
})
//Proveedores
const prove = new Vue({
    el: '#provee',
});