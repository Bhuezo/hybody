<?php 
    class Validacion
    {
        public function especiales($dato="") {
            if (preg_match(`/^(["'=;<>?_+]~#])+$/`, $dato)) {
                return false;
            }
            else {
                return true;
            }
        }
        public function correos($email="") {
            if (preg_match("/^([a-zA-Z0-9&])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-])*.([a-zA-Z])+$/", $email)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function tokens ($token) {
            return substr($token, ($token[1]+2),( ( ($token[1]+2)-5 )- $token[2]*2 ) - (($token[ ( $token[2]*2 )+5+$token[4] ] )+1) );

        }
        public function fechas($fecha="") {
            if (preg_match("/^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})+$/", $fecha)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function letras($letra="") {
            if (preg_match("/^([a-zA-Z- &]{3,100})+$/", $letra)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function telefonos($telefono="") {
            if (preg_match("/^([0-9]{8,})+$/", $telefono)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function precios($precios="") {
            if (preg_match("/^([0-9])*.([0-9]{0,2})+$/", $precios)) {
                return true;
            }
            else {
                return false;
            }
        }
        public function numeros($numero="") {
            if (preg_match("/^([0-9])+$/", $numero)) {
                return true;
            }
            else {
                return false;
            }
        }
        
        public function combobox($box="") {
            if ($box != "Elegir...") {
                return true;
            }
            else {
                return false;
            }
        }
    }
?>