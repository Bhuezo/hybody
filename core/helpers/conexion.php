<?php
	class Conexion
	{
		public function connec()  
		{
			$servername = "127.0.0.1";
			$database = "hybody";
			$username = "hbody";
			$password = "qwerty";
			// Create connection
			$conn = mysqli_connect($servername, $username, $password, $database);
			mysqli_query($conn,"SET NAMES 'UTF8'");
			// Check connection
			if (!$conn) {
				echo("Error: aqui" . mysqli_connect_error());
			}
			return $conn;
		}
		public function crud(){
			$sql = "select * from pedido where id_cliente = ?";
			$params = 2;
			$bd = new Conexion();
			$conn = $bd->connec();
			$ingre = array();
			if ($result = mysqli_prepare($conn, $sql) ){
				mysqli_stmt_bind_param($result, "i", $params);
				mysqli_stmt_execute($result);
				$row = array();
				$resultado = mysqli_stmt_get_result($result);
				while ($row = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
					
                    array_push($ingre, $row);
				}
				$res['carrito']=$ingre;
				header( 'Content-type: application/json' );
				echo json_encode($res);
			}
			else{
				$mensaje = 'Algo salio mal';
				switch(mysqli_errno($conn)){
					case 1045:
						$mensaje = 'Autenticacion desconocida';
					break;
					case 1049:
						$mensaje = 'Base desconocida';
					break;
					case 1054:
						$mensaje = 'Nombre del campo desconocido';
					break;
					case 1062:
						$mensaje = 'datos duplicados no se puede guardar';
					break;
					case 1146:
						$mensaje = 'Nombre de la tabla desconocido';
					break;
					case 1451:
						$mensaje = 'Registro ocupado no se puede eliminar';
					break;
					case 2002:
						$mensaje = 'Servidor desconocido';
					break;
				}
				$res['message']=$mensaje;
				header( 'Content-type: application/json' );
				echo json_encode($res);
			}
		}
	}
?>
