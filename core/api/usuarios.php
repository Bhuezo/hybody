<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {  
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");  
        header('Access-Control-Allow-Credentials: true');  
        header('Access-Control-Max-Age: 86400');   
    }  
    
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {  
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))  
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");  
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))  
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");  
    }

    include('../models/CRUD_usuarios.php');
    include('../helpers/validaciones.php');

    $crud = new Usuarios();
    $val = new Validacion();

    $action = 'login';
    $privilege = 'privete';
    
    if (isset($_GET['privilege'])) {
        $privilege = $_GET['privilege'];
    }
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }

    switch ($privilege) {
        case 'private':
            switch ($action) {
                case 'consulta':
                    $crud->getUsuariox();
                break;
                case 'crear':
                    $D = json_decode(file_get_contents('php://input'), true);
                    if ( !empty($D['data']['nombre']) && !empty($D['data']['apellido']) && !empty($D['data']['genero']) && !empty($D['data']['direccion']) &&
                    !empty($D['data']['fecha']) && !empty($D['data']['correo']) && !empty($D['data']['contra']) && !empty($D['data']['pais']) &&
                    !empty($D['data']['departamento']) && !empty($D['data']['ciudad']) && !empty($D['data']['token']) ) {
                        if ($val->correos($D['data']['correo'])) {
                            if ($val->fechas($D['data']['fecha'])) {
                                if ($val->numeros($D['data']['telefono'])) {
                                    if ($val->numeros($D['data']['token'])) {
                                        $crud->setUsuariox($D['data']['nombre'],$D['data']['apellido'],$D['data']['genero'],$D['data']['direccion'],
                                        $D['data']['fecha'],$D['data']['correo'],password_hash($D['data']['contra'], PASSWORD_DEFAULT),$D['data']['pais'],
                                        $D['data']['departamento'],$D['data']['ciudad'],$D['data']['telefono'],$D['data']['token'] );
                                    }
                                    else {
                                        $res['status'] = 0;
                                        $res['message'] = 'Privilegio no valído';
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }
                                }
                                else {
                                    $res['status'] = 0;
                                    $res['message'] = 'Telefono no valído';
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'Fecha no valído';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Correo no valído';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = 'Datos vacíos: '.$D['data']['apellido'];
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'modificar':
                    $D = json_decode(file_get_contents('php://input'), true);
                    if ( !empty($D['data']['nombre']) && !empty($D['data']['apellido']) && !empty($D['data']['genero']) && 
                    !empty($D['data']['direccion']) && !empty($D['data']['fecha']) && !empty($D['data']['correo'])  && 
                    !empty($D['data']['pais']) && !empty($D['data']['departamento']) && !empty($D['data']['ciudad']) && 
                    !empty($D['data']['token']) ) {
                        if ($val->correos($D['data']['correo'])) {
                            if ($val->fechas($D['data']['fecha'])) {
                                if ($val->numeros($D['data']['telefono'])) {
                                    if ($val->numeros($D['data']['token'])) {
                                        $crud->modUsuariox($_GET['id'],$D['data']['nombre'],$D['data']['apellido'],$D['data']['genero'],$D['data']['direccion'],
                                        $D['data']['fecha'],$D['data']['correo'],password_hash($D['data']['contra'], PASSWORD_DEFAULT),$D['data']['pais'],
                                        $D['data']['departamento'],$D['data']['ciudad'],$D['data']['telefono'],$D['data']['token']);
                                    }
                                    else {
                                        $res['status'] = 0;
                                        $res['message'] = 'Privilegio no valído';
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }
                                }
                                else {
                                    $res['status'] = 0;
                                    $res['message'] = 'Telefono no valído';
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'Fecha no valído';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Correo no valído';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = 'Datos vacíos';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'img':
                    if (isset($_GET['id']) && $_GET['id'] > 0) {
                        if (file_exists($_FILES['img']['tmp_name'])) {
                            if(!empty( $_FILES['img']['tmp_name'] )) {
                                $img = $_FILES['img']['tmp_name'];
                                $destino = '../../resources/img/imagen/usuarios';
                                if ( file_exists ($destino) ) {
                                    if ($_FILES['img']['type'] == "image/png") {
                                        $destino = '../../resources/img/imagen/usuarios/'.$_GET['id'].'.png';
                                        if (copy($img, $destino)) {
                                            //Ingresar
                                            $id = $_GET['id'];
                                            $des =  'http://localhost/hybody/resources/img/imagen/usuarios/'.$_GET['id'].'.png';
                                            $crud->imgUsuariox($id,$des);
                                        }
                                        else {
                                            $res['status'] = 0;
                                            $res['message'] = 'Error al intentar guardar imagen';
                                            header( 'Content-type: application/json');
                                            echo json_encode($res);
                                        }
                                    }
                                    else {
                                        $res['status'] = 0;
                                        $res['message'] = 'Imagen no valida';
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }
                                }
                                else {
                                    $res['status'] = 0;
                                    $res['message'] = 'La dirección no existe';
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'No ha seleccionado ninguna imagen nueva';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Imagen no seteada';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = 'Id de usuario no seteada';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'validar':
                    #Se valida si se manda el token
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);
                            #Se manda el token al modelo
                            #if (isset($_GET['contra'])) {
                                #if (!empty($_GET['contra'])) {
                                    $crud->verificar( $id );
                                /*}
                                else{
                                    #En caso de que el numero de caracteres
                                    #Sea invalido se mandara un mensaje
                                    $res['status'] = 0;
                                    $res['message'] = 'Contraseña no valida';
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }
                            }
                            else{
                                #En caso de que el numero de caracteres
                                #Sea invalido se mandara un mensaje
                                $res['status'] = 0;
                                $res['message'] = 'No se ha mandado contraseña';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }*/
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'login':
                    if ( isset($_POST['correo']) && isset($_POST['contra']) ) {
                        if ( !empty($_POST['correo']) && !empty($_POST['contra']) ) {
                            if ($val->correos($_POST['correo'])) {
                                $crud->login($_POST['correo'],$_POST['contra']);
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'Correo no valído';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Datos vacíos';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = 'No se ha mandado datos';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
            }
        break;
    }

?>