<?php

    
    if (isset($_SERVER['HTTP_ORIGIN'])) {  
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");  
        header('Access-Control-Allow-Credentials: true');  
        header('Access-Control-Max-Age: 86400');   
    }  

    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {  

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))  
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");  

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))  
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");  
    }
    include('../models/CRUD_pedidos.php');
    include('../helpers/validaciones.php');

    $crud = new Pedidos();
    $val = new Validacion();

    $action = 'consulta';
    $privilege = 'public';


    //Valida que accion se hara
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }
    if (isset($_GET['privilege'])) {
        $privilege = $_GET['privilege'];
    }
    /*
        ____________________________________________________________________________________
        | Este es el switch que se encarga de validar de qeu parte de la api se llama si es|
        |                         desde el dashboard o desde el public                     |
        ------------------------------------------------------------------------------------
    */

    switch ($privilege) {
        #Este es el case publico aqui estan las apis para el publico
        case 'public': 
            switch ($action) {
                #Esta accion es para la validadcion de sesion
                #Por medio del token que se acumula en el sessionstorage

                case 'consulta':
                    #Se valida si se manda el token
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);
                            #Se manda el token al modelo
                            $crud->leerPedidos( $id );
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'consultaP':
                    #Se valida si se manda el token
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);
                            #Se manda el token al modelo
                            $crud->leerPedidosP( $id );
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'consultaC':
                    #Se valida si se manda el token
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);
                            #Se manda el token al modelo
                            $crud->leerPedidosC( $id );
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
            }
        break;
        case 'private':
            switch ($action) {
                case 'ventasP':
                    $crud->ventasP();
                break;
                case 'ventasV':
                    if (isset($_GET['ano'])) {
                        if (!empty($_GET['ano'])) {
                            $crud->ventasV($_GET['ano']);
                        }
                        else {
                            #En vaso de que no se haya seteado nada se mandara que no hay
                            #Session alguna
                            $res['status'] = 0;
                            $res['message'] = 'No hay año';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay año';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'ventasC':
                    if (isset($_GET['ano'])) {
                        if (!empty($_GET['ano'])) {
                            $crud->ventasC($_GET['ano']);
                        }
                        else {
                            #En vaso de que no se haya seteado nada se mandara que no hay
                            #Session alguna
                            $res['status'] = 0;
                            $res['message'] = 'No hay año';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay año';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
            }
        break;
    }



    
?>