<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {  
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");  
        header('Access-Control-Allow-Credentials: true');  
        header('Access-Control-Max-Age: 86400');   
    }  
    
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {  
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))  
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");  
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))  
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");  
    }
    include('../models/CRUD_cajas.php');
    include('../helpers/validaciones.php');


    $val = new Validacion();
    $crud = new Cajas();

    $action = 'verificar';
    $privilege = 'public';

    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }
    if (isset($_GET['privilege'])) {
        $privilege = $_GET['privilege'];
    }

    switch ($privilege) {
        case 'public':
            switch ($action) {
                case 'verificar':
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);
                            #Se manda el token al modelo
                            $crud->verifi( $id );
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'No hay Sesión valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay Sesión';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'cambiar':
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);

                            $D = json_decode(file_get_contents('php://input'), true);

                            if (count($D) > 0) {
                                $crud->cambiar($D,$id);
                            }
                            else {
                                #En vaso de que no se haya seteado nada se mandara que no hay
                                #Session alguna
                                $res['status'] = 0;
                                $res['message'] = 'No hay datos';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'borrar':
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);

                            $D = json_decode(file_get_contents('php://input'), true);
                            if (count($D) > 0) {
                                $crud->borrar($D,$id);
                            }
                            else {
                                #En vaso de que no se haya seteado nada se mandara que no hay
                                #Session alguna
                                $res['status'] = 0;
                                $res['message'] = 'No hay datos';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'agregarCarrito':
                if (isset($_GET['token'])) {
                    #Se valida el numero de caracteres
                    if (strlen($_GET['token']) > 13){

                        $token = $_GET['token'];
                        #Se obtiene el valor del token
                        $id =  $val->tokens($token);

                        $D = json_decode(file_get_contents('php://input'), true);
                        if (count($D) > 0) {
                            if ( isset($_GET['ven']) ) {
                                if ($_GET['ven'] != null) {
                                    $crud->agregar($D,$id,$_GET['ven']);
                                }
                                else {
                                    $crud->agregar($D,$id,0);
                                }
                            }
                            else {
                                $crud->agregar($D,$id,0);
                            }
                        }
                        else {
                            #En vaso de que no se haya seteado nada se mandara que no hay
                            #Session alguna
                            $res['status'] = 0;
                            $res['message'] = 'No hay session';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else{
                        #En caso de que el numero de caracteres
                        #Sea invalido se mandara un mensaje
                        $res['status'] = 0;
                        $res['message'] = 'Token no valido';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                }
                else {
                    #En vaso de que no se haya seteado nada se mandara que no hay
                    #Session alguna
                    $res['status'] = 0;
                    $res['message'] = 'No hay session';
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
                break;
            }
        break;
    }
    /*if ($action=='verificar') {

        //$nombre_host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
 
        //echo $nombre_host;
    }*/
