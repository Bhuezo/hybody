<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {  
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");  
        header('Access-Control-Allow-Cresentials: true');  
        header('Access-Control-Max-Age: 86400');   
    }  
    
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {  
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))  
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");  
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))  
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");  
    }

    require_once('../libraries/pagos/config.php');
    require_once('../libraries/pagos/Pagadito.php');
    include('../helpers/validaciones.php');

    $val = new Validacion();

    $privilege = 'public';
    $action = 'pagar';

    if (isset($_GET['privilege'])) {
        $privilege = $_GET['privilege'];
    }

    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }


    switch($privilege) {
        case 'public':
            switch($action) {
                case 'pagar':
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);
                            #Se manda el token al modelo
                            $Pagadito = new Pagadito(UID, WSK);
                            if (SANDBOX) {
                                $Pagadito->mode_sandbox_on();
                            }
                            if ($Pagadito->connect()) {

                                $D = json_decode(file_get_contents('php://input'), true);
                                for ($i=0; $i < count($D); $i++) {
                                    if ($D[$i]["cantidad"] > 0) {
                                        $Pagadito->add_detail($D[$i]["cantidad"], $D[$i]["nombre"], $D[$i]["precio"]);
                                    }
                                }
                                if ( isset($_GET['idPedido']) ) {
                                    $ern = rand(1000, 2000);
                                    if (!$Pagadito->exec_trans($ern,$id,$_GET['idPedido'])) {
                                        $res['status'] = 0;
                                        switch($Pagadito->get_rs_code())
                                        {
                                            case "PG2001":
                                                $res['message'] = $Pagadito->get_rs_code().": Datos Incompletos";
                                            case "PG3002":
                                                $res['message'] = $Pagadito->get_rs_code().": Error!!";
                                            case "PG3003":
                                                $res['message'] = $Pagadito->get_rs_code().": No se ha registrado transacción!!";
                                            case "PG3004":
                                                $res['message'] = $Pagadito->get_rs_code().": Conexion desactivada";
                                            case "PG3005":
                                                /*Disabled connection*/
                                            default:
                                                $res['message'] = $Pagadito->get_rs_code().": ".$Pagadito->get_rs_message();
                                                break;
                                        }
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }
                                }
                                else {
                                    $res['status'] = 0;
                                    $res['message'] = 'No se ha agregado la id del pedido';
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }
                            } else {
                                $res['status'] = "hola";
                                switch($Pagadito->get_rs_code())
                                {
                                    case "PG2001":
                                        $res['message'] = $Pagadito->get_rs_code().": Datos Incompletos";
                                    case "PG3001":
                                    $res['message'] = $Pagadito->get_rs_code().": Problemas cn la conexion";
                                    case "PG3002":
                                    $res['message'] = $Pagadito->get_rs_code().": Error!!";
                                    case "PG3003":
                                    $res['message'] = $Pagadito->get_rs_code().": No se ha registrado transacción!!";
                                    case "PG3005":
                                    $res['message'] = $Pagadito->get_rs_code().": Conexion desactivada";
                                    case "PG3006":
                                    $res['message'] = $Pagadito->get_rs_code().": Se ha execedido y algo ha salido mal";
                                    default:
                                        $res['message'] = $Pagadito->get_rs_code().": ".$Pagadito->get_rs_message();
                                        break;
                                }
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
            }
        break;
    }
?>