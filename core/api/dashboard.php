<?php

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Allow: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }
    include('../models/CRUD-dashboard.php');
    include('../helpers/validaciones.php');

    $crud = new Dashboard();
    $validar = new Validacion();
    $tabla = '';
    $action='';
    //Valida que accion se hara y la tabla a la que se llamara
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }
    if (isset($_GET['tabla'])) {
        $tabla = $_GET['tabla'];
    }

    /*
        Usuario
    */
    if ($tabla=='usuario' && $action=="leer") {
        $crud->usuarioL();
        
    }
    if ($tabla=='usuario' && $action=="agregar") {
        $p0 = $_POST['nombre'];
        $p1 = $_POST['apellido'];
        $p2 = $_POST['genero'];
        $p3 = $_POST['direccion'];
        $p4 = $_POST['fecha'];
        $p5 = $_POST['correo'];
        $p6 = $_POST['contra'];
        $p7 = $_POST['pais'];
        $p8 = $_POST['ciudad'];
        $p9 = $_POST['municipio'];
        $p10 = $_POST['telefono'];
        
        //Validar campos vacíos
        if ($p0!="" && $p1!="" && $p2!="" && $p3!="" && $p4!="" && $p5!="" && $p6!="" && $p7!="" && $p8!="" && 
        $p9!="" && $p10!="") {
            //Validar nombre y apellido
            if ($validar->letras($p0) && $validar->letras($p1)) {
                //Validar Fecha de nacimiento
                if ($validar->fechas($p4)) {
                    //Validar Correo
                    if ($validar->correos($p5)) {
                        //validar combobox genero
                        if ($validar->combobox($p2)) {
                            //validar combobox pais, ciudad, municipio
                            if ($validar->combobox($p7) && $validar->combobox($p8) && $validar->combobox($p9)) {
                                //validar Telefono
                                if ($validar->telefonos($p10)) {
                                    //Ingresar
                                    $crud->usuarioA($p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10);
                                }
                                else {
                                    $ingre = array();
                                    $var = array('val'=>2 ,'mensaje'=> 'Telefono invalido');
                                    array_push( $ingre,$var);
                                    $res['error'] = $ingre;
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }
                            }
                            else {
                                $ingre = array();
                                $var = array('val'=>2 ,'mensaje'=> 'Ha faltado elegir algun dato de su ubicacion invalidos');
                                array_push( $ingre,$var);
                                $res['error'] = $ingre;
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $ingre = array();
                            $var = array('val'=>2 ,'mensaje'=> 'Elegir un Genero invalidos');
                            array_push( $ingre,$var);
                            $res['error'] = $ingre;
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $ingre = array();
                        $var = array('val'=>2 ,'mensaje'=> 'Correo invalidos');
                        array_push( $ingre,$var);
                        $res['error'] = $ingre;
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                }
                else {
                    $ingre = array();
                    $var = array('val'=>2 ,'mensaje'=> 'Fecha invalida');
                    array_push( $ingre,$var);
                    $res['error'] = $ingre;
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Nombre y/o apellido invalidos');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
    if ($tabla=='usuario' && $action=="modificar") {
        $id = $_GET['id'];
        $p0 = $_POST['nombre'];
        $p1 = $_POST['apellido'];
        $p2 = $_POST['genero'];
        $p3 = $_POST['direccion'];
        $p4 = $_POST['fecha'];
        $p5 = $_POST['correo'];
        $p6 = $_POST['contra'];
        $p7 = $_POST['pais'];
        $p8 = $_POST['ciudad'];
        $p9 = $_POST['municipio'];
        $p10 = $_POST['telefono'];
        
        //Validar campos vacíos
        if ($id!="" && $id!=0 && $p0!="" && $p1!="" && $p2!="" && $p3!="" && $p4!="" && $p5!="" && $p6!="" && $p7!="" && $p8!="" && 
        $p9!="" && $p10!="") {
            //Validar nombre y apellido
            if ($validar->letras($p0) && $validar->letras($p1)) {
                //Validar Fecha de nacimiento
                if ($validar->fechas($p4)) {
                    //Validar Correo
                    if ($validar->correos($p5)) {
                        //validar combobox genero
                        if ($validar->combobox($p2)) {
                            //validar combobox pais, ciudad, municipio
                            if ($validar->combobox($p7) && $validar->combobox($p8) && $validar->combobox($p9)) {
                                //validar Telefono
                                if ($validar->telefonos($p10)) {
                                    //Ingresar
                                    $crud->usuarioM($id,$p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10);
                                }
                                else {
                                    $ingre = array();
                                    $var = array('val'=>2 ,'mensaje'=> 'Telefono invalido');
                                    array_push( $ingre,$var);
                                    $res['error'] = $ingre;
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }
                            }
                            else {
                                $ingre = array();
                                $var = array('val'=>2 ,'mensaje'=> 'Ha faltado elegir algun dato de su ubicacion invalidos');
                                array_push( $ingre,$var);
                                $res['error'] = $ingre;
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $ingre = array();
                            $var = array('val'=>2 ,'mensaje'=> 'Elegir un Genero invalidos');
                            array_push( $ingre,$var);
                            $res['error'] = $ingre;
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $ingre = array();
                        $var = array('val'=>2 ,'mensaje'=> 'Correo invalidos');
                        array_push( $ingre,$var);
                        $res['error'] = $ingre;
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                }
                else {
                    $ingre = array();
                    $var = array('val'=>2 ,'mensaje'=> 'Fecha invalida');
                    array_push( $ingre,$var);
                    $res['error'] = $ingre;
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Nombre y/o apellido invalidos');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
    if ($tabla=='usuario' && $action=="borrar") {
        $id = $_GET['id'];
        if ($id!=0 && $id>0)
        {
            $crud->usuarioB($id);
        }
    }
    /*
        Clientes
    */
    if ($tabla=='cliente' && $action=="leer") {
        
        $crud->clienteL();
        
    }
    if ($tabla=='cliente' && $action=="agregar") {
        $p0 = $_POST['nombre'];//Nombre
        $p1 = $_POST['apellido'];//Apellido
        $p2 = $_POST['genero'];//Genero
        $p3 = $_POST['correo'];//Correo
        $p4 = $_POST['contra'];//Contra
        $p5 = $_POST['pais'];//Pais
        $p6 = $_POST['fecha'];//Fecha
        $p7 = $_POST['ciudad'];//Ciudad
        $p8 = $_POST['municipio'];//Minucipio
        $p9 = $_POST['postal'];//Postal
        $p10 = $_POST['telefono'];//Telefono
        $p11 = $_POST['direccion'];//Dirreccion
        
        //Validar campos vacíos
        if ($p0!="" && $p1!="" && $p2!="" && $p3!="" && $p4!="" && $p5!="" && $p6!="" && $p7!="" && $p8!="" && 
        $p9!="" && $p10!="" && $p11!="") {
            //Validar nombre y apellido
            if ($validar->letras($p0) && $validar->letras($p1)) {
                //Validar Fecha de nacimiento
                if ($validar->fechas($p6)) {
                    //Validar Correo
                    if ($validar->correos($p3)) {
                        //validar combobox genero
                        if ($validar->combobox($p2)) {
                            //validar combobox pais, ciudad, municipio
                            if ($validar->combobox($p5) && $validar->combobox($p7) && $validar->combobox($p8)) {
                                //validar Telefono
                                if ($validar->telefonos($p10)) {
                                    //validar Codigo Postal
                                    if ($validar->numeros($p9)) {
                                        //Ingresar
                                        $crud->clienteA($p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$p11);
                                    }
                                    else {
                                        $ingre = array();
                                        $var = array('val'=>2 ,'mensaje'=> 'Telefono invalido');
                                        array_push( $ingre,$var);
                                        $res['error'] = $ingre;
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }
                                }
                                else {
                                    $ingre = array();
                                    $var = array('val'=>2 ,'mensaje'=> 'Telefono invalido');
                                    array_push( $ingre,$var);
                                    $res['error'] = $ingre;
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }
                            }
                            else {
                                $ingre = array();
                                $var = array('val'=>2 ,'mensaje'=> 'Ha faltado elegir algun dato de su ubicacion invalidos');
                                array_push( $ingre,$var);
                                $res['error'] = $ingre;
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $ingre = array();
                            $var = array('val'=>2 ,'mensaje'=> 'Elegir un Genero invalidos');
                            array_push( $ingre,$var);
                            $res['error'] = $ingre;
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $ingre = array();
                        $var = array('val'=>2 ,'mensaje'=> 'Correo invalidos');
                        array_push( $ingre,$var);
                        $res['error'] = $ingre;
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                }
                else {
                    $ingre = array();
                    $var = array('val'=>2 ,'mensaje'=> 'Fecha invalida');
                    array_push( $ingre,$var);
                    $res['error'] = $ingre;
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Nombre y/o apellido invalidos');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
    if ($tabla=='cliente' && $action=="modificar") {
        $id = $_GET['id'];
        $p0 = $_POST['nombre'];//Nombre
        $p1 = $_POST['apellido'];//Apellido
        $p2 = $_POST['genero'];//Genero
        $p3 = $_POST['correo'];//Correo
        $p4 = $_POST['contra'];//Contra
        $p5 = $_POST['pais'];//Pais
        $p6 = $_POST['fecha'];//Fecha
        $p7 = $_POST['ciudad'];//Ciudad
        $p8 = $_POST['municipio'];//Minucipio
        $p9 = $_POST['postal'];//Postal
        $p10 = $_POST['telefono'];//Telefono
        $p11 = $_POST['direccion'];//Dirreccion
        
        //Validar campos vacíos
        if ($id!="" && $id!=0 && $p0!="" && $p1!="" && $p2!="" && $p3!="" && $p4!="" && $p5!="" && $p6!="" && 
        $p7!="" && $p8!="" && $p9!="" && $p10!="" && $p11!="") {
            //Validar nombre y apellido
            if ($validar->letras($p0) && $validar->letras($p1)) {
                //Validar Fecha de nacimiento
                if ($validar->fechas($p6)) {
                    //Validar Correo
                    if ($validar->correos($p3)) {
                        //validar combobox genero
                        if ($validar->combobox($p2)) {
                            //validar combobox pais, ciudad, municipio
                            if ($validar->combobox($p5) && $validar->combobox($p7) && $validar->combobox($p8)) {
                                //validar Telefono
                                if ($validar->telefonos($p10)) {
                                    //validar Codigo Postal
                                    if ($validar->numeros($p9)) {
                                        //Ingresar
                                        $crud->clienteM($id,$p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$p11);
                                    }
                                    else {
                                        $ingre = array();
                                        $var = array('val'=>2 ,'mensaje'=> 'Telefono invalido');
                                        array_push( $ingre,$var);
                                        $res['error'] = $ingre;
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }
                                }
                                else {
                                    $ingre = array();
                                    $var = array('val'=>2 ,'mensaje'=> 'Telefono invalido');
                                    array_push( $ingre,$var);
                                    $res['error'] = $ingre;
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }
                            }
                            else {
                                $ingre = array();
                                $var = array('val'=>2 ,'mensaje'=> 'Ha faltado elegir algun dato de su ubicacion 
                                invalidos');
                                
                                array_push( $ingre,$var);
                                $res['error'] = $ingre;
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $ingre = array();
                            $var = array('val'=>2 ,'mensaje'=> 'Elegir un Genero invalidos');
                            array_push( $ingre,$var);
                            $res['error'] = $ingre;
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $ingre = array();
                        $var = array('val'=>2 ,'mensaje'=> 'Correo invalidos');
                        array_push( $ingre,$var);
                        $res['error'] = $ingre;
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                }
                else {
                    $ingre = array();
                    $var = array('val'=>2 ,'mensaje'=> 'Fecha invalida');
                    array_push( $ingre,$var);
                    $res['error'] = $ingre;
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Nombre y/o apellido invalidos');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
    if ($tabla=='cliente' && $action=="borrar") {
        $id = $_GET['id'];
        if ($id!=0 && $id>0)
        {
            $crud->clienteB($id);
        }
    }
    /*
        Proveedores
    */
    if ($tabla=='proveedor' && $action=="leer") {
        
        $crud->proveedorL();
        
    }
    if ($tabla=='proveedor' && $action=="agregar") {
        $p0 = $_POST['nombre'];//Nombre
        $p1 = $_FILES['img']['tmp_name'];
        $p2 = $_POST['contacto'];//Contacto
        $p3 = $_POST['telefono'];//Telefono
        $p4 = $_POST['correo'];//Correo
        $p5 = $_POST['estado'];//Estado
        
        //Validar campos vacíos
        if ($p0!="" && $p1!="" && $p2!="" && $p3!="" && $p4!="" && $p5!="") {
            //Validar nombre y contacto
            if ($validar->letras($p0) && $validar->letras($p2)) {
                //Validar Correo
                if ($validar->correos($p4)) {
                    //validar combobox estado
                    if ($validar->combobox($p5)) {
                        //validar Telefono
                        if ($validar->telefonos($p3)) {
                            $destino = '/home/bhuezo/Documentos/Proyectos/hybody/resources/img/imagen/proveedores/'.$_FILES['img']['name'];
                            if (copy($p1, $destino))
                            {
                                //Ingresar
                                $des =  '../../resources/img/imagen/proveedores/'.$_FILES['img']['name'];
                                $crud->proveedorA($p0,$des,$p2,$p3,$p4,$p5);
                            }
                            else {
                                $ingre = array();
                                $var = array('val'=>2 ,'mensaje'=> 'Error al copiar imagen');
                                array_push( $ingre,$var);
                                $res['error'] = $ingre;
                                header( 'Content-type: application/json');
                            }
                        }
                        else {
                            $ingre = array();
                            $var = array('val'=>2 ,'mensaje'=> 'Telefono Invalido');
                            array_push( $ingre,$var);
                            $res['error'] = $ingre;
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $ingre = array();
                        $var = array('val'=>2 ,'mensaje'=> 'No se ha elegido un estado');
                        array_push( $ingre,$var);
                        $res['error'] = $ingre;
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                }
                else {
                    $ingre = array();
                    $var = array('val'=>2 ,'mensaje'=> 'Correo invalida');
                    array_push( $ingre,$var);
                    $res['error'] = $ingre;
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Nombre y/o Contacto invalidos');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
    if ($tabla=='proveedor' && $action=="modificar") {
        $id = $_GET['id'];
        $p0 = $_POST['nombre'];//Nombre
        $p2 = $_POST['contacto'];//contacto
        $p1 = $_FILES['img']['tmp_name'];
        $p3 = $_POST['correo'];//Correo
        $p4 = $_POST['telefono'];//Contra
        $p5 = $_POST['estado'];//Pais
        //Validar campos vacíos
        if ($id!="" && $id!=0 && $p0!="" && $p2!="" && $p3!="" && $p4!="" && $p5!="") {
            //Validar nombre y contacto
            if ($validar->letras($p0) && $validar->letras($p2)) {
                //Validar Correo
                if ($validar->correos($p3)) {
                    //validar combobox estado
                    if ($validar->combobox($p5)) {
                        //validar Telefono
                        if ($validar->telefonos($p4)) {
                            if (empty($_FILES['img']['name'])) {
                                $crud->proveedorM(1,$id,$p0,"",$p2,$p3,$p4,$p5);
                            }
                            else {
                                #$destino = '/home/bhuezo/Documentos/Proyectos/hybody/resources/img/imagen/proveedores/'.$_FILES['img']['name'];
                                $destino = 'C:/xampp/htdocs/hybody/resources/img/imagen/proveedores/'.$_FILES['img']['name'];
                                
                                if (copy($p1, $destino))
                                {
                                    //Ingresar
                                    $des =  '../../resources/img/imagen/proveedores/'.$_FILES['img']['name'];
                                    $crud->proveedorM(2,$id,$p0,$des,$p2,$p3,$p4,$p5);
                                }
                                else {
                                    $ingre = array();
                                    $var = array('val'=>2 ,'mensaje'=> 'Error al copiar imagen');
                                    array_push( $ingre,$var);
                                    $res['error'] = $ingre;
                                    header( 'Content-type: application/json');
                                }
                            }
                        }
                        else {
                            $ingre = array();
                            $var = array('val'=>2 ,'mensaje'=> 'Telefono Invalido');
                            array_push( $ingre,$var);
                            $res['error'] = $ingre;
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $ingre = array();
                        $var = array('val'=>2 ,'mensaje'=> 'No se ha elegido un estado');
                        array_push( $ingre,$var);
                        $res['error'] = $ingre;
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                }
                else {
                    $ingre = array();
                    $var = array('val'=>2 ,'mensaje'=> 'Correo invalida');
                    array_push( $ingre,$var);
                    $res['error'] = $ingre;
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Nombre y/o Contacto invalidos');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
    if ($tabla=='proveedor' && $action=="borrar") {
        $id = $_GET['id'];
        if ($id!=0 && $id>0)
        {
            $crud->proveedorB($id);
        }
    }
    /*
        Marca
    */
    if ($tabla=='marca' && $action=="leer") {
        
        $crud->marcaL();
        
    }
    if ($tabla=='marca' && $action=="agregar") {
        $p1 = $_POST['nombre'];//Nombre
        $p0 = $_POST['proveedor'];//Proveedor
        $p2 = $_POST['link'];//pagina web
        //Validar campos vacíos
        if ($p0!="" && $p1!="" && $p2!="") {
            //Validar nombre
            if ($validar->letras($p1)) {
                //validar combobox proveedor
                if ($validar->combobox($p0)) {
                    //Ingresar
                    $crud->marcaA($p0[0],$p1,$p2);
                }
                else {
                    $ingre = array();
                    $var = array('val'=>2 ,'mensaje'=> 'Correo invalida');
                    array_push( $ingre,$var);
                    $res['error'] = $ingre;
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Nombre y/o Contacto invalidos');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
    if ($tabla=='marca' && $action=="modificar") {
        $id = $_GET['id'];
        $p1 = $_POST['nombre'];//Nombre
        $p0 = $_POST['proveedor'];//Proveedor
        $p2 = $_POST['link'];//pagina web
        //Validar campos vacíos
        if ($p0!="" && $p1!="" && $p2!="") {
            //Validar nombre
            if ($validar->letras($p1)) {
                //validar combobox proveedor
                if ($validar->combobox($p0)) {
                    //Ingresar
                    $crud->marcaM($id,$p0[0],$p1,$p2);
                }
                else {
                    $ingre = array();
                    $var = array('val'=>2 ,'mensaje'=> 'No se ha elegido proveedor');
                    array_push( $ingre,$var);
                    $res['error'] = $ingre;
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Nombre invalido');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
    if ($tabla=='marca' && $action=="borrar") {
        $id = $_GET['id'];
        if ($id!=0 && $id>0)
        {
            $crud->marcaB($id);
        }
    }
    /*
        Categoria
    */
    if ($tabla=='categoria' && $action=="leer") {
        
        $crud->categoriaL();
        
    }
    if ($tabla=='categoria' && $action=="agregar") {
        $p0 = $_POST['nombre'];//Nombre
        //Validar campos vacíos
        if (!empty($p0)) {
            //Validar nombre
            if ($validar->letras($p0)) {
                //Ingresar
                $crud->categoriaA($p0);
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Nombre invalidos');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
    if ($tabla=='categoria' && $action=="modificar") {
        $id = $_GET['id'];
        $p0 = $_POST['nombre'];//Nombre
        //Validar campos vacíos
        if (!empty($p0)) {
            //Validar nombre
            if ($validar->letras($p0)) {
                //Ingresar
                $crud->categoriaM($id,$p0);
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Nombre invalidos');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
    if ($tabla=='categoria' && $action=="borrar") {
        $id = $_GET['id'];
        if ($id!=0 && $id>0)
        {
            $crud->categoriaB($id);
        }
    }
    /*
        Productos
    */
    if ($tabla=='producto' && $action=="leer") {
        
        $crud->productoL();
        
    }
    if ($tabla=='producto' && $action=="agregar") {
        $p0 = $_POST['categoria'];//Categoria
        $p1 = $_POST['marca'];//Marca
        $p2 = $_POST['nombre'];//Nombre
        $p3 = $_POST['descp'];//Descripcion
        $p4 = $_POST['precio'];//Precio
        $p5 = $_FILES['img']['tmp_name'];//Imagen
        $p6 = $_POST['cantidad'];//Cantidad
        $p7 = $_POST['estado'];//Estado
        $p8 = $_POST['ganancia'];//Ganancias


        //Validar campos vacíos
        if (!empty($p5) && !empty($p0) && !empty($p1) && !empty($p2) && !empty($p3) && !empty($p4)
        && !empty($p6) && !empty($p7) && !empty($p8)) {
            //Validar nombre
            if ($validar->letras($p2)) {
                //Validar precio
                if ($validar->precios($p4)) {
                    //Validar ganancia
                    if ($validar->precios($p8)) {
                        //Validar cantidad
                        if ($validar->numeros($p6)) {
                            //validar combobox categoria, marca, estado
                            if ($validar->combobox($p0) && $validar->combobox($p1) && $validar->combobox($p7)) {
                                
                                $destino = '/home/bhuezo/Documentos/Proyectos/hybody/resources/img/imagen/proveedores/'.$_FILES['img']['name'];
                                if (copy($p5, $destino))
                                {
                                    //Ingresar
                                    $des =  '../../resources/img/imagen/proveedores/'.$_FILES['img']['name'];
                                    $crud->productoA($p0[0],$p1[0],$p2,$p3,$p4,$des,$p6,$p7,$p8);
                                }
                                else {
                                    $ingre = array();
                                    $var = array('val'=>2 ,'mensaje'=> 'Error al copiar imagen');
                                    array_push( $ingre,$var);
                                    $res['error'] = $ingre;
                                    header( 'Content-type: application/json');
                                }
                            }
                            else {
                                    $ingre = array();
                                    $var = array('val'=>2 ,'mensaje'=> 'No se ha elegido un dato(marca, categoria,estado)');
                                    array_push( $ingre,$var);
                                    $res['error'] = $ingre;
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                            }
                        }
                        else {
                            $ingre = array();
                            $var = array('val'=>2 ,'mensaje'=> 'Cantidad no posee el formato adecuado');
                            array_push( $ingre,$var);
                            $res['error'] = $ingre;
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $ingre = array();
                        $var = array('val'=>2 ,'mensaje'=> 'Formato no valido');
                        array_push( $ingre,$var);
                        $res['error'] = $ingre;
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                }
                else {
                    $ingre = array();
                    $var = array('val'=>2 ,'mensaje'=> 'Fortmato de invalida');
                    array_push( $ingre,$var);
                    $res['error'] = $ingre;
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Nombre invalidos');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
    if ($tabla=='producto' && $action=="modificar") {
        $id = $_GET['id'];
        $p0 = $_POST['categoria'];//Categoria
        $p1 = $_POST['marca'];//Marca
        $p2 = $_POST['nombre'];//Nombre
        $p3 = $_POST['descp'];//Descripcion
        $p4 = $_POST['precio'];//Precio
        $p5 = $_FILES['img']['tmp_name'];//Imagen
        $p6 = $_POST['cantidad'];//Cantidad
        $p7 = $_POST['estado'];//Estado
        $p8 = $_POST['ganancia'];//Ganancias


        //Validar campos vacíos
        if (!empty($id) && $id!=0 && !empty($p0) && !empty($p1) && !empty($p2) && !empty($p3) && !empty($p4)
        && !empty($p6) && !empty($p7) && !empty($p8)) {
            //Validar nombre
            if ($validar->letras($p2)) {
                //Validar precio
                if ($validar->precios($p4)) {
                    //Validar ganancia
                    if ($validar->precios($p8)) {
                        //Validar cantidad
                        if ($validar->numeros($p6)) {
                            //validar combobox categoria, marca, estado
                            if ($validar->combobox($p0) && $validar->combobox($p1) && $validar->combobox($p7)) {
                                
                                if (empty($_FILES['img']['name'])) {
                                    $crud->productoM(1,$id,$p0[0],$p1[0],$p2,$p3,$p4,"",$p6,$p7,$p8);
                                }
                                else {
                                    $destino = '/home/bhuezo/Documentos/Proyectos/hybody/resources/img/imagen/proveedores/'.$_FILES['img']['name'];
                                    if (copy($p5, $destino))
                                    {
                                        //Ingresar
                                        $des =  '../../resources/img/imagen/proveedores/'.$_FILES['img']['name'];
                                        $crud->productoM(2,$id,$p0[0],$p1[0],$p2,$p3,$p4,$des,$p6,$p7,$p8);
                                    }
                                    else {
                                        $ingre = array();
                                        $var = array('val'=>2 ,'mensaje'=> 'Error al copiar imagen');
                                        array_push( $ingre,$var);
                                        $res['error'] = $ingre;
                                        header( 'Content-type: application/json');
                                    }
                                }
                            }
                            else {
                                    $ingre = array();
                                    $var = array('val'=>2 ,'mensaje'=> 'No se ha elegido un dato(marca, categoria,estado)');
                                    array_push( $ingre,$var);
                                    $res['error'] = $ingre;
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                            }
                        }
                        else {
                            $ingre = array();
                            $var = array('val'=>2 ,'mensaje'=> 'Cantidad no posee el formato adecuado');
                            array_push( $ingre,$var);
                            $res['error'] = $ingre;
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $ingre = array();
                        $var = array('val'=>2 ,'mensaje'=> 'Formato no valido');
                        array_push( $ingre,$var);
                        $res['error'] = $ingre;
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                }
                else {
                    $ingre = array();
                    $var = array('val'=>2 ,'mensaje'=> 'Fortmato de invalida');
                    array_push( $ingre,$var);
                    $res['error'] = $ingre;
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Nombre invalidos');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
    if ($tabla=='producto' && $action=="borrar") {
        $id = $_GET['id'];
        if ($id!=0 && $id>0)
        {
            $crud->productoB($id);
        }
    }
    /*
        Pedidos
    */
    if ($tabla=='pedido' && $action=="leer") {
        
        $crud->pedidoL();
        
    }
    /*
    if ($tabla=='pedido' && $action=="agregar") {
        $p0 = $_POST['cliente'];//Id Cliente
        $p1 = $_POST['precio'];//Precio
        $p2 = $_POST['fecha'];//Fecha
        $p3 = $_POST['estado'];//Estado


        //Validar campos vacíos
        if (!empty($p5) && !empty($p0) && !empty($p1) && !empty($p2) && !empty($p3)) {
            //Validar precio
            if ($validar->precios($p1)) {
                //Validar fecha
                if ($validar->fechas($p2)) {
                    //Ingresar
                    $crud->pedidoA($p0,$p1,$p2,$p3);
                            
                }
                else {
                    $ingre = array();
                    $var = array('val'=>2 ,'mensaje'=> 'Formato de fecha no valido');
                    array_push( $ingre,$var);
                    $res['error'] = $ingre;
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Precio invalidos');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
    if ($tabla=='pedido' && $action=="modificar") {
        $id = $_GET['id'];
        $p0 = $_POST['cliente'];//Id Cliente
        $p1 = $_POST['precio'];//Precio
        $p2 = $_POST['fecha'];//Fecha
        $p3 = $_POST['estado'];//Estado


        //Validar campos vacíos
        if (!empty($p5) && !empty($p0) && !empty($p1) && !empty($p2) && !empty($p3)) {
            //Validar precio
            if ($validar->precios($p1)) {
                //Validar fecha
                if ($validar->fechas($p2)) {
                    //Ingresar
                    $crud->pedidoM($id,$p0,$p1,$p2,$p3);
                            
                }
                else {
                    $ingre = array();
                    $var = array('val'=>2 ,'mensaje'=> 'Formato de fecha no valido');
                    array_push( $ingre,$var);
                    $res['error'] = $ingre;
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
            }
            else {
                $ingre = array();
                $var = array('val'=>2 ,'mensaje'=> 'Precio invalidos');
                array_push( $ingre,$var);
                $res['error'] = $ingre;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        else {
            $ingre = array();
            $var = array('val'=>2 ,'mensaje'=> 'Valores Vacios');
            array_push( $ingre,$var);
            $res['error'] = $ingre;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }*/
    if ($tabla=='pedido' && $action=="borrar") {
        $id = $_GET['id'];
        if ($id!=0 && $id>0)
        {
            $crud->pedidoB($id);
        }
    }
    if ($tabla=='detalle' && $action=="leer") {
        $id = $_GET['id'];
        if ($id!=0 && $id>0)
        {
            $crud->dpedidoL($id);
        }
    }
?>