<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {  
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");  
        header('Access-Control-Allow-Cresentials: true');  
        header('Access-Control-Max-Age: 86400');   
    }  
    
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {  
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))  
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");  
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))  
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");  
    }
require_once('../libraries/pagos/config.php');
require_once('../libraries/pagos/Pagadito.php');
include('../models/CRUD_cajas.php');
include('../helpers/validaciones.php');

$val = new Validacion();
$crud = new Cajas();

if (isset($_GET["token2"]) && $_GET["token2"] != ""&&isset($_GET["lal"]) && $_GET["lal"] != "") {

    $Pagadito = new Pagadito(UID, WSK);

    if (SANDBOX) {
        $Pagadito->mode_sandbox_on();
    }

    if ($Pagadito->connect()) {

        if ($Pagadito->get_status($_GET["token2"])) {
            /*
             * Luego validamos el estado de la transacción, consultando el
             * estado devuelto por la API.
             */
            switch($Pagadito->get_rs_status())
            {
                case "COMPLETED":
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);

                            
                            $message = $Pagadito->get_rs_reference();
                            $crud->pagar($_GET["lal"],$id,$message);
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                    break;
                
                case "REGISTEres":
                    $res['status'] = 0;
                    $res['message'] = "La transacción fue cancelada";
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                    break;
                
                case "VERIFYING":
                    $res['status'] = 1;
                    $res['message'] = "Su pago esta en validacion, NAP: ".$Pagadito->get_rs_reference()."
                    Fecha: ".$Pagadito->get_rs_date_trans();
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                    break;
                
                case "REVOKED":
                    $res['status'] = 0;
                    $res['message'] = "La transacción fue denegada";
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                    break;
                
                case "FAILED":
                    $res['status'] = 0;
                    $res['message'] = "La transacción fallo";
                default:
                    $res['status'] = 0;
                    $res['message'] = "La transacción no fue realizada";
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                    break;
            }
        } else {
            $res['status'] = 0;
            switch($Pagadito->get_rs_code())
            {
                case "PG2001":
                    $res['message'] = $Pagadito->get_rs_code().": Datos Incompletos";
                case "PG3002":
                    $res['message'] = $Pagadito->get_rs_code().": Error!!";
                case "PG3003":
                    $res['message'] = $Pagadito->get_rs_code().": No se ha registrado transacción!!";
                default:
                    $res['message'] = $Pagadito->get_rs_code().": ".$Pagadito->get_rs_message();
                    break;
            }
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    } else {
        $res['status'] = 0;
        switch($Pagadito->get_rs_code())
        {
            case "PG2001":
                $res['message'] = $Pagadito->get_rs_code().": Datos Incompletos";
            case "PG3001":
            $res['message'] = $Pagadito->get_rs_code().": Problemas cn la conexion";
            case "PG3002":
            $res['message'] = $Pagadito->get_rs_code().": Error!!";
            case "PG3003":
            $res['message'] = $Pagadito->get_rs_code().": No se ha registrado transacción!!";
            case "PG3005":
            $res['message'] = $Pagadito->get_rs_code().": Conexion desactivada";
            case "PG3006":
            $res['message'] = $Pagadito->get_rs_code().": Se ha execedido y algo ha salido mal";
            default:
                $res['message'] = $Pagadito->get_rs_code().": ".$Pagadito->get_rs_message();
                break;
        }
        header( 'Content-type: application/json');
        echo json_encode($res);
    }
} else {
    $res['status'] = 0;
    $res['message'] = "No se ha seteado el token";
    header( 'Content-type: application/json');
    echo json_encode($res);
}

?>