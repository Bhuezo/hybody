<?php

    
    if (isset($_SERVER['HTTP_ORIGIN'])) {  
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");  
        header('Access-Control-Allow-Credentials: true');  
        header('Access-Control-Max-Age: 86400');   
    }  

    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {  

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))  
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");  

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))  
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");  
    }
    include('../models/CRUD_cliente.php');
    include('../helpers/validaciones.php');

    $crud = new Cliente();
    $val = new Validacion();

    //echo password_hash('123', PASSWORD_DEFAULT);
    $action = 'iniciar';
    $privilege = 'public';


    //Valida que accion se hara
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }
    if (isset($_GET['privilege'])) {
        $privilege = $_GET['privilege'];
    }
    /*
        ____________________________________________________________________________________
        | Este es el switch que se encarga de validar de qeu parte de la api se llama si es|
        |                         desde el dashboard o desde el public                     |
        ------------------------------------------------------------------------------------
    */

    switch ($privilege) {
        #Este es el case publico aqui estan las apis para el publico
        case 'public': 
            switch ($action) {
                #Esta accion es para la validadcion de sesion
                #Por medio del token que se acumula en el sessionstorage

                case 'verificar':
                    #Se valida si se manda el token
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);
                            #Se manda el token al modelo
                            $crud->verificar( $id );
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'login':
                    if (isset($_POST['correo']) && isset($_POST['correo'])){
                        $correo = $_POST['correo'];
                        $contra = $_POST['contra'];
                        if (!empty($correo) && !empty($contra)) {
                            if ($val->correos($correo)){
                                $crud->iniciar($correo,$contra);
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'Correo invalido';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Valores Vacios';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = 'Valores no setiados';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'recuperar':
                    $correo = $_POST['correo'];
            
                    $crud->recuperar($correo);
                break;
                case 'imagen':
                    if (isset($_GET['token'])) {
                        $id =  $val->tokens($_GET['token']);
                        if (file_exists($_FILES['img']['tmp_name'])) {
                            if(!empty( $_FILES['img']['tmp_name'] )) {
                                $img = $_FILES['img']['tmp_name'];
                                $destino = '../../resources/img/imagen/clientes';
                                if ( file_exists ($destino) ) {
                                    $destino = '../../resources/img/imagen/clientes/'.$_FILES['img']['name'];
                                    if (copy($img, $destino)) {
                                        //Ingresar
                                        $des =  'http://localhost/hybody/resources/img/imagen/clientes/'.$_FILES['img']['name'];
                                        $crud->setImg($des,$id);
                                    }
                                    else {
                                        $res['status'] = 0;
                                        $res['message'] = 'Error al intentar guardar imagen';
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }
                                }
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'No ha seleccionado ninguna imagen nueva';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Imagen no seteada';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 2;
                        $res['message'] = 'No posee session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
            
                break;
                case 'general':
                    if ( isset($_GET['token']) ) {
                        $id =  $val->tokens($_GET['token']);

                        if ( isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['correo'])
                        && isset($_POST['telefono'])) {
                            $p0 = $_POST['nombre'];
                            $p1 = $_POST['apellido'];
                            $p2 = $_POST['correo'];
                            $p3 = $_POST['telefono'];

                            if ( !empty($p0) && !empty($p1) && !empty($p2) && !empty($p3) ) {

                                if ($val->correos($p2)) {

                                    if ($val->telefonos($p3)) {
                                        $crud->setGeneral($id,$p0,$p1,$p2,$p3);
                                    }
                                    else {
                                        $res['status'] = 0;
                                        $res['message'] = 'Teléfono no valido';
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }

                                }
                                else {
                                    $res['status'] = 0;
                                    $res['message'] = 'Correo no valido';
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }

                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'Datos vacíos';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Falta setear dato';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }

                    }
                    else {
                        $res['status'] = 2;
                        $res['message'] = 'No posee session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                
                break;
                case 'info':
                
                    if ( isset($_GET['token']) ) {
                        $id =  $val->tokens($_GET['token']);
                        $D = json_decode(file_get_contents('php://input'), true);
                        if ( isset($D) ) {

                            $p0 = $D['data']['genero'];
                            $p1 = $D['data']['pais'];
                            $p2 = $D['data']['departamento'];
                            $p3 = $D['data']['ciudad'];
                            $p4 = $D['data']['direccion'];
                            $p5 = $D['data']['postal'];
                            $p6 = $D['data']['fecha'];

                            if ( !empty($p0) && !empty($p1) && !empty($p2) && !empty($p3) &&
                            !empty($p4) && !empty($p5) && !empty($p6) ) {

                                if ($val->numeros($p5)) {

                                    if ($val->fechas($p6)) {
                                        $crud->setInfo($id,$p0,$p1,$p2,$p3,$p4,$p5,$p6);
                                    }
                                    else {
                                        $res['status'] = 0;
                                        $res['message'] = 'Fecha no valida';
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }

                                }
                                else {
                                    $res['status'] = 0;
                                    $res['message'] = 'Postal no valido';
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }

                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'Datos vacíos';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Falta setear dato';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }

                    }
                    else {
                        $res['status'] = 2;
                        $res['message'] = 'No posee session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'segurity':
                if ( isset($_GET['token']) ) {
                    $id =  $val->tokens($_GET['token']);

                    if ( isset($_POST['contra']) ) {
                        $p0 = $_POST['contra'];

                        if ( !empty($p0) ) {

                            $crud->setSeguridad($id,$p0);

                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Datos vacíos';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = 'Falta setear dato';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }

                }
                else {
                    $res['status'] = 2;
                    $res['message'] = 'No posee session';
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
                break;
                case 'registrar':
                
                    $D = json_decode(file_get_contents('php://input'), true);
                    if ( isset($D) ) {
                        $p0 = $D['data']['nombre'];
                        $p1 = $D['data']['apellido'];
                        $p2 = $D['data']['genero'];
                        $p3 = $D['data']['correo'];
                        $p4 = $D['data']['contra'];
                        $p5 = $D['data']['pais'];
                        $p6 = $D['data']['fecha'];
                        $p7 = $D['data']['ciudad'];
                        $p8 = $D['data']['departamento'];
                        $p9 = $D['data']['postal'];
                        $p10 = $D['data']['telefono'];
                        $p11 = $D['data']['direccion'];

                        if ( !empty($p0) && !empty($p1) && !empty($p2) && !empty($p3) &&
                        !empty($p4) && !empty($p5) && !empty($p6) && !empty($p7)&& !empty($p8)
                        && !empty($p9) && !empty($p10) && !empty($p11) ) {

                            if ($val->numeros($p10)) {

                                if ($val->correos($p3)) {
                                    if ($val->numeros($p9)) {
                                        if ($val->fechas($p6)) {
                                            $crud->registrar($p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$p11);
                                        }
                                        else {
                                            $res['status'] = 0;
                                            $res['message'] = 'Fecha no valida';
                                            header( 'Content-type: application/json');
                                            echo json_encode($res);
                                        }
                                    }
                                    else {
                                        $res['status'] = 0;
                                        $res['message'] = 'Postal no valida';
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }
                                }
                                else {
                                    $res['status'] = 0;
                                    $res['message'] = 'Correo no valida';
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }

                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'Telefono no valido';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }

                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Datos vacíos';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = 'Falta setear dato';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }

                break;
            }
        break;
        case 'private':
            switch ($action) {
                case 'consulta':
                    $crud->getClientex();
                break;
                case 'crear':
                    $D = json_decode(file_get_contents('php://input'), true);
                    if ( !empty($D['data']['nombre']) && !empty($D['data']['apellido']) && !empty($D['data']['genero']) && !empty($D['data']['direccion']) &&
                    !empty($D['data']['fecha']) && !empty($D['data']['correo']) && !empty($D['data']['contra']) && !empty($D['data']['pais']) &&
                    !empty($D['data']['departamento']) && !empty($D['data']['ciudad']) && !empty($D['data']['tipo']) && !empty($D['data']['telefono']) &&
                    !empty($D['data']['postal']) ) {
                        if ($val->correos($D['data']['correo'])) {
                            if ($val->fechas($D['data']['fecha'])) {
                                if ($val->numeros($D['data']['telefono'])) {
                                    if ($val->numeros($D['data']['postal'])) {
                                        if ($val->numeros($D['data']['tipo'])) {
                                            $crud->setClientex($D['data']['nombre'],$D['data']['apellido'],$D['data']['genero'],$D['data']['correo'],
                                            password_hash($D['data']['contra'], PASSWORD_DEFAULT),$D['data']['pais'],$D['data']['fecha'],
                                            $D['data']['departamento'],$D['data']['ciudad'],$D['data']['postal'],$D['data']['telefono'],$D['data']['direccion'],
                                            $D['data']['tipo'] );
                                        }
                                        else {
                                            $res['status'] = 0;
                                            $res['message'] = 'Privilegio no valído';
                                            header( 'Content-type: application/json');
                                            echo json_encode($res);
                                        }
                                    }
                                    else {
                                        $res['status'] = 0;
                                        $res['message'] = 'Privilegio no valído';
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }
                                }
                                else {
                                    $res['status'] = 0;
                                    $res['message'] = 'Telefono no valído';
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'Fecha no valído';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Correo no valído';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = 'Datos vacíos: '.$D['data']['apellido'];
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'modificar':
                    $D = json_decode(file_get_contents('php://input'), true);
                    if ( !empty($D['data']['nombre']) && !empty($D['data']['apellido']) && !empty($D['data']['genero']) && 
                    !empty($D['data']['direccion']) && !empty($D['data']['fecha']) && !empty($D['data']['correo'])  && 
                    !empty($D['data']['pais']) && !empty($D['data']['departamento']) && !empty($D['data']['ciudad']) && 
                    !empty($D['data']['postal']) && !empty($D['data']['tipo']) ) {
                        if ($val->correos($D['data']['correo'])) {
                            if ($val->fechas($D['data']['fecha'])) {
                                if ($val->numeros($D['data']['telefono'])) {
                                    if ($val->numeros($D['data']['tipo'])) {
                                        $crud->modClientex($_GET['id'],$D['data']['nombre'],$D['data']['apellido'],$D['data']['genero'],$D['data']['correo'],
                                        password_hash($D['data']['contra'], PASSWORD_DEFAULT),$D['data']['pais'],$D['data']['fecha'],
                                        $D['data']['departamento'],$D['data']['ciudad'],$D['data']['postal'],$D['data']['telefono'],$D['data']['direccion'],$D['data']['tipo']);
                                    }
                                    else {
                                        $res['status'] = 0;
                                        $res['message'] = 'Privilegio no valído';
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }
                                }
                                else {
                                    $res['status'] = 0;
                                    $res['message'] = 'Telefono no valído';
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'Fecha no valído';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Correo no valído';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = 'Datos vacíos';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'img':
                    if (isset($_GET['id']) && $_GET['id'] > 0) {
                        if (file_exists($_FILES['img']['tmp_name'])) {
                            if(!empty( $_FILES['img']['tmp_name'] )) {
                                $img = $_FILES['img']['tmp_name'];
                                $destino = '../../resources/img/imagen/clientes';
                                if ( file_exists ($destino) ) {
                                    if ($_FILES['img']['type'] == "image/png") {
                                        $destino = '../../resources/img/imagen/clientes/'.$_GET['id'].'.png';
                                        if (copy($img, $destino)) {
                                            //Ingresar
                                            $id = $_GET['id'];
                                            $des =  'http://localhost/hybody/resources/img/imagen/clientes/'.$_GET['id'].'.png';
                                            $crud->imgClientex($id,$des);
                                        }
                                        else {
                                            $res['status'] = 0;
                                            $res['message'] = 'Error al intentar guardar imagen';
                                            header( 'Content-type: application/json');
                                            echo json_encode($res);
                                        }
                                    }
                                    else {
                                        $res['status'] = 0;
                                        $res['message'] = 'Imagen no valida';
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }
                                }
                                else {
                                    $res['status'] = 0;
                                    $res['message'] = 'La dirección no existe';
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'No ha seleccionado ninguna imagen nueva';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Imagen no seteada';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = 'Id de usuario no seteada';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
            }
        break;
    }



    
?>