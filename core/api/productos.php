<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {  
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");  
        header('Access-Control-Allow-Credentials: true');  
        header('Access-Control-Max-Age: 86400');   
    }  
    
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {  
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))  
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");  
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))  
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");  
    }
    /*
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Allow: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }*/
    include('../models/CRUD_productos.php');
    session_start();
    #Mandar a llamar clase
    $crud = new Productos();

    #Variable quen poseera el valor del privilegio
    $privilegio = 'public';
    #Variable quen poseera el valor de la accion
    $action = 'leer';
    #Validar optencion de valor
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }
    if (isset($_GET['privilege'])) {
        $privilegio = $_GET['privilege'];
    }

    switch($privilegio){
        case 'public':
            switch($action){
                case 'leer':
                    $crud->leer();
                break;
                case 'leerProducto':
                    if (isset($_GET['id'])) {
                        $crud->leerProducto($_GET['id']);
                    }else{
                        $res['status'] = 0;
                        $res['message'] = 'Falta id del producto';
                        header('Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'categoria':
                    $crud->leerCategorias();
                break;
                case 'marca':
                    $crud->leerMarcas();
                break;
            }
        break;
        case 'private':
            switch ($action) {
                case 'consulta':
                    $crud->getProductox();
                break;
                case 'crear':
                    $D = json_decode(file_get_contents('php://input'), true);
                    if ( !empty($D['data']['categoria']) && !empty($D['data']['marca']) && !empty($D['data']['nombre']) && !empty($D['data']['precio']) &&
                    !empty($D['data']['cantidad']) && !empty($D['data']['estado']) && !empty($D['data']['ganancias']) && !empty($D['data']['comision']) ) {
                        if ($val->precios($D['data']['precio'])) {
                            $crud->setProductox($D['data']['categoria'],$D['data']['marca'],$D['data']['nombre'],$D['data']['precio'],
                            $D['data']['cantidad'],$D['data']['estado'],$D['data']['ganacias'],$D['data']['comision'] );
                                    
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Correo no valído';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = 'Datos vacíos: '.$D['data']['apellido'];
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'modificar':
                    $D = json_decode(file_get_contents('php://input'), true);
                    if ( !empty($D['data']['categoria']) && !empty($D['data']['marca']) && !empty($D['data']['nombre']) && !empty($D['data']['precio']) &&
                    !empty($D['data']['cantidad']) && !empty($D['data']['estado']) && !empty($D['data']['ganancias']) && !empty($D['data']['comision']) ) {
                        if ($val->precios($D['data']['precio'])) {
                            $crud->modProductox($_GET['id'],$D['data']['categoria'],$D['data']['marca'],$D['data']['nombre'],$D['data']['precio'],
                            $D['data']['cantidad'],$D['data']['estado'],$D['data']['ganacias'],$D['data']['comision'] );
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Correo no valído';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = 'Datos vacíos';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'img':
                    if (isset($_GET['id']) && $_GET['id'] > 0) {
                        if (file_exists($_FILES['img']['tmp_name'])) {
                            if(!empty( $_FILES['img']['tmp_name'] )) {
                                $img = $_FILES['img']['tmp_name'];
                                $destino = '../../resources/img/imagen/usuarios';
                                if ( file_exists ($destino) ) {
                                    if ($_FILES['img']['type'] == "image/png") {
                                        $destino = '../../resources/img/imagen/usuarios/'.$_GET['id'].'.png';
                                        if (copy($img, $destino)) {
                                            //Ingresar
                                            $id = $_GET['id'];
                                            $des =  'http://localhost/hybody/resources/img/imagen/usuarios/'.$_GET['id'].'.png';
                                            $crud->imgProductox($id,$des);
                                        }
                                        else {
                                            $res['status'] = 0;
                                            $res['message'] = 'Error al intentar guardar imagen';
                                            header( 'Content-type: application/json');
                                            echo json_encode($res);
                                        }
                                    }
                                    else {
                                        $res['status'] = 0;
                                        $res['message'] = 'Imagen no valida';
                                        header( 'Content-type: application/json');
                                        echo json_encode($res);
                                    }
                                }
                                else {
                                    $res['status'] = 0;
                                    $res['message'] = 'La dirección no existe';
                                    header( 'Content-type: application/json');
                                    echo json_encode($res);
                                }
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'No ha seleccionado ninguna imagen nueva';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Imagen no seteada';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        $res['status'] = 0;
                        $res['message'] = 'Id de usuario no seteada';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'productG':
                    $crud->getProductosC();
                break;
            }
        break;
    }
?>