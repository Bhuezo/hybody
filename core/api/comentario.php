<?php

    
    if (isset($_SERVER['HTTP_ORIGIN'])) {  
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");  
        header('Access-Control-Allow-Credentials: true');  
        header('Access-Control-Max-Age: 86400');   
    }  
      
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {  
      
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))  
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");  
      
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))  
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");  
    }  
    include('../models/CRUD_productos.php');
    include('../models/CRUD_comentarios.php');
    $ap = new Productos();
    $ap2 = new Comentarios();

    $action = 'comentario';
    $buscar='';
    //Valida que accion se hara
    if (isset($_GET['producto'])) {
        $buscar = $_GET['producto'];
    }
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }

    //Ejecutan la accion que se a escogido

    if ($action=='comentario') {
        $ap2->leer($buscar);
    }
    if ($action=='ingresarc') {
        $D = json_decode(file_get_contents('php://input'), true);
        $id = $buscar;
        $comen = $D['comentario'];
        $cali = $D['calificacion'];
        $ap2->insertar($comen,$cali,$id);
    }
    if ($action=='editar') {
        $D = json_decode(file_get_contents('php://input'), true);
        $id = $D['id'];
        $comen = $D['comentario'];
        $cali = $D['calificacion'];
        $ap2->modificar($comen,$cali,$id);
    }
?>