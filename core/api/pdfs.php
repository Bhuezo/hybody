<?php
    if (isset($_SERVER['HTTP_ORIGIN'])) {  
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");  
        header('Access-Control-Allow-Cresentials: true');  
        header('Access-Control-Max-Age: 86400');   
    }  
    
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {  
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))  
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");  
    
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))  
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");  
    }
    //Se manda a traer los distintos modelos y librerias
    require_once('PDF.php');
    require_once('../models/CRUD_pedidos.php');
    require_once('../models/CRUD_usuarios.php');
    require_once('../models/CRUD_productos.php');
    include('../helpers/validaciones.php');

    $val = new Validacion();
    $crud = new Pedidos();
    $crud2 = new Usuarios();
    $crud3 = new Productos();
    //La P sirve para poner el pdf vertical, sirve para las medidas mm es milimetro y letter es para la pagina tamaño carta
	$pdf = new PDF('P','mm','letter');
    $pdf->SetMargins(25,25,25);
    $pdf->SetAutoPageBreak(true,25);
    $action = 'ticket';
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }
    $privilege = 'public';
    if (isset($_GET['privilege'])) {
        $privilege = $_GET['privilege'];
    }
    
    switch ($privilege) {
        case 'public':
            switch ($action) {
                case 'ticket':
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);
                            #Se manda el token al modelo
                            if (isset($_GET['lal']) && $_GET['lal'] != null && isset($_GET['nap']) && $_GET['nap'] != null) {
                                //Variables
                                $title = 'Hybody Ticket';
                                $miCabecera = array( 'Nombre', 'Cantidad', 'Precio', 'Comision', 'Precio Total');
                                $datosReporte = $crud->getPedido($_GET['lal'],$id);
                                $total = 0;
                                for ($i=0; $i < count($datosReporte); $i++) { 
                                    $total += $datosReporte[$i]['precio_total'];
                                }
                                $pdf->SetTitle($title);
                                $pdf->AddPage();
                                $pdf->AliasNbPages();
                                
                                $pdf->SetFont('Arial','I',10);
                                $pdf->Cell(40,25,'Id: '.$datosReporte[0]['id']);
                                $pdf->Ln(5);
                                $pdf->Cell(40,25,'NAP: '.$_GET['nap']);
                                $pdf->Ln(5);
                                $pdf->Cell(40,25,'Cliente: '.$datosReporte[0]['cliente']);
                                $pdf->Ln(5);
                                $pdf->Cell(40,25,'Fecha: '.$datosReporte[0]['fecha']);
                                $pdf->Ln(5);
                                $pdf->SetFont('Arial','I',16);
                                $w = $pdf->GetStringWidth('Productos')+6;
                                $pdf->SetX((210-$w)/2);
                            
                                $pdf->Cell($w,40,'Productos');
                                $pdf->tablaHorizontal($miCabecera, $datosReporte);
                                $pdf->SetFont('Arial','B',15);
                                $pdf->Cell(40,25,'Total: '.$total);
                                
                                $pdf->Output();
                            }
                            else {
                                $res['status'] = 0;
                                $res['message'] = 'Falta comprobante';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
            }
        break;
        case 'private':

            include('../helpers/conexion.php');
            $app = new Conexion(); 
            switch ($action) {
                case 'product':
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);

                            if (isset($_GET['ano'])) {
                                date_default_timezone_set('America/El_Salvador');
                                $g = date('d-m-Y g:i:s A');

                                $title = 'Hybody inventario';
                                $miCabecera = array( 'ID', 'Nombre', 'Cantidad', 'Precio', 'Comision', 'Ganancias');
                                #Mandamos a llamar los datos del empleado que crea el reporte
                                $empleado = $crud2->getEmpleado($id,$app);
                                #mandamos a traer los datos que van en el reporte
                                $datosReporte = $crud3->getProductos($_GET['ano'],$app);
                                $total = 0;
                                for ($i=0; $i < count($datosReporte); $i++) { 
                                    $total ++;
                                }
                                $pdf->SetTitle($title);
                                $pdf->AddPage();
                                $pdf->AliasNbPages();
                                $pdf->SetFont('Arial','B',10);
                                $pdf->Cell(40,25,'ID: '.$empleado[0]->id);
                                $pdf->Ln(5);
                                $pdf->Cell(40,25,'Empleado: '.$empleado[0]->nombre.' '.$empleado[0]->apellido);
                                $pdf->Ln(5);
                                $pdf->Cell(40,25,'Fecha: '.$g);
                                $pdf->Ln(5);
                                $pdf->tablaHorizontal($miCabecera, $datosReporte,4);
                                $pdf->SetFont('Arial','B',15);
                                $pdf->Cell(20,25,'Total de productos diferentes: '.$total);
                                
                                $pdf->Output();
                            }
                            else {
                                #En vaso de que no se haya seteado nada se mandara que no hay
                                #Session alguna
                                $res['status'] = 0;
                                $res['message'] = 'No ha maandado año';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'productC':
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);

                            if (isset($_GET['ano']) && isset($_GET['id'])) {
                                date_default_timezone_set('America/El_Salvador');
                                $g = date('d-m-Y g:i:s A');

                                $miCabecera = array( 'Nombre', 'Cantidad');
                                #Mandamos a llmar los datos del empleado que crea el reporte
                                $empleado = $crud2->getEmpleado($id,$app);
                                #mandamos a traer los datos que van en el reporte
                                $datosReporte = $crud3->getProductosC2($_GET['id'],$_GET['ano'],$app);
                                $title = 'Hybody Ventas De "'.$datosReporte[0]['producto'].'"';

                                $total = 0;
                                for ($i=0; $i < count($datosReporte); $i++) { 
                                    $total += $datosReporte[$i]['cantidad'];
                                }
                                $pdf->SetTitle($title);
                                $pdf->AddPage();
                                $pdf->AliasNbPages();
                                $pdf->SetFont('Arial','B',10);
                                $pdf->Cell(40,25,'ID: '.$empleado[0]->id);
                                $pdf->Ln(5);
                                $pdf->Cell(40,25,'Empleado: '.$empleado[0]->nombre.' '.$empleado[0]->apellido);
                                $pdf->Ln(5);
                                $pdf->Cell(40,25,'Fecha: '.$g);
                                $pdf->Ln(5);
                                $pdf->tablaHorizontal($miCabecera, $datosReporte,5);
                                $pdf->SetFont('Arial','B',15);
                                $pdf->Cell(20,25,'Total de productos vendidos: '.$total);
                                
                                $pdf->Output();
                            }
                            else {
                                #En vaso de que no se haya seteado nada se mandara que no hay
                                #Session alguna
                                $res['status'] = 0;
                                $res['message'] = 'No ha maandado año';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'ventas':
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);

                            if (isset($_GET['ano'])) {
                                date_default_timezone_set('America/El_Salvador');
                                $g = date('d-m-Y g:i:s A');

                                $title = 'Hybody ventas mensuales';
                                $miCabecera = array( 'Ganacias', 'Mes', 'Año');
                                #Mandamos a llmar los datos del empleado que crea el reporte
                                $empleado = $crud2->getEmpleado($id,$app);
                                #mandamos a traer los datos que van en el reporte
                                $datosReporte = $crud->getPedidoA($_GET['ano'],$app);
                                $total = 0;
                                for ($i=0; $i < count($datosReporte); $i++) { 
                                    $total += $datosReporte[$i]['precio'];
                                }
                                $pdf->SetTitle($title);
                                $pdf->AddPage();
                                $pdf->AliasNbPages();
                                $pdf->SetFont('Arial','B',10);
                                $pdf->Cell(40,25,'ID: '.$empleado[0]->id);
                                $pdf->Ln(5);
                                $pdf->Cell(40,25,'Empleado: '.$empleado[0]->nombre.' '.$empleado[0]->apellido);
                                $pdf->Ln(5);
                                $pdf->Cell(40,25,'Fecha: '.$g);
                                $pdf->Ln(5);
                                $pdf->tablaHorizontal($miCabecera, $datosReporte,2);
                                $pdf->SetFont('Arial','B',15);
                                $pdf->Cell(20,25,'Total: '.$total);
                                
                                $pdf->Output();
                            }
                            else {
                                #En caso de que no se haya seteado nada se mandara que no hay
                                #Session alguna
                                $res['status'] = 0;
                                $res['message'] = 'No ha maandado año';
                                header( 'Content-type: application/json');
                                echo json_encode($res);
                            }
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'ventasPY':
                    if (isset($_GET['token'])) {
                        #Se valida el numero de caracteres
                        if (strlen($_GET['token']) > 13){

                            $token = $_GET['token'];
                            #Se obtiene el valor del token
                            $id =  $val->tokens($token);
                            date_default_timezone_set('America/El_Salvador');
                            $g = date('d-m-Y g:i:s A');
                            $title = 'Hybody Productos vendido Hoy';
                            $miCabecera = array( 'Producto', 'Cantidad');
                            #Mandamos a llmar los datos del empleado que crea el reporte
                            $empleado = $crud2->getEmpleado($id,$app);
                            #mandamos a traer los datos que van en el reporte
                            $datosReporte = $crud->getPedidoBY($app);
                            $total = 0;
                            for ($i=0; $i < count($datosReporte); $i++) { 
                                $total += $datosReporte[$i]->cantidad;
                            }
                            $pdf->SetTitle($title);
                            $pdf->AddPage();
                            $pdf->AliasNbPages();
                            $pdf->SetFont('Arial','B',10);
                            $pdf->Cell(40,25,'ID: '.$empleado[0]->id);
                            $pdf->Ln(5);
                            $pdf->Cell(40,25,'Empleado: '.$empleado[0]->nombre.' '.$empleado[0]->apellido);
                            $pdf->Ln(5);
                            $pdf->Cell(40,25,'Fecha: '.$g);
                            $pdf->Ln(5);
                            $pdf->tablaHorizontal($miCabecera, $datosReporte,3);
                            $pdf->SetFont('Arial','B',15);
                            $pdf->Cell(20,25,'Total: '.$total);
                            
                            $pdf->Output();
                        }
                        else{
                            #En caso de que el numero de caracteres
                            #Sea invalido se mandara un mensaje
                            $res['status'] = 0;
                            $res['message'] = 'Token no valido';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else {
                        #En vaso de que no se haya seteado nada se mandara que no hay
                        #Session alguna
                        $res['status'] = 0;
                        $res['message'] = 'No hay session';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                break;
                case 'ventasC':
                if (isset($_GET['token'])) {
                    #Se valida el numero de caracteres
                    if (strlen($_GET['token']) > 13){

                        $token = $_GET['token'];
                        #Se obtiene el valor del token
                        $id =  $val->tokens($token);

                        if (isset($_GET['ano'])) {
                            date_default_timezone_set('America/El_Salvador');
                            $g = date('d-m-Y g:i:s A');
                            $title = 'Hybody Productos vendidos por Clientes';
                            $miCabecera = array( 'Clientes', 'Cantidad');
                            #Mandamos a llmar los datos del empleado que crea el reporte
                            $empleado = $crud2->getEmpleado($id,$app);
                            #mandamos a traer los datos que van en el reporte
                            $datosReporte = $crud->getPedidoC($_GET['ano'],$app);
                            $total = 0;
                            for ($i=0; $i < count($datosReporte); $i++) { 
                                $total += $datosReporte[$i]->cantidad;
                            }
                            $pdf->SetTitle($title);
                            $pdf->AddPage();
                            $pdf->AliasNbPages();
                            $pdf->SetFont('Arial','B',10);
                            $pdf->Cell(40,25,'ID: '.$empleado[0]->id);
                            $pdf->Ln(5);
                            $pdf->Cell(40,25,'Empleado: '.$empleado[0]->nombre.' '.$empleado[0]->apellido);
                            $pdf->Ln(5);
                            $pdf->Cell(40,25,'Fecha: '.$g);
                            $pdf->Ln(5);
                            $pdf->tablaHorizontal($miCabecera, $datosReporte,3);
                            $pdf->SetFont('Arial','B',15);
                            $pdf->Cell(20,25,'Total: '.$total);
                            
                            $pdf->Output();
                        }
                        else {
                            #En vaso de que no se haya seteado nada se mandara que no hay
                            #Session alguna
                            $res['status'] = 0;
                            $res['message'] = 'No ha maandado año';
                            header( 'Content-type: application/json');
                            echo json_encode($res);
                        }
                    }
                    else{
                        #En caso de que el numero de caracteres
                        #Sea invalido se mandara un mensaje
                        $res['status'] = 0;
                        $res['message'] = 'Token no valido';
                        header( 'Content-type: application/json');
                        echo json_encode($res);
                    }
                }
                else {
                    #En vaso de que no se haya seteado nada se mandara que no hay
                    #Session alguna
                    $res['status'] = 0;
                    $res['message'] = 'No hay session';
                    header( 'Content-type: application/json');
                    echo json_encode($res);
                }
            break;
            }
        break;
    }
?>