<?php


    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Allow: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }
    include('../models/CRUD_sobre.php');
    session_start();
    $ap = new Sobre();
    $action = 'leer';
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }
    if ($action=='leer') {
        $ap->provedor();
    }
?>