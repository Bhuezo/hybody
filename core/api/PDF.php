<?php
require('fpdf.php');
 
class PDF extends FPDF
{
    // Page header
    function Header()
    {
        global $title;
        // Logo
        $this->Image('http://localhost/hybody/resources/img/imagen/tienda/logo/logo.png',25,25,30);
        $this->SetFont('Arial','B',15);
        $w = $this->GetStringWidth($title)-9;
        $this->SetX((210-$w)/2);
        $this->SetDrawColor(256,256,256);
        //Aqui cambiamos el color del header
        $this->SetFillColor(256,256,256);
        $this->SetTextColor(0,0,0);
        $this->SetLineWidth(1);
        
        $this->Cell($w+10,9,$title,1,1,'C',true);
        $this->Ln(10);
        // Save ordinate
        $this->y0 = $this->GetY();
    }
    
    // Page footer
    function Footer()
    {
        // Position at 2.5 cm from bottom
        $this->SetY(-25);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->Cell(0,10,'Hybody report',0,0,'C');
        $this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
    }
    function cabeceraHorizontal($cabecera,$id=1)
    {
        if ($id == 1) {
            $this->SetXY(25, 90);
        }
        if ($id == 2) {
            $this->SetXY(25, 70);
        }
        if ($id == 3) {
            $this->SetXY(25, 70);
        }
        if ($id == 4) {
            $this->SetXY(25, 90);
        }
        if ($id == 5) {
            $this->SetXY(25, 70);
        }
        $this->SetFont('Arial','B',9);
        $this->SetFillColor(256,256,256);//Fondo blanco de celda
        $this->SetTextColor(0, 0, 0); //Letra color blanco
        $ejeX = 25;
        foreach($cabecera as $fila)
        {
            if ($id == 1) {
                $this->RoundedRect($ejeX, 90, 30, 7, 0, 'FD');
                $this->CellFitSpace(30,7, utf8_decode($fila),0, 0 , 'C');
                $ejeX = $ejeX + 30;
            }
            if ($id == 2) {
                $this->RoundedRect($ejeX, 70, 55, 7, 0, 'FD');
                $this->CellFitSpace(55,7, utf8_decode($fila),0, 0 , 'C');
                $ejeX = $ejeX + 55;
            }
            if ($id == 3) {
                $this->RoundedRect($ejeX, 70, 80, 7, 0, 'FD');
                $this->CellFitSpace(80,7, utf8_decode($fila),0, 0 , 'C');
                $ejeX = $ejeX + 80;
            }
            if ($id == 4) {
                $this->RoundedRect($ejeX, 90, 28.35, 7, 0, 'FD');
                $this->CellFitSpace(28.35,7, utf8_decode($fila),0, 0 , 'C');
                $ejeX = $ejeX + 28.35;
            }
            if ($id == 5) {
                $this->RoundedRect($ejeX, 70, 80, 7, 0, 'FD');
                $this->CellFitSpace(80,7, utf8_decode($fila),0, 0 , 'C');
                $ejeX = $ejeX + 80;
            }
        }
    }
 
    function datosHorizontal($datos,$id=1)
    {
        $bandera = false; //Para alternar el relleno
        if ($id == 1) {
            $this->SetXY(25,97);
            $this->SetFont('Arial','',10);
            $this->SetFillColor(229, 229, 229); //Gris tenue de cada fila
            $this->SetTextColor(3, 3, 3); //Color del texto: Negro
            $ejeY = 97; //Aquí se encuentra la primer CellFitSpace e irá incrementando
        }
        if ($id == 2) {
            $this->SetXY(25,77);
            $this->SetFont('Arial','',10);
            $this->SetFillColor(229, 229, 229); //Gris tenue de cada fila
            $this->SetTextColor(3, 3, 3); //Color del texto: Negro
            $ejeY = 77; //Aquí se encuentra la primer CellFitSpace e irá incrementando
        }
        if ($id == 3) {
            $this->SetXY(25,77);
            $this->SetFont('Arial','',10);
            $this->SetFillColor(229, 229, 229); //Gris tenue de cada fila
            $this->SetTextColor(3, 3, 3); //Color del texto: Negro
            $ejeY = 77; //Aquí se encuentra la primer CellFitSpace e irá incrementando
        }
        if ($id == 4) {
            $this->SetXY(25,97);
            $this->SetFont('Arial','',10);
            $this->SetFillColor(229, 229, 229); //Gris tenue de cada fila
            $this->SetTextColor(3, 3, 3); //Color del texto: Negro
            $ejeY = 97; //Aquí se encuentra la primer CellFitSpace e irá incrementando
        }
        if ($id == 5) {
            $this->SetXY(25,77);
            $this->SetFont('Arial','',10);
            $this->SetFillColor(229, 229, 229); //Gris tenue de cada fila
            $this->SetTextColor(3, 3, 3); //Color del texto: Negro
            $ejeY = 77; //Aquí se encuentra la primer CellFitSpace e irá incrementando
        }
        $letra = 'D'; //'D' Dibuja borde de cada CellFitSpace -- 'FD' Dibuja borde y rellena
        foreach($datos as $fila)
        {
            //Por cada 3 CellFitSpace se crea un RoundedRect encimado
            //El parámetro $letra de RoundedRect cambiará en cada iteración
            //para colocar FD y D, la primera iteración es D
            //Solo la celda de enmedio llevará bordes, izquierda y derecha
            if($id == 1){
                //Las celdas laterales colocarlas sin borde
                $this->RoundedRect(25, $ejeY, 150, 7, 0, $letra);
                //$this->CellFitSpace(70,7, utf8_decode($fila['id_user']),0, 0 , 'L' );
                $this->CellFitSpace(30,7, utf8_decode($fila['nombre']),0, 0 , 'L' );
                $this->CellFitSpace(30,7, utf8_decode($fila['cantidad']),'LR', 0 , 'L' );
                $this->CellFitSpace(30,7, utf8_decode($fila['precio']),'LR', 0 , 'L' );
                $this->CellFitSpace(30,7, utf8_decode($fila['comision']),'LR', 0 , 'L' );
                $this->CellFitSpace(30,7, utf8_decode($fila['precio_total']),0, 0 , 'L' );
            }
            else if ($id == 2) {
                //Las celdas laterales colocarlas sin borde
                $this->RoundedRect(25, $ejeY, 165, 7, 0, $letra);
                //$this->CellFitSpace(70,7, utf8_decode($fila['id_user']),0, 0 , 'L' );
                $this->CellFitSpace(55,7, utf8_decode($fila['precio']),0, 0 , 'L' );
                $this->CellFitSpace(55,7, utf8_decode($fila['mes']),'LR', 0 , 'L' );
                $this->CellFitSpace(55,7, utf8_decode($fila['ano']),0, 0 , 'L' );
            }
            else if ($id == 3) {
                //Las celdas laterales colocarlas sin borde
                $this->RoundedRect(25, $ejeY, 160, 7, 0, $letra);
                //$this->CellFitSpace(70,7, utf8_decode($fila['id_user']),0, 0 , 'L' );
                $this->CellFitSpace(80,7, utf8_decode($fila->nombre),0, 0 , 'L' );
                $this->CellFitSpace(80,7, utf8_decode($fila->cantidad),'LR', 0 , 'L' );
            }
            if($id == 4){
                //Las celdas laterales colocarlas sin borde
                $this->RoundedRect(25, $ejeY, 170, 7, 0, $letra);
                //$this->CellFitSpace(70,7, utf8_decode($fila['id_user']),0, 0 , 'L' );
                $this->CellFitSpace(30,7, utf8_decode($fila['id']),0, 0 , 'L' );
                $this->CellFitSpace(40,7, utf8_decode($fila['nombre']),0, 0 , 'L' );
                $this->CellFitSpace(25,7, utf8_decode($fila['cantidad']),0, 0 , 'L' );
                $this->CellFitSpace(30,7, utf8_decode($fila['precio']),0, 0 , 'L' );
                $this->CellFitSpace(25,7, utf8_decode($fila['comision']),0, 0 , 'L' );
                $this->CellFitSpace(30,7, utf8_decode($fila['precio_total']),0, 0 , 'L' );
            }
            else if ($id == 5) {
                //Las celdas laterales colocarlas sin borde
                $this->RoundedRect(25, $ejeY, 160, 7, 0, $letra);
                //$this->CellFitSpace(70,7, utf8_decode($fila['id_user']),0, 0 , 'L' );
                $this->CellFitSpace(80,7, utf8_decode($fila['nombre']),0, 0 , 'L' );
                $this->CellFitSpace(80,7, utf8_decode($fila['cantidad']),'LR', 0 , 'L' );
            }
 
            $this->Ln();
            //Condición ternaria que cambia el valor de $letra
            ($letra == 'D') ? $letra = 'FD' : $letra = 'D';
            //Aumenta la siguiente posición de Y (recordar que X es fijo)
            //Se suma 7 porque cada celda tiene esa altura
            $ejeY = $ejeY + 7;
        }
    }
 
    function tablaHorizontal($cabeceraHorizontal, $datosHorizontal,$id=1)
    {
        $this->cabeceraHorizontal($cabeceraHorizontal,$id);
        $this->datosHorizontal($datosHorizontal,$id);
    }
 
    //**************************************************************************************************************
    function CellFit($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='', $scale=false, $force=true)
    {
        //Get string width
        $str_width=$this->GetStringWidth($txt);
 
        //Calculate ratio to fit cell
        if($w==0)
            $w = $this->w-$this->rMargin-$this->x;
        $ratio = ($w-$this->cMargin*2)/$str_width;
 
        $fit = ($ratio < 1 || ($ratio > 1 && $force));
        if ($fit)
        {
            if ($scale)
            {
                //Calculate horizontal scaling
                $horiz_scale=$ratio*100.0;
                //Set horizontal scaling
                $this->_out(sprintf('BT %.2F Tz ET',$horiz_scale));
            }
            else
            {
                //Calculate character spacing in points
                $char_space=($w-$this->cMargin*2-$str_width)/max($this->MBGetStringLength($txt)-1,1)*$this->k;
                //Set character spacing
                $this->_out(sprintf('BT %.2F Tc ET',$char_space));
            }
            //Override user alignment (since text will fill up cell)
            $align='';
        }
 
        //Pass on to Cell method
        $this->Cell($w,$h,$txt,$border,$ln,$align,$fill,$link);
 
        //Reset character spacing/horizontal scaling
        if ($fit)
            $this->_out('BT '.($scale ? '100 Tz' : '0 Tc').' ET');
    }
 
    function CellFitSpace($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        $this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,false,false);
    }
 
    //Patch to also work with CJK double-byte text
    function MBGetStringLength($s)
    {
        if($this->CurrentFont['type']=='Type0')
        {
            $len = 0;
            $nbbytes = strlen($s);
            for ($i = 0; $i < $nbbytes; $i++)
            {
                if (ord($s[$i])<128)
                    $len++;
                else
                {
                    $len++;
                    $i++;
                }
            }
            return $len;
        }
        else
            return strlen($s);
    }
//**********************************************************************************************
 
 function RoundedRect($x, $y, $w, $h, $r, $style = '', $angle = '1234')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' or $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2f %.2f m', ($x+$r)*$k, ($hp-$y)*$k ));
 
        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2f %.2f l', $xc*$k, ($hp-$y)*$k ));
        if (strpos($angle, '2')===false)
            $this->_out(sprintf('%.2f %.2f l', ($x+$w)*$k, ($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
 
        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2f %.2f l', ($x+$w)*$k, ($hp-$yc)*$k));
        if (strpos($angle, '3')===false)
            $this->_out(sprintf('%.2f %.2f l', ($x+$w)*$k, ($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
 
        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2f %.2f l', $xc*$k, ($hp-($y+$h))*$k));
        if (strpos($angle, '4')===false)
            $this->_out(sprintf('%.2f %.2f l', ($x)*$k, ($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
 
        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2f %.2f l', ($x)*$k, ($hp-$yc)*$k ));
        if (strpos($angle, '1')===false)
        {
            $this->_out(sprintf('%.2f %.2f l', ($x)*$k, ($hp-$y)*$k ));
            $this->_out(sprintf('%.2f %.2f l', ($x+$r)*$k, ($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }
 
    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2f %.2f %.2f %.2f %.2f %.2f c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }
} // FIN Class PDF
?>