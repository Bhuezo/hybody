<?php 
    class Pedidos
    {
        public function basex() {
            include('../helpers/conexion.php');
            $app = new Conexion(); 
            $conex = $app->connec();
            return $conex;
        }
        /*
            /////////////////////////////////////////
            |              Parte publica            |
            /////////////////////////////////////////
        */
        public function leerPedidos ($id) {
            $app2 = new Pedidos();
            $conn = $app2->basex();

            $sql = "SELECT id_pedido, fecha, token,estado,nombre,nap FROM pedido INNER JOIN cliente using(id_cliente) WHERE id_cliente = ".$id;

            $pedido = array();
            $detalle = array();
            $result = $conn->query($sql);
            if ( $result ) {
                while ( $row = $result->fetch_object()) {
                    array_push( $pedido, $row);
                }
                $sql = "SELECT detalle_pedido.id_pedido, id_detallepedido as 'id', nombre, round((producto.precio+(producto.precio*(producto.ganancias/100))),2) as 'precio',
                    ((round((producto.precio+(producto.precio*(producto.ganancias/100))),2))*detalle_pedido.cantidad) as 'precio_total',
                    producto.imagen as 'img', producto.comision as 'comision', detalle_pedido.cantidad as 'cantidad', pedido.estado, detalle_pedido.id_venta
                    FROM  pedido INNER JOIN detalle_pedido on 
                    pedido.id_pedido = detalle_pedido.id_pedido INNER JOIN producto 
                    on detalle_pedido.id_producto = producto.id_producto WHERE id_cliente= ".$id;

                $result = $conn->query($sql);
                if ( $result ) {
                    while ( $row = $result->fetch_object()) {
                        array_push( $detalle, $row);
                    }
                    $res['status'] = 1;
                }
                else {
                    
                    $res['status'] = 0;
                    $res['message'] = mysqli_error($conn);
                }
            }
            else {
                
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            //$mac = system('arp -an');
            
            $res['carrito']=$pedido;
            $res['detalle']=$detalle;
            header( 'Content-type: application/json' );
            echo json_encode($res);

        }
        public function leerPedidosP ($id) {
            $app2 = new Pedidos();
            $conn = $app2->basex();

            $sql = "SELECT SUM(((round((producto.precio+(producto.precio*(producto.ganancias/100))),2))*detalle_pedido.cantidad)) as 'total',
            MONTH(fecha) as 'mes', YEAR(fecha) as 'fecha'
            FROM pedido INNER JOIN detalle_pedido USING (id_pedido) INNER JOIN producto USING(id_producto)  
            where id_cliente = ".$id." and pedido.estado = 3 GROUP BY MONTH(fecha), 
            YEAR(fecha) ORDER BY fecha";

            $detalle = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $res['status'] = 0;
                while ( $row = $result->fetch_object()) {
                    $res['status'] = 1;
                    array_push( $detalle, $row);
                }
            }
            else {
                
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            //$mac = system('arp -an');
            
            $res['detalle']=$detalle;
            header( 'Content-type: application/json' );
            echo json_encode($res);

        }
        public function leerPedidosC ($id) {
            $app2 = new Pedidos();
            $conn = $app2->basex();

            $sql = "SELECT SUM(detalle_pedido.cantidad) as 'cantidad', producto.nombre as 'nombre', pedido.fecha
            FROM pedido INNER JOIN detalle_pedido USING(id_pedido)
            INNER JOIN producto USING(id_producto)
            WHERE pedido.estado = 3 and pedido.id_cliente =  ".$id." GROUP BY pedido.fecha, producto.nombre";

            $detalle = array();
            $result = $conn->query($sql);
            if ( $result ) {
                while ( $row = $result->fetch_object()) {
                    $res['status'] = 1;
                    array_push( $detalle, $row);
                }
            }
            else {
                
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            //$mac = system('arp -an');
            
            $res['detalle']=$detalle;
            header( 'Content-type: application/json' );
            echo json_encode($res);

        }
        public function getPedido ($lal,$id) {
            $app2 = new Pedidos();
            $conn = $app2->basex();

            $sql = "SELECT cliente.nombre,cliente.apellido, pedido.id_pedido as 'id', producto.nombre as 'producto', 
            detalle_pedido.cantidad, pedido.fecha,
            round((producto.precio+(producto.precio*(producto.ganancias/100))),2) as 'precio', 
            producto.comision, detalle_pedido.id_venta

            FROM cliente INNER JOIN pedido USING(id_cliente) INNER JOIN detalle_pedido USING(id_pedido) INNER JOIN producto USING(id_producto) 
            WHERE pedido.token = ".$lal." AND pedido.estado = 3 AND cliente.id_cliente=".$id;

            $pedido = array();
            $result = $conn->query($sql);
            if ( $result ) {
                while ( $row = $result->fetch_object()) {
                    if ($row->id_venta > 0) {
                        $ped = array('id'=>$row->id, 'nombre'=>$row->producto,'precio'=>$row->precio,
                        'cantidad'=>$row->cantidad,'cliente'=>$row->nombre.' '.$row->apellido,
                        'fecha'=>$row->fecha,
                        'comision'=>$row->comision,'precio_total'=>(($row->comision+$row->precio)*$row->cantidad));
                    }
                    else{
                        $ped = array('id'=>$row->id, 'nombre'=>$row->producto,'precio'=>$row->precio,
                        'cantidad'=>$row->cantidad,'cliente'=>$row->nombre.' '.$row->apellido,
                        'fecha'=>$row->fecha,
                        'comision'=>0,'precio_total'=>($row->precio*$row->cantidad));
                    }
                    array_push( $pedido, $ped);
                }
            }
            else {
                
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            //$mac = system('arp -an');
            
            return $pedido;

        }
        /*
            /////////////////////////////////////////
            |              Parte Privada            |
            /////////////////////////////////////////
        */
        public function ventasP(){
            $app2 = new Pedidos();
            $conn = $app2->basex();

            $sql = "SELECT producto.nombre, SUM(detalle_pedido.cantidad) as 'cantidad' 
            FROM pedido INNER JOIN detalle_pedido USING(id_pedido) INNER JOIN producto USING(id_producto) 
            where pedido.fecha = date_format(NOW(),'%Y%m%d') and pedido.estado = 3 
            GROUP BY detalle_pedido.id_producto";

            $ventas = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $res['status'] = 0;
                while ( $row = $result->fetch_object()) {
                    $res['status'] = 1;
                    array_push( $ventas, $row);
                }
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['ventas']=$ventas;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        public function ventasV($ano){
            $app2 = new Pedidos();
            $conn = $app2->basex();

            $sql = "SELECT producto.nombre, producto.comision, MONTH(fecha) as 'mes', detalle_pedido.id_venta,
            round((producto.precio+(producto.precio*(producto.ganancias/100))),2) as 'precio'
            FROM pedido INNER JOIN detalle_pedido USING(id_pedido) INNER JOIN producto USING(id_producto)
            where YEAR(pedido.fecha) = '".$ano."' and pedido.estado = 3
            ORDER BY MONTH(fecha) asc, detalle_pedido.id_producto desc";

            $detalle = array();
            $meses = array('0'=>0,'1'=>0,'2'=>0,'3'=>0,'4'=>0,'5'=>0,'6'=>0,'7'=>0,'8'=>0,'9'=>0,'10'=>0,'11'=>0);
            $result = $conn->query($sql);
            if ( $result ) {
                while ( $row = $result->fetch_object()) {
                    $res['status'] = 1;
                    $total = $row->precio;
                    if ($row->id_venta > 0) {
                        $total = $row->precio * $row->comision;
                    }
                    $meses[$row->mes-1] += $total;
                }
                for ($i = 0; $i < 12; $i++) {
                    $red = array('precio'=>$meses[$i]);
                    array_push( $detalle, $red);
                }
            }
            else {
                
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            //$mac = system('arp -an');
            
            $res['ventas']=$detalle;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        public function ventasC($ano){
            $app2 = new Pedidos();
            $conn = $app2->basex();

            $sql = "SELECT cliente.nombre as 'nombre', SUM(detalle_pedido.cantidad) as 'cantidad'
            FROM cliente INNER JOIN ventas USING(id_cliente) INNER JOIN detalle_pedido USING(id_venta) INNER JOIN pedido USING(id_pedido)
            WHERE YEAR(pedido.fecha) = '".$ano."' GROUP BY ventas.id_cliente";

            $ventas = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $res['status'] = 0;
                while ( $row = $result->fetch_object()) {
                    $res['status'] = 1;
                    array_push( $ventas, $row);
                }
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['ventas']=$ventas;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        public function getpedidoA($ano = '2019',$app2){
            $conn = $app2->connec();

            $sql = "SELECT producto.nombre, producto.comision, MONTH(fecha) as 'mes', detalle_pedido.id_venta,
            round((producto.precio+(producto.precio*(producto.ganancias/100))),2) as 'precio'
            FROM pedido INNER JOIN detalle_pedido USING(id_pedido) INNER JOIN producto USING(id_producto)
            where YEAR(pedido.fecha) = '".$ano."' and pedido.estado = 3
            ORDER BY MONTH(fecha) asc, detalle_pedido.id_producto desc";

            $detalle = array();
            $meses = array('0'=>0,'1'=>0,'2'=>0,'3'=>0,'4'=>0,'5'=>0,'6'=>0,'7'=>0,'8'=>0,'9'=>0,'10'=>0,'11'=>0);
            $result = $conn->query($sql);
            if ( $result ) {
                while ( $row = $result->fetch_object()) {
                    $total = $row->precio;
                    if ($row->id_venta > 0) {
                        $total = $row->precio * $row->comision;
                    }
                    $meses[$row->mes-1] += $total;
                }
                for ($i = 0; $i < 12; $i++) {
                    switch ($i) {
                        case 0:
                            $mes = 'Enero';
                        break;
                        case 1:
                            $mes = 'Febrero';
                        break;
                        case 2:
                            $mes = 'Marzo';
                        break;
                        case 3:
                            $mes = 'Abril';
                        break;
                        case 4:
                            $mes = 'Mayo';
                        break;
                        case 5:
                            $mes = 'Junio';
                        break;
                        case 6:
                            $mes = 'Julio';
                        break;
                        case 7:
                            $mes = 'Agosto';
                        break;
                        case 8:
                            $mes = 'Septiembre';
                        break;
                        case 9:
                            $mes = 'Octubre';
                        break;
                        case 10:
                            $mes = 'Noviembre';
                        break;
                        case 11:
                            $mes = 'Diciembre';
                        break;
                    }
                    $red = array('mes'=>$mes,'ano'=>$ano,'precio'=>$meses[$i]);
                    array_push( $detalle, $red);
                }
            }
            else {
                
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            //$mac = system('arp -an');
            
            return $detalle;
        }
        public function getPedidoBY($app2){
            $conn = $app2->connec();

            $sql = "SELECT producto.nombre, SUM(detalle_pedido.cantidad) as 'cantidad' 
            FROM pedido INNER JOIN detalle_pedido USING(id_pedido) INNER JOIN producto USING(id_producto) 
            where pedido.fecha = date_format(NOW(),'%Y%m%d') and pedido.estado = 3 
            GROUP BY detalle_pedido.id_producto";

            $ventas = array();
            $result = $conn->query($sql);
            if ( $result ) {
                while ( $row = $result->fetch_object()) {
                    array_push( $ventas, $row);
                }
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            //$mac = system('arp -an');
            
            return $ventas;
        }
        public function getPedidoC($ano,$app2){
            $conn = $app2->connec();

            $sql = "SELECT cliente.nombre as 'nombre', SUM(detalle_pedido.cantidad) as 'cantidad'
            FROM cliente INNER JOIN ventas USING(id_cliente) INNER JOIN detalle_pedido USING(id_venta) INNER JOIN pedido USING(id_pedido)
            WHERE YEAR(pedido.fecha) = '".$ano."' GROUP BY ventas.id_cliente";

            $ventas = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $res['status'] = 0;
                while ( $row = $result->fetch_object()) {
                    $res['status'] = 1;
                    array_push( $ventas, $row);
                }
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            return $ventas;
        }
        
    }
?>