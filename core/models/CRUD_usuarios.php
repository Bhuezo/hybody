<?php
    class Usuarios{
        #Conexion a que se hace a la clase del archivo conexion en el helpers
        public function basex() {
            include('../helpers/conexion.php');
            $app = new Conexion(); 
            $conex = $app->connec();
            return $conex;
        }
        public function verificar ($id) {
            $app2 = new Usuarios();
            $conn = $app2->basex();

            #Consulta que se hara
            $sql= "SELECT id_usuario, estado, usuario.contra
            FROM usuario 
            where id_usuario = '".$id."'";
            $result = $conn->query($sql);
            if ($result) {
                $row = $result->fetch_object();
                if ($row != null) {
                    if ($row->estado == '00000000') {
                        $res['status'] = 0;
                        $res['message'] = 'Este usuario esta bloqueado';
                    }
                    else {
                        $res['privilege'] = $row->estado;
                        $res['status'] = 1;
                        /*if (password_verify($contra, $row->contra) == 1) {
                            $res['status'] = 1;
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Correo y contraseña no compatibles';
                        }*/
                    }
                }
                else {
                    $res['status'] = 0;
                    $res['message'] = 'Correo no existente';
                }
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
               
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function login($correo,$contra) {
            $app2 = new Usuarios();
            $conn = $app2->basex();

            #Consulta que se hara
            $sql= "SELECT id_usuario, estado, usuario.contra
            FROM usuario 
            where correo = '".$correo."'";
            $result = $conn->query($sql);
            if ($result) {
                $row = $result->fetch_object();
                if ($row != null) {
                    if ($row->estado == '00000000') {
                        $res['status'] = 0;
                        $res['message'] = 'Este usuario esta bloqueado';
                    }
                    else {
                        if (password_verify($contra, $row->contra) == 1) {
                            $res['status'] = 1;
                            $e = strlen($row->id_usuario);
                            $lim1 = rand( 3 , 6);
                            $lim2 = $lim1+($e-1);
                            $token = '$'.$lim1.$lim2.'$'.$e;
                            $f = strlen(getdate()['mday'].getdate()['mon'].getdate()['year']);
                            $g = getdate()['mday'].getdate()['mon'].getdate()['year'];
    
                            for ($i=0; $i < ($lim2*2); $i++) {
                                
                                if($i == $lim1-3)
                                {
                                    $token = $token.$row->id_usuario;
                                }
                                $token = $token.rand( 0 , 9);
                            }
                            $token = $token.$f.$g;
                            $res['subtoken'] =  $token;
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Correo y contraseña no compatibles';
                        }
                    }
                }
                else {
                    $res['status'] = 0;
                    $res['message'] = 'Correo no existente';
                }
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
               
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function getUsuariox () {
            $app2 = new Usuarios();
            $conn = $app2->basex();

            #Consulta que se hara
            $sql= "SELECT id_usuario as 'id', usuario.nombre, usuario.apellido, usuario.genero
            , usuario.direccion, usuario.fecha_nacimiento as 'fecha', usuario.correo, usuario.pais
            , usuario.ciudad, usuario.municipio, usuario.telefono, usuario.estado, img
            FROM usuario ";

            $usuarios = array();

            $result = $conn->query($sql);
            if ($result) {
                $res['status'] = 0;
                while ($row = $result->fetch_object()) {
                    $res['status'] = 1;
                    array_push($usuarios, $row);
                }
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['usuarios'] = $usuarios;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function setUsuariox ($p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$p11,$p12) {
            $app2 = new Usuarios();
            $conn = $app2->basex();

            #Consulta que se hara
            $sql= "insert into usuario values(null,'".$p1."','".$p2."','".$p3."','".$p4."','".$p5."'
            ,'".$p6."','".$p7."','".$p8."','".$p9."','".$p10."','".$p11."','".$p12."',
            'http://localhost/hybody/resources/img/imagen/clientes/1.png')";

            $usuarios = array();

            $result = $conn->query($sql);
            if ($result) {
                $res['status'] = 1;
                $res['message'] = 'Se agrego usuario correctamente!!';
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['usuarios'] = $usuarios;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function modUsuariox ($id,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$p11,$p12) {
            $app2 = new Usuarios();
            $conn = $app2->basex();

            #Consulta que se hara
            if (empty($p7) || $p7 == '') {
                $sql= "UPDATE  usuario  SET  nombre = '".$p1."',
                apellido = '".$p2."', genero = '".$p3."', direccion = '".$p4."', fecha_nacimiento = '".$p5."',
                correo = '".$p6."',pais = '".$p8."', ciudad = '".$p9."',
                municipio = '".$p10."', telefono = '".$p11."', estado = '".$p12."' WHERE id_usuario = ".$id;
            }
            else {
                $sql= "UPDATE  usuario  SET  nombre = '".$p1."',
                 apellido = '".$p2."', genero = '".$p3."', direccion = '".$p4."', fecha_nacimiento = '".$p5."',
                 correo = '".$p6."',contra='".$p7."',pais = '".$p8."', ciudad = '".$p9."',
                 municipio = '".$p10."', telefono = '".$p11."', estado = '".$p12."' WHERE id_usuario = ".$id;
            }
            $usuarios = array();

            $result = $conn->query($sql);
            if ($result) {
                $res['status'] = 1;
                $res['message'] = 'Se agrego usuario correctamente!!';
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['usuarios'] = $usuarios;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function imgUsuariox ($id,$img){
            $app2 = new Usuarios();
            $conn = $app2->basex();

            #Consulta que se hara
            $sql= "UPDATE  usuario SET img = '".$img."' WHERE id_usuario = ".$id;
            $usuarios = array();

            $result = $conn->query($sql);
            if ($result) {
                $res['status'] = 1;
                $res['message'] = 'Se actualizo usuario correctamente!!';
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['usuarios'] = $usuarios;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }

        /*
        
            -----------------------------------------
            |           MODELO PARA PDFs            |
            -----------------------------------------

        */


        public function getEmpleado ($id,$app2) {
            $conn = $app2->connec();

            #Consulta que se hara
            $sql= "SELECT id_usuario as 'id', usuario.nombre, apellido, estado
            FROM usuario 
            where id_usuario = '".$id."'";
            $usu = array();
            $result = $conn->query($sql);
            if ($result) {
                while ( $row = $result->fetch_object()) {
                    if ($row->estado != '00000000') {
                        array_push( $usu, $row);
                    }
                    else {
                        echo '<script>window.history.back();<script>';
                    }
                }
            }
            else {
                echo mysqli_error($conn);
            }
            mysqli_close($conn);
            return $usu;
        }
    }
?>