<?php 
    class Cajas
    {
        public function basex() {
            include('../helpers/conexion.php');
            $app = new Conexion(); 
            $conex = $app->connec();
            return $conex;
        }
        public function verifi ($id) {
            $app2 = new Cajas();
            $conn = $app2->basex();
//El exist en el where, valida rapidamente si algo existe si algo existe si algo no existe evita hacer la pregunta nuevamente
            $sql = "SELECT detalle_pedido.id_pedido, id_detallepedido, nombre, round((producto.precio+(producto.precio*(producto.ganancias/100))),2) as 'precio',
             ((round((producto.precio+(producto.precio*(producto.ganancias/100))),2))*detalle_pedido.cantidad) as 'precio_total',
            producto.imagen as 'img', producto.comision as 'comision', detalle_pedido.cantidad as 'cantidad',detalle_pedido.id_venta FROM  pedido INNER JOIN detalle_pedido on 
            pedido.id_pedido = detalle_pedido.id_pedido INNER JOIN producto 
            on detalle_pedido.id_producto = producto.id_producto WHERE 
            EXISTS(SELECT id_pedido FROM pedido where id_cliente= ? and estado = 1 
            ORDER BY id_pedido desc LIMIT 0,1) and
            pedido.estado = 1 and 
            pedido.id_pedido = (SELECT id_pedido FROM pedido where id_cliente= ? and estado = 1 
            ORDER BY id_pedido desc LIMIT 0,1)";

            $ingre = array();
            //Preparar consulta para recibir datos
			if ($result = mysqli_prepare($conn, $sql) ){
                //Asignar datos
                mysqli_stmt_bind_param($result, "ii", $id,$id);
                //Ejecutar consulta ya con datos
                mysqli_stmt_execute($result);
                //Obtener resultados
                $resultado = mysqli_stmt_get_result($result);
                //Ingresar datos al array que se decodificara como json
				while ($row = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
                    
                    $res['status'] = 1;
                    array_push($ingre, $row);
                }
            }
            else {
                //validaciones de errores
                switch(mysqli_errno($conn)){
					case 1045:
						$mensaje = 'Autenticacion desconocida';
					break;
					case 1049:
						$mensaje = 'Base desconocida';
					break;
					case 1054:
						$mensaje = 'Nombre del campo desconocido';
					break;
					case 1062:
						$mensaje = 'datos duplicados no se puede guardar';
					break;
					case 1146:
						$mensaje = 'Nombre de la tabla desconocido';
					break;
					case 1451:
						$mensaje = 'Registro ocupado no se puede eliminar';
					break;
					case 2002:
						$mensaje = 'Servidor desconocido';
					break;
                    $res['status']= 0;
                    $res['error']=$mensaje;
				}
            }
            $res['carrito']=$ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);

        }
        public function nuevoPedido($conn,$id){
            $sql= "SELECT id_pedido
            FROM pedido 
            where id_cliente = ? and estado = 1";
            if ($result = mysqli_prepare($conn, $sql) ){
				mysqli_stmt_bind_param($result, "i", $id);
				mysqli_stmt_execute($result);
				$row = array();
				$resultado = mysqli_stmt_get_result($result);
				if ($row = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
                }
                else{
                    $sql = "insert into pedido values(null,?,now(),'','',1)";
                    if ($result = mysqli_prepare($conn, $sql) ){
                        mysqli_stmt_bind_param($result, "i", $id);
                        mysqli_stmt_execute($result);
                        $row = array();
                        $resultado = mysqli_stmt_get_result($result);
                        $res['status']= 1;
                    }
                    else{
                        $mensaje = 'Algo salio mal';
                        //validaciones de errores//
                        switch(mysqli_errno($conn)){
                            case 1045:
                                $mensaje = 'Autenticacion desconocida';
                            break;
                            case 1049:
                                $mensaje = 'Base desconocida';
                            break;
                            case 1054:
                                $mensaje = 'Nombre del campo desconocido';
                            break;
                            case 1062:
                                $mensaje = 'datos duplicados no se puede guardar';
                            break;
                            case 1146:
                                $mensaje = 'Nombre de la tabla desconocido';
                            break;
                            case 1451:
                                $mensaje = 'Registro ocupado no se puede eliminar';
                            break;
                            case 2002:
                                $mensaje = 'Servidor desconocido';
                            break;
                        }
                        $res['status']= 0;
                        $res['error']=$mensaje;
                    }
                }
			}
			else{
				$mensaje = 'Algo salio mal';
                //validaciones de errores//
                switch(mysqli_errno($conn)){
					case 1045:
						$mensaje = 'Autenticacion desconocida';
					break;
					case 1049:
						$mensaje = 'Base desconocida';
					break;
					case 1054:
						$mensaje = 'Nombre del campo desconocido';
					break;
					case 1062:
						$mensaje = 'datos duplicados no se puede guardar';
					break;
					case 1146:
						$mensaje = 'Nombre de la tabla desconocido';
					break;
					case 1451:
						$mensaje = 'Registro ocupado no se puede eliminar';
					break;
					case 2002:
						$mensaje = 'Servidor desconocido';
					break;
                }
                
                $res['status']= 0;
				$res['error']=$mensaje;
			}
        }
        public function agregar($dato,$id,$vend) {
            $app2 = new Cajas();
            $conn = $app2->basex();
            $sql = "select id_detallepedido, producto.nombre, detalle_pedido.cantidad from detalle_pedido INNER JOIN producto USING(id_producto)
            where id_pedido = (SELECT id_pedido FROM pedido where id_cliente= ? and estado = 1 
            ORDER BY id_pedido desc LIMIT 0,1)";
            $ingre = array();
            //Preparar consulta para recibir datos
			if ($result = mysqli_prepare($conn, $sql) ){
                //Asignar datos
                mysqli_stmt_bind_param($result, "i", $id);
                //Ejecutar consulta ya con datos
                mysqli_stmt_execute($result);
                //Obtener resultados
                $resultado = mysqli_stmt_get_result($result);
                //Ingresar datos al array que se decodificara como json
                
                $ven = 0;
                $ven = $app2->validarVendo($conn,$vend,$dato['id_pro']);
                $sql = "insert into detalle_pedido values(null,?,(SELECT id_pedido FROM pedido 
                where id_cliente= ? and estado = 1 
                ORDER BY id_pedido desc LIMIT 0,1),?,?)";
                $val = 0;
                $p1 = 0;
                $p2 = 0;
				while ($row = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
                    if($dato['nombre'] == $row['nombre']) {
                        $val = 1;
                        $p1 = ($row['cantidad']+$dato['cantidad']);
                        $p2 = $row['id_detallepedido'];
                        $sql = "update detalle_pedido set cantidad = ? where id_detallepedido = ?";
                    }
                }
                if ($result = mysqli_prepare($conn, $sql) ){
                    if($val == 0){
                        mysqli_stmt_bind_param($result, "iiii", $dato['id_pro'], $id,$dato['cantidad'],$ven);
                    }
                    else if ($val == 1){
                        mysqli_stmt_bind_param($result, "ii", $p1, $p2);
                    }
                    mysqli_stmt_execute($result);
                    $resultado = mysqli_stmt_get_result($result);
                    $res['status'] = 1;
                }
                else{
                    $mensaje = 'Algo salio mal';
                    //validaciones de errores//
                    switch(mysqli_errno($conn)){
                        case 1045:
                            $mensaje = 'Autenticacion desconocida';
                        break;
                        case 1049:
                            $mensaje = 'Base desconocida';
                        break;
                        case 1054:
                            $mensaje = 'Nombre del campo desconocido';
                        break;
                        case 1062:
                            $mensaje = 'datos duplicados no se puede guardar';
                        break;
                        case 1146:
                            $mensaje = 'Nombre de la tabla desconocido';
                        break;
                        case 1451:
                            $mensaje = 'Registro ocupado no se puede eliminar';
                        break;
                        case 2002:
                            $mensaje = 'Servidor desconocido';
                        break;
                    }
                    $res['status']= 0;
                    $res['error']=$mensaje;
                }
                header( 'Content-type: application/json' );
                echo json_encode($res);
            }
            else {
                //validaciones de errores
                switch(mysqli_errno($conn)){
					case 1045:
						$mensaje = 'Autenticacion desconocida';
					break;
					case 1049:
						$mensaje = 'Base desconocida';
					break;
					case 1054:
						$mensaje = 'Nombre del campo desconocido';
					break;
					case 1062:
						$mensaje = 'datos duplicados no se puede guardar';
					break;
					case 1146:
						$mensaje = 'Nombre de la tabla desconocido';
					break;
					case 1451:
						$mensaje = 'Registro ocupado no se puede eliminar';
					break;
					case 2002:
						$mensaje = 'Servidor desconocido';
					break;
                    $res['status']= 0;
                    $res['error']=$mensaje;
				}
            }
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        public function cambiar($dato,$id) {
            $app2 = new Cajas();
            $conn = $app2->basex();
            
            $sql = "update detalle_pedido set cantidad = ? where id_detallepedido = ?";
            
            $ingre = array();
            if ($result = mysqli_prepare($conn, $sql) ){
                mysqli_stmt_bind_param($result, "ii", $dato['cantidad'], $dato["id"]);
                mysqli_stmt_execute($result);
                $row = array();
                $resultado = mysqli_stmt_get_result($result);
                $res['status'] = 1;
            }
            else{
                $mensaje = 'Algo salio mal';
                //validaciones de errores//
                switch(mysqli_errno($conn)){
                    case 1045:
                        $mensaje = 'Autenticacion desconocida';
                    break;
                    case 1049:
                        $mensaje = 'Base desconocida';
                    break;
                    case 1054:
                        $mensaje = 'Nombre del campo desconocido';
                    break;
                    case 1062:
                        $mensaje = 'datos duplicados no se puede guardar';
                    break;
                    case 1146:
                        $mensaje = 'Nombre de la tabla desconocido';
                    break;
                    case 1451:
                        $mensaje = 'Registro ocupado no se puede eliminar';
                    break;
                    case 2002:
                        $mensaje = 'Servidor desconocido';
                    break;
                }
                $res['status']= 0;
                $res['error']=$mensaje;
            }
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        public function borrar ($dato,$id) {
            $app2 = new Cajas();
            $conn = $app2->basex();
            
            $sql = "delete from detalle_pedido where id_detallepedido = ?";
            $ingre = array();
            if ($result = mysqli_prepare($conn, $sql) ){
                mysqli_stmt_bind_param($result, "i", $dato['id']);
                mysqli_stmt_execute($result);
                $row = array();
                $resultado = mysqli_stmt_get_result($result);
                $res['status'] = 1;
            }
            else{
                $mensaje = 'Algo salio mal';
                //validaciones de errores//
                switch(mysqli_errno($conn)){
                    case 1045:
                        $mensaje = 'Autenticacion desconocida';
                    break;
                    case 1049:
                        $mensaje = 'Base desconocida';
                    break;
                    case 1054:
                        $mensaje = 'Nombre del campo desconocido';
                    break;
                    case 1062:
                        $mensaje = 'datos duplicados no se puede guardar';
                    break;
                    case 1146:
                        $mensaje = 'Nombre de la tabla desconocido';
                    break;
                    case 1451:
                        $mensaje = 'Registro ocupado no se puede eliminar';
                    break;
                    case 2002:
                        $mensaje = 'Servidor desconocido';
                    break;
                }
                $res['status']= 0;
                $res['error']=$mensaje;
            }
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        public function proceso($id, $idP) {
            $app2 = new Cajas();
            $conn = $app2->basex();
            
            $sql = "update pedido set estado= 2 where id_cliente= ? and estado = 1 and id_pedido= ?";
            

            $ingre = array();
            if ($result = mysqli_prepare($conn, $sql) ){
                mysqli_stmt_bind_param($result, "ii",$id,$idP);
                mysqli_stmt_execute($result);
                $resultado = mysqli_stmt_get_result($result);
                return true;
            }
            else{
                $mensaje = 'Algo salio mal';
                //validaciones de errores//
                switch(mysqli_errno($conn)){
                    case 1045:
                        $mensaje = 'Autenticacion desconocida';
                    break;
                    case 1049:
                        $mensaje = 'Base desconocida';
                    break;
                    case 1054:
                        $mensaje = 'Nombre del campo desconocido';
                    break;
                    case 1062:
                        $mensaje = 'datos duplicados no se puede guardar';
                    break;
                    case 1146:
                        $mensaje = 'Nombre de la tabla desconocido';
                    break;
                    case 1451:
                        $mensaje = 'Registro ocupado no se puede eliminar';
                    break;
                    case 2002:
                        $mensaje = 'Servidor desconocido';
                    break;
                }
                return false;
                $res['error']=$mensaje;
            }
        }
        public function pagar($token,$id, $message) {
            $app2 = new Cajas();
            $conn = $app2->basex();
            
            $sql = "update pedido set estado= 3 , token = ?, fecha = NOW(), nap = ? where id_cliente= ? and estado = 2";
            

            if ($result = mysqli_prepare($conn, $sql) ){
                mysqli_stmt_bind_param($result, "isi", $token,$message,$id);
                mysqli_stmt_execute($result);
                $row = array();
                $resultado = mysqli_stmt_get_result($result);
                $app2->nuevoPedido($conn,$id);
                $res['status'] = 1;
                $res['link'] = $message;
                $res['message'] = 'Gracias por su compra';
            }
            else{
                $mensaje = 'Algo salio mal';
                //validaciones de errores//
                switch(mysqli_errno($conn)){
                    case 1045:
                        $mensaje = 'Autenticacion desconocida';
                    break;
                    case 1049:
                        $mensaje = 'Base desconocida';
                    break;
                    case 1054:
                        $mensaje = 'Nombre del campo desconocido';
                    break;
                    case 1062:
                        $mensaje = 'datos duplicados no se puede guardar';
                    break;
                    case 1146:
                        $mensaje = 'Nombre de la tabla desconocido';
                    break;
                    case 1451:
                        $mensaje = 'Registro ocupado no se puede eliminar';
                    break;
                    case 2002:
                        $mensaje = 'Servidor desconocido';
                    break;
                }
                $res['status']= 0;
                $res['error']=$mensaje;
            }
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        public function validarVendo($conn,$val,$id){
            
            $sql = "SELECT id_venta from ventas where token = ? and id_producto = ? and estado = 1";
            
            $ingre = array();
			if ($result = mysqli_prepare($conn, $sql) ){
				mysqli_stmt_bind_param($result, "si", $val,$id);
				mysqli_stmt_execute($result);
                $resultado = mysqli_stmt_get_result($result);
				if ($row = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
                    return $row["id_venta"];
                }
                else{
                    return 0;
                }
            }
            else{
                $mensaje = 'Algo salio mal';
                //validaciones de errores//
                switch(mysqli_errno($conn)){
                    case 1045:
                        $mensaje = 'Autenticacion desconocida';
                    break;
                    case 1049:
                        $mensaje = 'Base desconocida';
                    break;
                    case 1054:
                        $mensaje = 'Nombre del campo desconocido';
                    break;
                    case 1062:
                        $mensaje = 'datos duplicados no se puede guardar';
                    break;
                    case 1146:
                        $mensaje = 'Nombre de la tabla desconocido';
                    break;
                    case 1451:
                        $mensaje = 'Registro ocupado no se puede eliminar';
                    break;
                    case 2002:
                        $mensaje = 'Servidor desconocido';
                    break;
                }
            }
        }
    }
?>