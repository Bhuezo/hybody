<?php
    class Cliente{
        #Conexion a que se hace a la clase del archivo conexion en el helpers
        public function basex() {
            include('../helpers/conexion.php');
            $app = new Conexion(); 
            $conex = $app->connec();
            return $conex;
        }

        function validar ($conn, $id) {

            $sql = "SELECT tipo FROM cliente where id_cliente = ?";
			if ($result = mysqli_prepare($conn, $sql) ){
				mysqli_stmt_bind_param($result, "i", $id);
				mysqli_stmt_execute($result);
				$row = array();
				$resultado = mysqli_stmt_get_result($result);
				if ($row = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
                    if ($row['tipo'] == 0) {
                        return 2;
                    }
                    else {
                        return 0;
                    }
                }
                else{
                    return 1;
                }
			}
			else{
				$mensaje = 'Algo salio mal';
				switch(mysqli_errno($conn)){
					case 1045:
						$mensaje = 'Autenticacion desconocida';
					break;
					case 1049:
						$mensaje = 'Base desconocida';
					break;
					case 1054:
						$mensaje = 'Nombre del campo desconocido';
					break;
					case 1062:
						$mensaje = 'datos duplicados no se puede guardar';
					break;
					case 1146:
						$mensaje = 'Nombre de la tabla desconocido';
					break;
					case 1451:
						$mensaje = 'Registro ocupado no se puede eliminar';
					break;
					case 2002:
						$mensaje = 'Servidor desconocido';
					break;
				}
                $res['message']=$mensaje;
                return 3;
			}
        }
        /*

            -----------------------------------------
            |Modelo o funciones del lado del publico|
            -----------------------------------------

        */

        public function verificar($id=0) {
            $app2 = new Cliente();
            $conn = $app2->basex();

            $val = $app2->validar ($conn, $id);

            if ($val == 0) {
                
                #Consulta que se hara
                $sql= "SELECT id_cliente,nombre, apellido, genero, correo,contra,pais,
                fecha_nacimiento as 'fecha',ciudad,departamento,codigo_postal as 'postal',
                telefono,direccion,tipo, img, tipo
                FROM cliente 
                where id_cliente = ?";
                $pro = array();
                if ($result = mysqli_prepare($conn, $sql) ){
                    mysqli_stmt_bind_param($result, "i",$id);
                    mysqli_stmt_execute($result);
                    $resultado = mysqli_stmt_get_result($result);
                    $res['status'] = 0;
                    while ($row = mysqli_fetch_array($resultado,MYSQLI_ASSOC)) {
                        array_push( $pro, $row);
                        $res['status'] = 1;
                    }
                }
                else{
                    $mensaje = 'Algo salio mal';
                    switch(mysqli_errno($conn)){
                        case 1045:
                            $mensaje = 'Autenticacion desconocida';
                        break;
                        case 1049:
                            $mensaje = 'Base desconocida';
                        break;
                        case 1054:
                            $mensaje = 'Nombre del campo desconocido';
                        break;
                        case 1062:
                            $mensaje = 'datos duplicados no se puede guardar';
                        break;
                        case 1146:
                            $mensaje = 'Nombre de la tabla desconocido';
                        break;
                        case 1451:
                            $mensaje = 'Registro ocupado no se puede eliminar';
                        break;
                        case 2002:
                            $mensaje = 'Servidor desconocido';
                        break;
                    }
                    $res['status'] = 0;
                    $res['message'] = $mensaje;
                }

                $res['clientes'] = $pro;
            }
            else if ($val == 1) {
                $res['status'] = 0;
                $res['message'] = 'Su usuario no existe!!';
            }
            else if ($val == 2) {
                $res['status'] = 0;
                $res['message'] = 'Su usuario esta bloqueado!!';
            }
            else {
                $res['status'] = 0;
                $res['message'] = 'Verificar Consulta';
            }
            header( 'Content-type: application/json');
            echo json_encode($res);
        }

        #Ingresar un nuevo cliente

        public function registrar($p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$p11) {
            $app2 = new Cliente();
            $conn = $app2->basex();

            $sql = "insert into cliente 
            values(null,?,?,'http://localhost/hybody/resources/img/imagen/clientes/1.png',?,?,?,?,?,?,?,?,?,?,1)";
            
            if ($result = mysqli_prepare($conn, $sql) ){
                mysqli_stmt_bind_param($result, "isssssssssss",$p0,$p1,$p2,$p3,password_hash($p4, PASSWORD_DEFAULT),$p5,$p6,$p7,$p8,$p9,$p10,$p11);
                mysqli_stmt_execute($result);
                $resultado = mysqli_stmt_get_result($result);
                $app2->iniciar2($conn,$p3,$p4);
            }
            else{
                $mensaje = 'Algo salio mal';
                switch(mysqli_errno($conn)){
                    case 1045:
                        $mensaje = 'Autenticacion desconocida';
                    break;
                    case 1049:
                        $mensaje = 'Base desconocida';
                    break;
                    case 1054:
                        $mensaje = 'Nombre del campo desconocido';
                    break;
                    case 1062:
                        $mensaje = 'datos duplicados no se puede guardar';
                    break;
                    case 1146:
                        $mensaje = 'Nombre de la tabla desconocido';
                    break;
                    case 1451:
                        $mensaje = 'Registro ocupado no se puede eliminar';
                    break;
                    case 2002:
                        $mensaje = 'Servidor desconocido';
                    break;
                }
                $res['status'] = 0;
                $res['message'] = $mensaje;
                header( 'Content-type: application/json');
                echo json_encode($res);
            }
        }
        public function iniciar2($conn,$correo,$contra) {
            include('CRUD_cajas.php');
            $crud = new Cajas();
            #Consulta que se hara
            $sql= "SELECT id_cliente, tipo, contra
            FROM cliente 
            where correo = '".$correo."'";
            $result = $conn->query($sql);
            if ($result) {
                $row = $result->fetch_object();
                if ($row != null) {
                    if ($row->tipo == 0) {
                        $res['status'] = 0;
                        $res['message'] = 'Este usuario esta bloqueado';
                    }
                    else {
                        if (password_verify($contra, $row->contra) == 1) {
                            $res['status'] = 1;
                            $e = strlen($row->id_cliente);
                            $lim1 = rand( 3 , 6);
                            $lim2 = $lim1+($e-1);
                            $token = '$'.$lim1.$lim2.'$'.$e;
                            $f = strlen(getdate()['mday'].getdate()['mon'].getdate()['year']);
                            $g = getdate()['mday'].getdate()['mon'].getdate()['year'];
                            $crud->nuevoPedido($conn,$row->id_cliente);
                            for ($i=0; $i < ($lim2*2); $i++) {
                                
                                if($i == $lim1-3)
                                {
                                    $token = $token.$row->id_cliente;
                                }
                                $token = $token.rand( 0 , 9);
                            }
                            $token = $token.$f.$g;
                            $res['token'] =  $token;
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Correo y contraseña no compatibles';
                        }
                    }
                }
                else {
                    $res['status'] = 0;
                    $res['message'] = 'Correo no existente';
                }
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
               
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function iniciar($correo,$contra) {
            include('CRUD_cajas.php');
            $crud = new Cajas();
            
            $app2 = new Cliente();
            $conn = $app2->basex();

            #Consulta que se hara
            $sql= "SELECT id_cliente, tipo, contra
            FROM cliente 
            where correo = '".$correo."'";
            $result = $conn->query($sql);
            if ($result) {
                $row = $result->fetch_object();
                if ($row != null) {
                    if ($row->tipo == 0) {
                        $res['status'] = 0;
                        $res['message'] = 'Este usuario esta bloqueado';
                    }
                    else {
                        if (password_verify($contra, $row->contra) == 1) {
                            $res['status'] = 1;
                            $e = strlen($row->id_cliente);
                            $lim1 = rand( 3 , 6);
                            $lim2 = $lim1+($e-1);
                            $token = '$'.$lim1.$lim2.'$'.$e;
                            $f = strlen(getdate()['mday'].getdate()['mon'].getdate()['year']);
                            $g = getdate()['mday'].getdate()['mon'].getdate()['year'];
                            $crud->nuevoPedido($conn,$row->id_cliente);
    
                            for ($i=0; $i < ($lim2*2); $i++) {
                                
                                if($i == $lim1-3)
                                {
                                    $token = $token.$row->id_cliente;
                                }
                                $token = $token.rand( 0 , 9);
                            }
                            $token = $token.$f.$g;
                            $res['token'] =  $token;
                        }
                        else {
                            $res['status'] = 0;
                            $res['message'] = 'Correo y contraseña no compatibles';
                        }
                    }
                }
                else {
                    $res['status'] = 0;
                    $res['message'] = 'Correo no existente';
                }
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
               
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function setImg($img,$id) {
            $app2 = new Cliente();
            $conn = $app2->basex();

            $sql = "Update cliente set img = '".$img."' where id_cliente = ".$id;
            $result = $conn->query($sql);
            if ( $result) {

                $res['status'] = 1;
                $res['message'] = "Imagen actualizada!!";
            } 
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function setGeneral ($id,$p0,$p1,$p2,$p3){
            $app2 = new Cliente();
            $conn = $app2->basex();

            $sql = "Update cliente set nombre = '".$p0."',apellido = '".$p1."',correo = '".$p2."',
            telefono = '".$p3."'
            where id_cliente = ".$id;
            $result = $conn->query($sql);
            if ( $result) {

                $res['status'] = 1;
                $res['message'] = "Informacion actualizada!!";
            } 
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            header( 'Content-type: application/json');
            echo json_encode($res);

        }
        public function setInfo ($id,$p0,$p1,$p2,$p3,$p4,$p5,$p6){
            $app2 = new Cliente();
            $conn = $app2->basex();

            $sql = "Update cliente set genero = '".$p0."',pais = '".$p1."',departamento = '".$p2."',
            ciudad = '".$p3."', direccion = '".$p4."',codigo_postal = '".$p5."',fecha_nacimiento = '".$p6."'
            where id_cliente = ".$id;
            $result = $conn->query($sql);
            if ( $result) {

                $res['status'] = 1;
                $res['message'] = "Informacion actualizada!!";
            } 
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            header( 'Content-type: application/json');
            echo json_encode($res);

        }
        public function setSeguridad($id,$p0) {
            $app2 = new Cliente();
            $conn = $app2->basex();

            $sql = "Update cliente set contra = '".password_hash($p0,PASSWORD_DEFAULT)."'
            where id_cliente = ".$id;
            $result = $conn->query($sql);
            if ( $result) {

                $res['status'] = 1;
                $res['message'] = "Contraseña actualizada!!";
            } 
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            header( 'Content-type: application/json');
            echo json_encode($res);

        }



        /*
        
            -----------------------------------------
            |Modelo o funciones del lado del dashboard|
            -----------------------------------------

        */



        public function getClientex () {
            $app2 = new Cliente();
            $conn = $app2->basex();

            #Consulta que se hara
            $sql= "SELECT id_cliente as 'id', cliente.nombre, cliente.apellido, cliente.genero
            , cliente.direccion, cliente.fecha_nacimiento as 'fecha', cliente.correo, cliente.pais
            , cliente.ciudad , cliente.departamento as 'municipio', cliente.telefono,
            cliente.tipo, img, cliente.codigo_postal as 'codigo', tipo
            FROM cliente ";

            $clientes = array();

            $result = $conn->query($sql);
            if ($result) {
                $res['status'] = 0;
                while ($row = $result->fetch_object()) {
                    $res['status'] = 1;
                    array_push($clientes, $row);
                }
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['clientes'] = $clientes;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function setClientex ($p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$p11,$p12,$p13) {
            $app2 = new Cliente();
            $conn = $app2->basex();

            #Consulta que se hara
            $sql= "insert into cliente values(null,'".$p1."','".$p2."',
            'http://localhost/hybody/resources/img/imagen/clientes/1.png',
            '".$p3."','".$p4."','".$p5."','".$p6."','".$p7."','".$p8."',
            '".$p9."','".$p10."','".$p11."','".$p12."','".$p13."'
            )";

            $clientes = array();

            $result = $conn->query($sql);
            if ($result) {
                $res['status'] = 1;
                $res['message'] = 'Se agrego usuario correctamente!!';
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['clientes'] = $clientes;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function modClientex ($id,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$p11,$p12,$p13) {
            $app2 = new Cliente();
            $conn = $app2->basex();

            #Consulta que se hara
            if (empty($p5) || $p5 == '') {
                $sql= "UPDATE  cliente  SET  nombre = '".$p1."',
                apellido = '".$p2."', genero = '".$p3."', direccion = '".$p12."', fecha_nacimiento = '".$p7."',
                correo = '".$p4."',pais = '".$p6."', ciudad = '".$p9."',
                departamento = '".$p8."', telefono = '".$p11."', tipo = '".$p13."', codigo_postal = '".$p10."'
                 WHERE id_cliente = ".$id;
            }
            else {
                $sql= "UPDATE  cliente  SET  nombre = '".$p1."',
                 apellido = '".$p2."', genero = '".$p3."', direccion = '".$p12."', fecha_nacimiento = '".$p7."',
                 correo = '".$p4."',contra='".$p5."',pais = '".$p6."', ciudad = '".$p9."',
                 departamento = '".$p8."', telefono = '".$p11."', tipo = '".$p13."', codigo_postal = '".$p10."'
                  WHERE id_cliente = ".$id;
            }
            $clientes = array();

            $result = $conn->query($sql);
            if ($result) {
                $res['status'] = 1;
                $res['message'] = 'Se agrego usuario correctamente!!';
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['clientes'] = $clientes;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function imgClientex ($id,$img){
            $app2 = new Cliente();
            $conn = $app2->basex();

            #Consulta que se hara
            $sql= "UPDATE  cliente SET img = '".$img."' WHERE id_cliente = ".$id;
            $clientes = array();

            $result = $conn->query($sql);
            if ($result) {
                $res['status'] = 1;
                $res['message'] = 'Se actualizo usuario correctamente!!';
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['clientes'] = $clientes;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
?>