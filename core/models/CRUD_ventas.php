<?php 
    class Ventas
    {
        public function basex() {
            include('../helpers/conexion.php');
            $app = new Conexion(); 
            $conex = $app->connec();
            return $conex;
        }
        public function leerVentas ($id) {
            $app2 = new Ventas();
            $conn = $app2->basex();

            $sql = "SELECT producto.id_producto as 'id', ventas.id_venta as 'idv', producto.nombre as 'nombre',
            ventas.token as 'link', ventas.estado
            FROM ventas INNER JOIN producto USING(id_producto)
            WHERE ventas.id_cliente = ".$id." and ventas.estado != 0
            GROUP BY id_venta";

            $ventas = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $res['status'] = 0;
                while ( $row = $result->fetch_object()) {
                    $res['status'] = 1;
                    $letras = array("id"=>$row->id,"id_venta"=>$row->idv, "producto"=>$row->nombre, "link"=>$row->link, "cantidad"=>$app2->cantidad($conn,$row->id), "estado"=>$row->estado);
                    array_push( $ventas, $letras);
                }
            }
            else {
                
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            //$mac = system('arp -an');
            
            $res['ventas']=$ventas;
            header( 'Content-type: application/json' );
            echo json_encode($res);

        }
        public function cantidad ($conn,$id) {
            $sql = "SELECT SUM(detalle_pedido.cantidad) as 'cantidad'
            FROM detalle_pedido INNER JOIN pedido USING(id_pedido) 
            WHERE id_venta = ".$id." AND pedido.estado = 3";

            $result = $conn->query($sql);
            if ( $result ) {
                $row = $result->fetch_object();
                if ($row != null) {
                    if ($row->cantidad != null && !empty($row->cantidad) ){
                        return $row->cantidad;
                    }
                    else {
                        return 0;
                    }
                }
                else {
                    return 0;
                }
            }
            else {
                return 0;
            }
        }
        public function agregar ($datos,$id) {
            $app2 = new Ventas();
            $conn = $app2->basex();

            $sql = "SELECT id_venta from ventas where id_cliente = ".$id." and id_producto = ".$datos['id_producto']." and estado = 1";

            $result = $conn->query($sql);
            if ( $result ) {
                $row = $result->fetch_object();
                if ($row == null) {
                    $sql = "INSERT INTO ventas Values(null,".$id.",".$datos['id_producto'].",'".password_hash($datos['id_producto'],PASSWORD_DEFAULT)."',1)";

                    $detalle = array();
                    $result = $conn->query($sql);
                    if ( $result ) {
                        $res['status'] = 1;
                        $res['message'] = "Link creado";
                    }
                    else {
                        
                        $res['status'] = 0;
                        $res['message'] = mysqli_error($conn);
                    }
                }
                else {
                    $res['status'] = 0;
                    $res['message'] = "Ya existe una venta con este producto";
                }
            }
            else {
                
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            //$mac = system('arp -an');
            
            header( 'Content-type: application/json' );
            echo json_encode($res);

        }
        public function estadoVenta($token,$id,$ac) {
            $app2 = new Ventas();
            $conn = $app2->basex();
            if ($ac == 1) {
                $estado = 2;
            }
            if ($ac == 2) {
                $estado = 1;
            }
            $sql = "update ventas set estado= ".$estado." where id_cliente= ".$token." and id_venta = ".$id;
            

            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $res['status'] = 1;
                $res['message'] = "El estado se ha actualizado";
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        
    }
?>