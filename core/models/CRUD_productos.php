<?php
    class Productos
    {
        public function basex() {
            include('../helpers/conexion.php');
            $app = new Conexion(); 
            $conex = $app->connec();
            return $conex;
        }
        
        public function leer() {
            #Mandar a llmar la funcion que conecta a la base
            $app2 = new Productos();
            $conn = $app2->basex();
            #Consulta de todos los productos que tengan una cantidad mayor a 0 a la base de datos
            $sql= "SELECT producto.id_producto as 'id', producto.nombre as 'producto', 
            producto.descripcion, marca.nombre_marca as 'marca', categoria.nombre as 'categoria', 
            round((producto.precio+(producto.precio*(producto.ganancias/100))),2) as 'precio',
            producto.comision as 'comision',producto.imagen,
            (producto.cantidad-producto.cantidad)+1 as 'cantidad' FROM marca 
            INNER JOIN producto on marca.id_marca = producto.id_marca 
            INNER JOIN categoria on categoria.id_categoria = producto.id_categoria 
            where cantidad > 0";

            #Realizar Query
            $result = $conn->query($sql);
            $pro = array();
            if ($result) {
                while ($row = $result->fetch_assoc()) {
                    $res['status'] = 1;
                    $letra = array('id' => $row['id'] , 'producto'=>$row['producto'],'imagen'=>$row['imagen'], 'img'=>$app2->img($conn,$row['id']), 'descripcion'=> $row['descripcion'], 'marca'=> $row['marca'],'categoria'=>$row['categoria'], 'precio'=>$row['precio'], 'cantidad'=> $row['cantidad'], 'comision'=>$row['comision'] );
                    array_push($pro, $letra);
                }
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['productos'] = $pro; 
            header('Content-type: application/json');
            echo json_encode($res);
        }
        public function leerProducto($id) {
            
            $app2 = new Productos();
            $conn = $app2->basex();

            #Query para leer producto segun id del mismo
            $sql= "SELECT producto.id_producto as 'id', producto.nombre as 'producto', 
            producto.descripcion, marca.nombre_marca as 'marca', categoria.nombre as 'categoria', 
            round((producto.precio+(producto.precio*(producto.ganancias/100))),2) as 'precio', 
            (producto.cantidad-producto.cantidad)+1 as 'cantidad', producto.comision as 'comision'
            FROM marca 
            INNER JOIN producto on marca.id_marca = producto.id_marca 
            INNER JOIN categoria on categoria.id_categoria = producto.id_categoria 
            where cantidad > 0 and producto.id_producto = ".$id;

            $result = $conn->query($sql);
            $pro = array();
            if ($result) {
                while ($row = $result->fetch_assoc()) {
                    $res['status'] = 1;
                    $letra = array('id' => $row['id'] , 'producto'=>$row['producto'], 'img'=>$app2->img($conn,$row['id']), 'descripcion'=> $row['descripcion'], 'marca'=> $row['marca'],'categoria'=>$row['categoria'], 'precio'=>$row['precio'], 'cantidad'=> $row['cantidad'], 'comision'=>$row['comision'] );
                    array_push($pro, $letra);
                }
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['productos'] = $pro; 
            header('Content-type: application/json');
            echo json_encode($res);
        }

        #Funcion que sirve para traer las imagenes de los productos
        public function img($conn, $id) {
            $sql= "SELECT id_img as 'id', url, estado FROM imagenes where id_producto = ".$id;
            $result = $conn->query($sql);
            $pro2 = array();
            while ($row = $result->fetch_assoc()) {
                array_push($pro2, $row);
            }
            return $pro2;
        }
        #Funcion que funciona para mandar a llamar las categorias
        public function leerCategorias() {
            $app2 = new Productos();
            $conn = $app2->basex();
            $sql= "SELECT id_categoria, nombre FROM categoria";
            $result = $conn->query($sql);
            $pro = array();
            if ($result) {
                while ($row = $result->fetch_assoc()) {
                    $res['status'] = 1;
                    array_push($pro, $row);
                }
            }
            else {
                $res['status'] = 0;
            }
            $res['categorias'] = $pro; 
            header('Content-type: application/json');
            echo json_encode($res);
        }
        #Funcion que funciona para mandar a llamar las categorias
        public function leerMarcas() {
            $app2 = new Productos();
            $conn = $app2->basex();
            $sql= "SELECT id_marca, nombre_marca as 'nombre' FROM marca";
            $result = $conn->query($sql);
            $pro = array();
            if ($result) {
                while ($row = $result->fetch_assoc()) {
                    $res['status'] = 1;
                    array_push($pro, $row);
                }
            }
            else {
                $res['status'] = 0;
            }
            $res['categorias'] = $pro; 
            header('Content-type: application/json');
            echo json_encode($res);
        }
        #Funcion que funciona para mandar a llamar las categorias
        public function getProductos($ano,$app2) {
            $conn = $app2->connec();

            $sql= "SELECT id_producto as id,nombre,precio,cantidad,comision,ganancias as 'precio_total' FROM producto";
            $result = $conn->query($sql);
            $pro = array();
            if ($result) {
                while ($row = $result->fetch_assoc()) {
                    $res['status'] = 1;
                    array_push($pro, $row);
                }
            }
            else {
                $res['status'] = 0;
            }
            return $pro;
        }
        public function getProductosC2($id,$ano,$app2) {
            $conn = $app2->connec();

            $sql= "SELECT producto.nombre as 'producto', SUM(detalle_pedido.cantidad) as 'cantidad',
            cliente.nombre
            from producto INNER JOIN detalle_pedido USING(id_producto) INNER JOIN pedido USING(id_pedido) INNER JOIN cliente USING(id_cliente)
            WHERE pedido.estado = 3 and producto.id_producto = ".$id." and YEAR(pedido.fecha) = '".$ano."'
            GROUP BY cliente.id_cliente";
            $result = $conn->query($sql);
            $pro = array();
            if ($result) {
                $res['status'] = 0;
                while ($row = $result->fetch_assoc()) {
                    $res['status'] = 1;
                    array_push($pro, $row);
                }
            }
            else {
                $res['status'] = 0;
            }
            return $pro;
        }
        public function getProductosC() {
            $app2 = new Productos();
            $conn = $app2->basex();
//Limit solo va a permitir que se muestre del dato 0 al dato 5 de forma descendente
            $sql= "SELECT producto.nombre, SUM(detalle_pedido.cantidad) as 'cantidad' 
            from producto INNER JOIN detalle_pedido USING(id_producto) 
            GROUP BY detalle_pedido.id_producto
            ORDER BY detalle_pedido.cantidad desc LIMIT 0,5";
            $result = $conn->query($sql);
            $pro = array();
            if ($result) {
                $res['status'] = 0;
                while ($row = $result->fetch_assoc()) {
                    $res['status'] = 1;
                    array_push($pro, $row);
                }
            }
            else {
                $res['status'] = 0;
            }
            $res['productos'] = $pro;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function getProductox () {
            $app2 = new Productos();
            $conn = $app2->basex();

            #Consulta que se hara
            $sql= "SELECT id_producto as 'id', producto.nombre, producto.id_categoria, categoria.nombre as 'categoria', marca.nombre_marca as 'marca', producto.id_marca, producto.imagen as 'img', producto.precio, producto.cantidad, producto.comision,
            producto.ganancias, producto.estado
            FROM categoria INNER JOIN producto USING(id_categoria) INNER JOIN marca USING(id_marca)";

            $usuarios = array();

            $result = $conn->query($sql);
            if ($result) {
                $res['status'] = 0;
                while ($row = $result->fetch_object()) {
                    $res['status'] = 1;
                    array_push($usuarios, $row);
                }
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['productos'] = $usuarios;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function setProductox ($p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$p11,$p12) {
            $app2 = new Productos();
            $conn = $app2->basex();

            #Consulta que se hara
            $sql= "insert into producto values(null,'".$p1."','".$p2."','".$p3."','".$p4."','".$p5."'
            ,'".$p6."','".$p7."','".$p8."','".$p9."','".$p10."','".$p11."','".$p12."',
            'http://localhost/hybody/resources/img/imagen/clientes/1.png')";

            $usuarios = array();

            $result = $conn->query($sql);
            if ($result) {
                $res['status'] = 1;
                $res['message'] = 'Se agrego usuario correctamente!!';
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['usuarios'] = $usuarios;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function modProductox ($id,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$p11,$p12) {
            $app2 = new Productos();
            $conn = $app2->basex();

            #Consulta que se hara
            if (empty($p7) || $p7 == '') {
                $sql= "UPDATE  usuario  SET  nombre = '".$p1."',
                apellido = '".$p2."', genero = '".$p3."', direccion = '".$p4."', fecha_nacimiento = '".$p5."',
                correo = '".$p6."',pais = '".$p8."', ciudad = '".$p9."',
                municipio = '".$p10."', telefono = '".$p11."', estado = '".$p12."' WHERE id_usuario = ".$id;
            }
            else {
                $sql= "UPDATE  usuario  SET  nombre = '".$p1."',
                 apellido = '".$p2."', genero = '".$p3."', direccion = '".$p4."', fecha_nacimiento = '".$p5."',
                 correo = '".$p6."',contra='".$p7."',pais = '".$p8."', ciudad = '".$p9."',
                 municipio = '".$p10."', telefono = '".$p11."', estado = '".$p12."' WHERE id_usuario = ".$id;
            }
            $usuarios = array();

            $result = $conn->query($sql);
            if ($result) {
                $res['status'] = 1;
                $res['message'] = 'Se agrego usuario correctamente!!';
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['usuarios'] = $usuarios;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
        public function imgProductox ($id,$img){
            $app2 = new Productos();
            $conn = $app2->basex();

            #Consulta que se hara
            $sql= "UPDATE  usuario SET img = '".$img."' WHERE id_usuario = ".$id;
            $usuarios = array();

            $result = $conn->query($sql);
            if ($result) {
                $res['status'] = 1;
                $res['message'] = 'Se actualizo usuario correctamente!!';
            }
            else {
                $res['status'] = 0;
                $res['message'] = mysqli_error($conn);
            }
            $res['usuarios'] = $usuarios;
            header( 'Content-type: application/json');
            echo json_encode($res);
        }
    }
?>