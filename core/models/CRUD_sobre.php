<?php
    class Sobre
    {
        public function basex() {
            include('../../helpers/conexion.php');
            $app = new conexion(); 
            $conex = $app->connec();
            return $conex;
        }
        public function provedor() {
            $app2 = new Sobre();
            $conn = $app2->basex();

            $sql= "SELECT id_proveedor as 'id', nombre, img FROM proveedor";
            $result = $conn->query($sql);

            $pro = array();

            while ( $row = $result->fetch_assoc() ) {
                array_push( $pro, $row );
            }
            $res['proveedor'] = $pro; 
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
    }
?>