<?php 
    class Dashboard
    {
        /*
            Conexion a la base de datos
        */
        public function basex() {
            include('../helpers/conexion.php');
            $app = new conexion();
            $conex = $app->connec();
            return $conex;
        }
        /*
            Usuarios
        */
        //Leer Usuarios para ingresar

        public function usuarioVerificar($correo,$contra) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            $sql= "SELECT id_usuario as 'id',nombre,correo FROM usuario where correo ='".$correo."' and contra = '".$contra."'";
            
            $result = $conn->query($sql);
            $pro = array();
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                
                while ( $row = $result->fetch_object() ) {
                    $_SESSION['empleado']= $row->id;
                }
                $var = array('val'=>0);
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 , 'mensaje'=>'El conjunto de correo y contraseña no coinciden');
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        //Leer Usuarios

        public function usuarioV($id) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            $sql= "SELECT id_usuario as 'id',nombre, apellido, direccion,genero, correo,contra,pais,
            fecha_nacimiento as 'fecha',ciudad,municipio,telefono FROM usuario where id_usuario = ".$id;
            
            $result = $conn->query($sql);
            $pro = array();
            while ( $row = $result->fetch_object() ) {
                array_push( $pro, $row );
            }
            $res['usuarios'] = $pro;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        //Leer Usuarios

        public function usuarioL() {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            $sql= "SELECT id_usuario as 'id',nombre, apellido, direccion,genero, correo,contra,pais,
            fecha_nacimiento as 'fecha',ciudad,municipio,telefono FROM usuario";
            
            $result = $conn->query($sql);
            $pro = array();
            while ( $row = $result->fetch_object() ) {
                array_push( $pro, $row );
            }
            $res['usuarios'] = $pro;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Agregar Usuarios

        public function usuarioA($p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10) {
            $app2 = new Dashboard();
            $conn = $app2->basex();

            $sql = "insert into usuario values(null,'".$p0."','".$p1."','".$p2."','".$p3."','".$p4."','".$p5."','".$p6."','".$p7."','".$p8."','".$p9."','".$p10."')";
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Modificar Usuarios

        public function usuarioM($id,$p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10) {
            $app2 = new Dashboard();
            $conn = $app2->basex();

            $sql = "UPDATE usuario SET nombre='".$p0."',apellido='".$p1."',genero='".$p2."',correo='".$p5."',contra='".$p6."',pais='".$p7."',fecha_nacimiento='".$p4."',ciudad='".$p8."',municipio='".$p9."',telefono='".$p10."',direccion='".$p3."' WHERE id_usuario = ".$id;
           
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Borrar Usuarios 

        public function usuarioB($id) {
            $app2 = new Dashboard();
            $conn = $app2->basex();

            $sql = "DELETE FROM usuario WHERE id_usuario = ".$id;
           
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0, 'mensaje'=>'¡Eliminado exitosamente!');
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1, 'mensaje'=>'¡Eliminado sin exito!');
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        /*
            ------------------------------------------------------------------
                                        Cliente
            ------------------------------------------------------------------
        */

        //Leer Cliente

        public function clienteL() {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            $sql= "SELECT id_cliente as 'id',nombre, apellido, genero, correo,contra,pais,fecha_nacimiento as 'fecha',ciudad,departamento,codigo_postal as 'postal',telefono,direccion FROM cliente";
            
            $result = $conn->query($sql);
            $pro = array();

            while ( $row = $result->fetch_object() ) {
                array_push( $pro, $row );
            }
            $res['clientes'] = $pro;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Agregar Cliente

        public function clienteA($p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$p11) {
            $app2 = new Dashboard();
            $conn = $app2->basex();

            $sql = "insert into cliente values(null,'".$p0."','".$p1."','".$p2."','".$p3."','".$p4."','".$p5."','".$p6."','".$p7."','".$p8."','".$p9."','".$p10."','".$p11."')";
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Modificar Cliente

        public function clienteM($id,$p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9,$p10,$p11) {
            $app2 = new Dashboard();
            $conn = $app2->basex();

            $sql = "UPDATE cliente SET nombre='".$p0."',apellido='".$p1."',genero='".$p2."',correo='".$p3."',contra='".$p4."',pais='".$p5."',fecha_nacimiento='".$p6."',ciudad='".$p7."',departamento='".$p8."',codigo_postal='".$p9."',telefono='".$p10."',direccion='".$p11."' WHERE id_cliente = ".$id;
            
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Borrar Cliente

        public function clienteB($id) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            
            $sql = "DELETE FROM cliente WHERE id_cliente = ".$id;
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0, 'mensaje'=>'¡Eliminado exitosamente!');
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1, 'mensaje'=>'¡Eliminado sin exito!');
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        /*
            ------------------------------------------------------------------
                                        Proveedor
            ------------------------------------------------------------------
        */

        //Leer Proveedor

        public function proveedorL() {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            $sql= "SELECT id_proveedor as 'id',nombre, img, contacto, correo,telefono,estado FROM proveedor";
            
            $result = $conn->query($sql);
            $pro = array();

            while ( $row = $result->fetch_object() ) {
                array_push( $pro, $row );
            }
            $res['proveedor'] = $pro;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Agregar Proveedor

        public function proveedorA($p0,$p1,$p2,$p3,$p4,$p5) {
            $app2 = new Dashboard();
            $conn = $app2->basex();

            $sql = "insert into proveedor values(null,'".$p0."','".$p1."','".$p2."','".$p3."','".$p4."','".$p5."')";
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Modificar Proveedor

        public function proveedorM($v,$id,$p0,$p1,$p2,$p3,$p4,$p5) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            if($v == 1) {
                $sql = "UPDATE proveedor SET nombre='".$p0."',contacto='".$p2."',correo='".$p3."', telefono='".$p4."',estado='".$p5."' WHERE id_proveedor = ".$id;
            }
            else{
                $sql = "UPDATE proveedor SET nombre='".$p0."',img='".$p1."',contacto='".$p2."',correo='".$p3."', telefono='".$p4."',estado='".$p5."' WHERE id_proveedor = ".$id;
            
            }
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Borrar Proveedor

        public function proveedorB($id) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            
            $sql = "DELETE FROM proveedor WHERE id_proveedor = ".$id;
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0, 'mensaje'=>'¡Eliminado exitosamente!');
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1, 'mensaje'=>'¡Eliminado sin exito!');
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        /*
            ------------------------------------------------------------------
                                        Marca
            ------------------------------------------------------------------
        */

        //Leer Marca

        public function marcaL() {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            $sql= "SELECT id_marca as 'id', marca.id_proveedor as 'idprovee', proveedor.nombre as 'proveedor', nombre_marca as 'nombre', pagina_web as 'link' FROM marca INNER JOIN proveedor on marca.id_proveedor = proveedor.id_proveedor";
            
            $result = $conn->query($sql);
            $pro = array();

            while ( $row = $result->fetch_object() ) {
                array_push( $pro, $row );
            }
            $res['marca'] = $pro;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Agregar Marca

        public function marcaA($p0,$p1,$p2) {
            $app2 = new Dashboard();
            $conn = $app2->basex();

            $sql = "insert into marca values(null,'".$p0."','".$p1."','".$p2."')";
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Modificar Marca

        public function marcaM($id,$p0,$p1,$p2) {
            $app2 = new Dashboard();
            $conn = $app2->basex();

            $sql = "UPDATE marca SET nombre_marca='".$p1."',id_proveedor='".$p0."',pagina_web='".$p2."' WHERE id_marca = ".$id;
            
            
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Borrar Marca

        public function marcaB($id) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            
            $sql = "DELETE FROM marca WHERE id_marca = ".$id;
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0, 'mensaje'=>'¡Eliminado exitosamente!');
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1, 'mensaje'=>'¡Eliminado sin exito!');
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        /*
            ------------------------------------------------------------------
                                        Categoria
            ------------------------------------------------------------------
        */

        //Leer Categoria

        public function categoriaL() {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            $sql= "SELECT id_categoria as 'id', nombre FROM categoria";
            
            $result = $conn->query($sql);
            $pro = array();

            while ( $row = $result->fetch_object() ) {
                array_push( $pro, $row );
            }
            $res['categoria'] = $pro;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Agregar Categoria

        public function categoriaA($p0) {
            $app2 = new Dashboard();
            $conn = $app2->basex();

            $sql = "insert into categoria values(null,'".$p0."')";
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Modificar Categoria

        public function categoriaM($id,$p0) {
            $app2 = new Dashboard();
            $conn = $app2->basex();

            $sql = "UPDATE categoria SET nombre='".$p0."' WHERE id_categoria = ".$id;
            
            
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Borrar Categoria

        public function categoriaB($id) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            
            $sql = "DELETE FROM categoria WHERE id_categoria = ".$id;
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0, 'mensaje'=>'¡Eliminado exitosamente!');
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1, 'mensaje'=>'¡Eliminado sin exito!');
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        /*
            ------------------------------------------------------------------
                                        Productos
            ------------------------------------------------------------------
        */

        //Leer Productos

        public function productoL() {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            $sql= "SELECT producto.id_producto as 'id', producto.nombre as 'producto',producto.imagen as 'img', producto.descripcion,marca.id_marca as 'idm', marca.nombre_marca as 'marca',marca.pagina_web as 'link', categoria.id_categoria as 'ida', categoria.nombre as 'categoria', producto.precio,producto.ganancias, producto.cantidad as 'cantidad', producto.estado FROM marca INNER JOIN producto on marca.id_marca = producto.id_marca INNER JOIN categoria on categoria.id_categoria = producto.id_categoria";
            
            $result = $conn->query($sql);
            $pro = array();

            while ( $row = $result->fetch_object() ) {
                array_push( $pro, $row );
            }
            $res['productos'] = $pro;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Agregar Productos

        public function productoA($p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8) {
            $app2 = new Dashboard();
            $conn = $app2->basex();

            $sql = "insert into producto values(null,'".$p0."','".$p1."','".$p2."','".$p3."','".$p4."','".$p5."','".$p6."','".$p7."','".$p8."')";
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Modificar Productos

        public function productoM($v,$id,$p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            if($v == 1) {
                $sql = "UPDATE producto SET id_categoria='".$p0."',id_marca='".$p1."',nombre='".$p2."',descripcion='".$p3."', 
                precio='".$p4."',cantidad='".$p6."',estado='".$p7."', ganancias='".$p8."' WHERE id_producto = ".$id;
            }
            else{
                $sql = "UPDATE producto SET id_categoria='".$p0."',id_marca='".$p1."',nombre='".$p2."',descripcion='".$p3."', 
                precio='".$p4."', imagen='".$p5."',cantidad='".$p6."',estado='".$p7."', ganancias='".$p8."' WHERE id_producto = ".$id;
            }
            
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Borrar Productos

        public function productoB($id) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            
            $sql = "DELETE FROM producto WHERE id_producto = ".$id;
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0, 'mensaje'=>'¡Eliminado exitosamente!');
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1, 'mensaje'=>'¡Eliminado sin exito!');
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        /*
            ------------------------------------------------------------------
                                        Pedidos
            ------------------------------------------------------------------
        */

        //Leer Pedidos

        public function pedidoL() {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            $sql= "SELECT id_pedido as 'id', pedido.id_cliente as 'idc', cliente.nombre, precio_total as 'precio', fecha, estado from pedido inner join cliente on pedido.id_cliente = cliente.id_cliente ORDER BY pedido.id_pedido";
            
            $result = $conn->query($sql);
            $pro = array();

            while ( $row = $result->fetch_object() ) {
                array_push( $pro, $row );
            }
            $res['pedidos'] = $pro;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        /*
        //Agregar Pedidos
        
        public function pedidoA($p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8) {
            $app2 = new Dashboard();
            $conn = $app2->basex();

            $sql = "insert into producto values(null,'".$p0."','".$p1."','".$p2."','".$p3."','".$p4."','".$p5."','".$p6."','".$p7."','".$p8."')";
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Modificar Pedidos

        public function pedidoM($v,$id,$p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            if($v == 1) {
                $sql = "UPDATE producto SET id_categoria='".$p0."',id_marca='".$p1."',nombre='".$p2."',descripcion='".$p3."', 
                precio='".$p4."',cantidad='".$p6."',estado='".$p7."', ganancias='".$p8."' WHERE id_producto = ".$id;
            }
            else{
                $sql = "UPDATE producto SET id_categoria='".$p0."',id_marca='".$p1."',nombre='".$p2."',descripcion='".$p3."', 
                precio='".$p4."', imagen='".$p5."',cantidad='".$p6."',estado='".$p7."', ganancias='".$p8."' WHERE id_producto = ".$id;
            }
            
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        */
        //Borrar Pedidos

        public function pedidoB($id) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            
            $sql = "DELETE FROM pedido WHERE id_pedido = ".$id;
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0, 'mensaje'=>'¡Eliminado exitosamente!');
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1, 'mensaje'=>'¡Eliminado sin exito!');
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        /*
            ------------------------------------------------------------------
                                        Detalle Pedidos
            ------------------------------------------------------------------
        */

        //Leer Detalle Pedidos

        public function dpedidoL($id) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            $sql= "SELECT id_detallepedido as 'id',producto.nombre, detalle_pedido.cantidad, round(detalle_pedido.cantidad*(producto.precio+(producto.precio*(producto.ganancias/100))),2) as 'total' FROM producto INNER JOIN detalle_pedido on producto.id_producto = detalle_pedido.id_producto INNER JOIN pedido on detalle_pedido.id_pedido = pedido.id_pedido INNER JOIN cliente on pedido.id_cliente= cliente.id_cliente where detalle_pedido.id_pedido = ".$id;
            
            $result = $conn->query($sql);
            $pro = array();

            while ( $row = $result->fetch_object() ) {
                array_push( $pro, $row );
            }
            $res['dpedidos'] = $pro;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        /*
        //Agregar Detalle Pedidos
        
        public function dpedidoA($p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8) {
            $app2 = new Dashboard();
            $conn = $app2->basex();

            $sql = "insert into producto values(null,'".$p0."','".$p1."','".$p2."','".$p3."','".$p4."','".$p5."','".$p6."','".$p7."','".$p8."')";
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }

        //Modificar Detalle Pedidos

        public function dpedidoM($v,$id,$p0,$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            if($v == 1) {
                $sql = "UPDATE producto SET id_categoria='".$p0."',id_marca='".$p1."',nombre='".$p2."',descripcion='".$p3."', 
                precio='".$p4."',cantidad='".$p6."',estado='".$p7."', ganancias='".$p8."' WHERE id_producto = ".$id;
            }
            else{
                $sql = "UPDATE producto SET id_categoria='".$p0."',id_marca='".$p1."',nombre='".$p2."',descripcion='".$p3."', 
                precio='".$p4."', imagen='".$p5."',cantidad='".$p6."',estado='".$p7."', ganancias='".$p8."' WHERE id_producto = ".$id;
            }
            
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0 );
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1 );
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }
        */
        //Borrar Detalle Pedidos

        public function dpedidoB($id) {
            $app2 = new Dashboard();
            $conn = $app2->basex();
            
            $sql = "DELETE FROM detalle_pedido WHERE id_detalle_pedido = ".$id;
            $ingre = array();
            $result = $conn->query($sql);
            if ( $result ) {
                $var = array('val'=>0, 'mensaje'=>'¡Eliminado exitosamente!');
                array_push( $ingre,$var);
            }
            else {
                $var = array('val'=>1, 'mensaje'=>'¡Eliminado sin exito!');
                array_push( $ingre,$var);
            }
            $res['error'] = $ingre;
            header( 'Content-type: application/json' );
            echo json_encode($res);
        }


    }
?>