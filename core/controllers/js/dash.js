var app = new Vue({
    el: '#nav',
    data:{
        u: '',
        c: '',
        pro: '',
        m: '',
        prod: '',
        pedi: '',
        a1: 'active',
        a2: '',
        a3: '',
        a4: '',
        a5: '',
        a6: '',
        pedidos_pendientes: '',
        //Datos session
        usuarix: [],
        //Datos de modelo
        id: 0,
        add: 0,
        error: '',
        //fecha
        ano: new Date().getFullYear(),
        mes: new Date().getMonth(),
        day: new Date().getDate(),
    },
    mounted() {
        this.hide();
        this.getUsu();
    },
    methods: {
        hide(txt='u'){
            document.getElementById("usu").style.display = 'none';
            document.getElementById("client").style.display = 'none';
            document.getElementById("provee").style.display = 'none';
            document.getElementById("myc").style.display = 'none';
            document.getElementById("pro").style.display = 'none';
            document.getElementById("pyd").style.display = 'none';
            switch(txt){
                case 'u':
                    document.getElementById("usu").style.display = '';
                break;
                case 'c':
                    document.getElementById("client").style.display = '';
                break;
                case 'pro':
                    document.getElementById("provee").style.display = '';
                break;
                case 'm':
                    document.getElementById("myc").style.display = '';
                break;
                case 'prod':
                    document.getElementById("pro").style.display = '';
                break;
                case 'pedi':
                    document.getElementById("pyd").style.display = '';
                break;
            }
        },
        cerrar() {
            axios.post('./../../core/controllers/php/indexDash.php?action=cerrar')
                .then(res => {
                    console.log(res)
                    window.location.href = 'index.php'
                })
        },
        esconder: function(dato='') {
            this.a1= '',
            this.a2= '',
            this.a3= '',
            this.a4= '',
            this.a5= '',
            this.a6= '',
            this.u= 'none';
            this.c='none';
            this.pro= 'none';
            this.m='none';
            this.prod= 'none';
            this.pedi='none';
            switch(dato){
                case 'u':
                    this.a1= 'active';
                break;
                case 'c':
                    this.a2= 'active';
                break;
                case 'pro':
                    this.a3= 'active';
                break;
                case 'm':
                    this.a4= 'active';
                break;
                case 'prod':
                    this.a5= 'active';
                break;
                case 'pedi':
                    this.a6= 'active';
                break;
            }
        },
        getUsu() {
            axios.post('./../../core/controllers/php/indexDash.php?action=iniciar')
                .then(res => {
                    console.log(res)
                    this.usuarix = res.data.usuarios
                })
        },
        modUsu(e){
            axios.post('./../../core/controllers/php/dashboard.php?tabla=usuario&action=modificar&id='+this.id, new FormData(e.target))
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.add = 1;
                    this.getUsu();
                }
                else if(vali[0].val == 1){
                    this.add = 2;
                    this.error = "Error al ingresar, verificar correo y/o telefono"
                }
                else if(vali[0].val == 2){
                    this.add = 2;
                    this.error = vali[0].mensaje
                }
            })
        },
    },
});
