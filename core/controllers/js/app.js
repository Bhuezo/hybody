
const contenido = new Vue({
    el: '#cuerpo',
    data: {
        /*
            /////////////////////////////////////////////////////
            || Lista de navegacion para categoria de productos ||
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        */
        productos_exit: true,
        a1: 'active',
        a2: '',
        a3: '',
        a4: '',
        a5: '',
        /*
            /////////////////////////////////////
            || Producto index pagina principal ||
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        */
        filtros: [],
        buscar: '',
        productos: [],
        categorias: [],
        tags: [],
        /*
            ////////////////////
            || caja productos ||
            \\\\\\\\\\\\\\\\\\\\
        */
        cajas: [],
        tcaja: 0,
        t: 0,
        total: 0,

        /*
            ////////////////////
            || Iniciar sesion ||
            \\\\\\\\\\\\\\\\\\\\
        */
        nombre: 'Boris',
        apellido: 'Huezo',
        ok: false,
        sesion: false,
        /*
            ////////////////////
            || Producto modal ||
            \\\\\\\\\\\\\\\\\\\\
        */
        info_pro: [],
        /*
             ///////////////////
             || Cliente Sesión||
             \\\\\\\\\\\\\\\\\\\
        */
        clientes: [],
        error: false,
        //fecha
        ano: new Date().getFullYear(),
        mes: new Date().getMonth(),
        day: new Date().getDate(),

        //Recuperar contraseña
        contraseña: '',
        /*
            //////////////////////////////////
            ||Producto y Comentario Clientes||
            \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
        */
        //Producto
        idp: 0,
        imgp: '',
        nombrep: '',
        descripcionp: '',
        marcap: '',
        comentarios: [],
        calif: 0,
        //Comentario
        comen: true,
        p: false,
        descp: '',
        cali: 'none',
        desc: 'nav-link active',
        cal: 'nav-link',
        //Pagar:
        add: 0,
        error: '',
    },
    mounted() {
        this.getPro()
        this.verificar()
        this.getCate()
    },
    computed: {
    },
    methods: {
        getPro() {
            axios.post('./../../core/controllers/php/productos.php?action=leer')
                .then(res => {
                    console.log(res)
                    this.productos = res.data.productos
                    this.filtros = this.productos
                    this.productos_exit = true
                })
        },
        getCate() {
            axios.post('./../../core/controllers/php/productos.php?action=categoria')
                .then(res => {
                    console.log(res)
                    this.categorias = res.data.categorias
                })
        },
        getCategoria(e) {
            this.filtros = this.productos.filter(function (elem) {
                let el1 = elem.categoria.toLowerCase();
                if (e === '') return true;
                else return el1.indexOf(e.toLowerCase()) > -1;

            })
            if (this.filtros.length <= 0 && e != 'planes') {
                this.productos_exit = false;
            }
            else {
                this.productos_exit = true
            }
            this.buscar = ''
        },
        getBuscar() {
            var comp = this.buscar;
            this.filtros = this.productos.filter(function (elem) {
                let el1 = elem.producto.toLowerCase();
                if (comp === '') return true;
                else return el1.indexOf(comp.toLowerCase()) > -1;

            })
            if (this.filtros.length <= 0) {
                this.productos_exit = false;
            }
            else {
                this.productos_exit = true
            }
            this.buscar = ''
        },
        /* Productos en caja */
        verificarP() {
            axios.post('./../../core/controllers/php/caja.php?action=verificar')
                .then(res => {
                    console.log(res)
                    this.total = 0;
                    this.tcaja = parseInt(0);
                    this.t = 0;
                    if (res.data.cajas != null) {
                        this.cajas = res.data.cajas
                        console.log(this.cajas.length);
                        for (let i = 0; i < this.cajas.length; i++) {
                            this.t += parseFloat(this.cajas[i].precio) * parseFloat(this.cajas[i].cantidad);
                            this.tcaja += parseInt(this.cajas[i].cantidad);
                        }
                        this.total = this.t.toFixed(2);
                    }
                })
        },
        agregar(id,nombre, precio, canti) {
            let r = 0;
            this.tcaja += parseInt(canti);
            this.t += parseFloat(precio) * parseFloat(canti);
            this.total = this.t.toFixed(2);
            for (let i = 0; i < this.cajas.length; i++) {
                if (this.cajas[i].nombre == nombre) {
                    r = 1;
                    this.cajas[i].cantidad = parseInt(this.cajas[i].cantidad) + parseInt(canti);
                    axios.post('./../../core/controllers/php/caja.php?action=sumar&nombre=' + nombre + '&num=' + canti)
                        .then(res => {
                            console.log(res);
                        })
                    break;
                }
                else {
                    r = 0;
                }
            }
            if (r == 0) {
                this.cajas.push({
                    nombre: nombre,
                    precio: precio,
                    cantidad: canti,
                });
                axios.post('./../../core/controllers/php/caja.php?action=agregarCarrito&id='+id+'&nombre=' + nombre +
                    '&precio=' + precio + '&cantidad=' + canti)
                    .then(res => {
                        console.log(res);
                    })
            }


        },
        sumar(nombre) {
            axios.post('./../../core/controllers/php/caja.php?action=sumar&nombre=' + nombre)
                .then(res => {
                    console.log(res);
                })
        },
        resta(nombre) {
            axios.post('./../../core/controllers/php/caja.php?action=resta&nombre=' + nombre)
                .then(res => {
                    console.log(res);
                })
        },
        quitar(produc, precio, canti, id) {
            this.tcaja -= parseInt(canti);
            this.cajas.splice(produc, 1);
            this.t -= parseFloat(precio) * parseFloat(canti);
            this.total = this.t.toFixed(2);
            axios.post('./../../core/controllers/php/caja.php?action=borrar&nombre=' + id)
                .then(res => {
                    console.log(res);
                })
        },
        pago(precio, canti) {
            axios('./../../core/controllers/php/caja.php?action=pagar&precio='+precio+'&cantidad='+canti)
                .then(res => {
                    console.log(res)
                    var vali = res.data.error
                    if(vali[0].val == 0)
                    {
                        this.add = 1;
                        this.error = vali[0].mensaje
                        this.verificarP();
                    }
                    if(vali[0].val == 1)
                    {
                        this.add = 2;
                        this.error = vali[0].mensaje
                    }
                })
        },
        esconder: function (dato = '') {
            this.a1 = ''
            this.a2 = ''
            this.a3 = ''
            this.a4 = ''
            this.a5 = ''
            switch (dato) {
                case 'ini':
                    this.a1 = 'active';
                    this.planes = '';
                    break;
                case 'd':
                    this.a2 = 'active';
                    break;
                case 'a':
                    this.a3 = 'active';
                    break;
                case 't':
                    this.a4 = 'active';
                    break;
                case 'c':
                    this.a5 = 'active';
                    break;
            }
        },
        /*
            CLIENTE SESIÓN
            en este apartado llevara los diferentes controladores
            para la expericencia de inicair sesion o crear una  cuenta el cliente
            o recuperar la contraseña
        */
        //Verifica que ya haya iniciao sesion
        verificar() {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=verificar')
                .then(res => {
                    console.log(res)
                    if (res.data.clientes != null) {
                        this.clientes = res.data.clientes
                        if (this.clientes.length > 0) {

                            this.sesion = true;
                            this.comen = true;

                        } else {
                            this.comen = false;
                        }
                    }
                })
            this.verificarP()
        },
        //Recuperar contraseña
        recuperar(e) {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=recuperar', new FormData(e.target))
                .then(res => {
                    console.log(res)
                    let validar = [];
                    validar = res.data.recuperar
                    if (validar.length > 0) {
                        alert(validar[0].contra)
                        window.location.href = 'index.php';
                    }
                    else {

                    }
                })
        },
        //Iniciar sesion en la pagina por parte del cliente
        iniciarsesion(e) {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=iniciar', new FormData(e.target))
                .then(res => {
                    console.log(res)
                    this.clientes = res.data.clientes
                    if (this.clientes.length > 0) {
                        this.error = false;
                        this.sesion = true;
                        this.comen = true;
                    }
                    else {
                        this.error = true;
                        //Borrar datos con jquery
                        $("#contra1").val('');
                        $("#correo1").val('');
                    }
                })
        },
        //Usarios cerrar sesión
        cerrarsesion() {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=cerrar')
                .then(res => {
                    console.log(res)
                })
            this.sesion = false;
            this.clientes = null
            this.comen = false;
        },
        //Modificar datos del ussuario
        modificarsesion(e) {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=modificar', new FormData(e.target))
                .then(res => {
                    console.log(res)
                })
        },
        //Productos
        mostrarP(num, img, nom, desp, mar) {
            this.idp = num
            this.imgp = img
            this.nombrep = nom
            this.descripcionp = desp
            this.marcap = mar
        },
        //comentarios
        comentario() {
            this.f = this.idp
            var media = 0;
            axios.get('./../../core/controllers/php/comentario.php?producto=' + this.f + '&action=comentario')
                .then(res => {
                    console.log(res)
                    this.comentarios = res.data.comentarios
                    for(let i = 0; i < this.comentarios.length; i++){
                        media += parseInt(this.comentarios[i].calificacion)
                    }
                    media = media / this.comentarios.length
                    this.calif = media.toFixed(1);;
                })
        },
        Ingresarcomentario(e) {
            this.f = this.idp
            axios.post('./../../core/controllers/php/comentario.php?producto=' + this.f + '&action=ingresarc', new FormData(e.target))
                .then(res => {
                    console.log(res)
                    this.comentario()
                })
        },
        //Productos y comentarios
        cambio: function () {
            if (this.p == false) {
                this.descp = '';
                this.cali = 'none';
                this.desc = 'nav-link active';
                this.cal = 'nav-link';
            }
            else if (this.p == true) {
                this.cali = '';
                this.descp = 'none';
                this.desc = 'nav-link';
                this.cal = 'nav-link active';
            }
        }

        /*
              SOBRE NOSOTROS
              este apartado llevara los controladores prar el sobre nosostros
        */

    },
});