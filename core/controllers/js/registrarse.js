var contenido = new Vue({
    el: '#cuerpo',
    data: {
        /*
            ////////////////////
            || caja productos ||
            \\\\\\\\\\\\\\\\\\\\
        */
        cajas: [],
        tcaja: 0,
        t: 0,
        total: 0,

        /*
            ////////////////////
            || Iniciar sesion ||
            \\\\\\\\\\\\\\\\\\\\
        */
        puntos: 0,
        nombre: 'Boris',
        apellido: 'Huezo',
        ok: false,
        sesion: false,
        /*
            ////////////////////
            || Iniciar sesion ||
            \\\\\\\\\\\\\\\\\\\\
        */
        puntos: 0,
        nombre: 'Boris',
        apellido: 'Huezo',
        ok: false,
        sesion: false,
        clientes: [],
        error: false,
        //fecha
        ano: new Date().getFullYear(),
        mes: new Date().getMonth(),
        day: new Date().getDate(),

        //Recuperar contraseña
        contraseña: '',
        //Pasos
        p0: '',
        p1: 'none',
        p2: 'none',
        p3: 'none',
        a1: 'active',
        a2: 'disabled',
        a3: 'disabled',
        a4: 'disabled',
        //ERROR
        error: '',
        //Completar form
        genero: '',
        nombre: '',
        apellido: '',
        fecha: '',
        correo: '',
        contra: '',
        pais: '',
        ciudad: '',
        municipio: '',
        postal: '',
        direccion: '',
        telefono: '',
        reg_error: false,
        //fecha
        ano: new Date().getFullYear(),
        mes: new Date().getMonth(),
        day: new Date().getDate(),
    },
    mounted() {
        this.verificar()
    },
    methods: {
        adelante: function (dato = 0) {
            this.a1 = 'disabled',
                this.a2 = 'disabled',
                this.a3 = 'disabled',
                this.a4 = 'disabled',
                this.p0 = 'none';
            this.p1 = 'none';
            this.p2 = 'none';
            this.p3 = 'none';
            this.error = '';
            switch (dato) {
                case 0:
                    this.p0 = '';
                    this.a1 = 'active';
                    break;
                case 1:
                    this.p1 = '';
                    this.a2 = 'active';
                    break;
                case 2:
                    if (this.nombre != '' && this.apellido != '' && this.fecha != '' && this.correo != '' && this.contra != '') {
                        this.p2 = '';
                        this.a3 = 'active';
                    }
                    else {
                        this.error = 'show';
                        this.p1 = '';
                        this.a2 = 'active';
                    }
                    break;
                case 3:
                    if (this.pais != '' && this.ciudad != '' && this.municipio != '' && this.direccion != '' && this.telefono != '' && this.postal != '') {
                        this.p3 = '';
                        this.a4 = 'active';
                    }
                    else {
                        this.error = 'show';
                        this.p2 = '';
                        this.a3 = 'active';
                    }
                    break;
            }
        },
        atras: function (dato = 0) {
            this.a1 = 'disabled',
                this.a2 = 'disabled',
                this.a3 = 'disabled',
                this.a4 = 'disabled',
                this.p0 = 'none';
            this.p1 = 'none';
            this.p2 = 'none';
            this.p3 = 'none';
            switch (dato) {
                case 0:
                    this.p0 = '';
                    this.a1 = 'active';
                    break;
                case 1:
                    this.p1 = '';
                    this.a2 = 'active';
                    break;
                case 2:
                    this.p2 = '';
                    this.a3 = 'active';
                    break;
                case 3:
                    this.p3 = '';
                    this.a4 = 'active';
                    break;
            }
        },
        /* Productos en caja */
        verificarP() {
            axios.post('./../../core/controllers/php/caja.php?action=verificar')
                .then(res => {
                    console.log(res)
                    this.total = 0;
                    this.tcaja = parseInt(0);
                    this.t = 0;
                    if (res.data.cajas != null) {
                        this.cajas = res.data.cajas
                        console.log(this.cajas.length);
                        for (let i = 0; i < this.cajas.length; i++) {
                            this.t += parseFloat(this.cajas[i].precio) * parseFloat(this.cajas[i].cantidad);
                            this.tcaja += parseInt(this.cajas[i].cantidad);
                        }
                        this.total = this.t.toFixed(2);
                    }
                })
        },
        sumar(nombre) {
            axios.post('./../../core/controllers/php/caja.php?action=sumar&nombre=' + nombre)
                .then(res => {
                    console.log(res);
                })
        },
        resta(nombre) {
            axios.post('./../../core/controllers/php/caja.php?action=resta&nombre=' + nombre)
                .then(res => {
                    console.log(res);
                })
        },
        quitar(produc, precio, canti, id) {
            this.tcaja -= parseInt(canti);
            this.cajas.splice(produc, 1);
            this.t -= parseFloat(precio) * parseFloat(canti);
            this.total = this.t.toFixed(2);
            axios.post('./../../core/controllers/php/caja.php?action=borrar&nombre=' + id)
                .then(res => {
                    console.log(res);
                })
        },
        /*
        CLIENTE SESIÓN
        en este apartado llevara los diferentes controladores
        para la expericencia de inicair sesion o crear una  cuenta el cliente
        o recuperar la contraseña
        */
        //Verifica que ya haya iniciao sesion
        verificar() {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=verificar')
                .then(res => {
                    console.log(res)
                    if (res.data.clientes != null) {
                        this.clientes = res.data.clientes
                        if (this.clientes.length > 0) {

                            this.sesion = true;
                            this.comen = true;

                        } else {
                            this.comen = false;
                        }
                    }
                })
            this.verificarP()
        },
        //Iniciar sesion en la pagina por parte del cliente
        iniciarsesion(e) {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=iniciar', new FormData(e.target))
                .then(res => {
                    console.log(res)
                    this.clientes = res.data.clientes
                    if (this.clientes.length > 0) {
                        this.error = false;
                        this.sesion = true;
                        this.comen = true;
                    }
                    else {
                        this.error = true;
                        //Borrar datos con jquery
                        $("#contra1").val('');
                        $("#correo1").val('');
                    }
                })
        },
        //Usarios cerrar sesión
        cerrarsesion() {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=cerrar')
                .then(res => {
                    console.log(res)
                })
            this.sesion = false;
            this.clientes = null
            this.comen = false;
        },
        //Modificar datos del ussuario
        modificarsesion(e) {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=modificar', new FormData(e.target))
                .then(res => {
                    console.log(res)
                })
        },
        ingresarUsuario(e) {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=ingresarNuevo', new FormData(e.target))
                .then(res => {
                    console.log(res)
                    this.reg_error=true;
                    if(res.data.clientes){
                        this.clientes = res.data.clientes
                        if (this.clientes.length > 0) {
                            this.reg_error=false;
                            this.sesion = true;
                            location.reload();
                        }
                    }
                    else {
                        //Borrar datos con jquery
                    }
                })
        },
    },
});