Vue.component('marca-comp', {
    template: //html
        `
        <div class="col-12 col-md-6 col-lg-6">
            <img :src="img" class="img rounded-circle"
                alt="Upss Hubo un error">
            <h5 class="text-center mt-3">{{nombre}}</h5>
        </div>
    `,
    props: ['nombre', 'img'],
})
const contenido = new Vue({
    el: '#cuerpo',
    data: {
        //Cabecera
        t1: 'Como Funciona?', //Titulo
        p1: '<h3>' +
            'Lorem ipsum, dolor sit consectetur adipisicing elit. Quo perferendis' + //Cabeza
            '</h3>' +
            '<p>' +
            'expedita laborum quaerat minus quisquam molestiae numquam animi deserunt cupiditate, vero' + //Cuerpo
            '</p>' +
            '<ul>' +
            '<li>' +
            'expedita laborum quaerat minus quisquam molestiae numquam animi deserunt cupiditate, vero' + //Complemtento
            ' </li>' +
            '<li>' +
            'expedita laborum quaerat minus quisquam molestiae numquam animi deserunt cupiditate, vero' + //Complemtento
            ' </li>' +
            '</ul>' +
            '<p>' +
            'sunt repellendus debitis in neque ullam iure dolore atque.' + //pie
            '</p>',
        marcas: 'none',
        sobre: '',
        a1: 'active',
        a2: '',
        /*
             ///////////////////
             || Cliente Sesión||
             \\\\\\\\\\\\\\\\\\\
        */
        clientes: [],
        error: false,
        //fecha
        ano: new Date().getFullYear(),
        mes: new Date().getMonth(),
        day: new Date().getDate(),

        //Recuperar contraseña
        contraseña: '',
        /*
            ////////////////////
            || caja productos ||
            \\\\\\\\\\\\\\\\\\\\
        */
        cajas: [],
        tcaja: 0,
        t: 0,
        total: 0,

        /*
            ////////////////////
            || Iniciar sesion ||
            \\\\\\\\\\\\\\\\\\\\
        */
        puntos: 0,
        nombre: 'Boris',
        apellido: 'Huezo',
        ok: false,
        sesion: false,
        //Provedor
        provedor: [],
    },
    mounted() {
        this.getProve()
        this.verificar()
    },
    methods: {
        getProve() {
            axios.post('./../../core/controllers/php/sobre.php?action=leer')
                .then(res => {
                    console.log(res)
                    this.provedor = res.data.proveedor
                })
        },
        esconder: function (dato = '') {
            this.a1 = '',
                this.a2 = '',
                this.marcas = 'none';
            this.sobre = 'none';
            switch (dato) {
                case 'm':
                    this.marcas = '';
                    this.a2 = 'active';
                    break;
                case 's':
                    this.sobre = '';
                    this.a1 = 'active';
                    break;
            }
        },
        /* Productos en caja */
        verificarP() {
            axios.post('./../../core/controllers/php/caja.php?action=verificar')
                .then(res => {
                    console.log(res)
                    this.total = 0;
                    this.tcaja = parseInt(0);
                    this.t = 0;
                    if (res.data.cajas != null) {
                        this.cajas = res.data.cajas
                        console.log(this.cajas.length);
                        for (let i = 0; i < this.cajas.length; i++) {
                            this.t += parseFloat(this.cajas[i].precio) * parseFloat(this.cajas[i].cantidad);
                            this.tcaja += parseInt(this.cajas[i].cantidad);
                        }
                        this.total = this.t.toFixed(2);
                    }
                })
        },
        agregar(nombre, precio, canti) {
            let r = 0;
            this.tcaja += parseInt(canti);
            this.t += parseFloat(precio) * parseFloat(canti);
            this.total = this.t.toFixed(2);
            for (let i = 0; i < this.cajas.length; i++) {
                if (this.cajas[i].nombre == nombre) {
                    r = 1;
                    this.cajas[i].cantidad = parseInt(this.cajas[i].cantidad) + parseInt(canti);
                    axios.post('./../../core/controllers/php/caja.php?action=sumar&nombre=' + nombre + '&num=' + canti)
                        .then(res => {
                            console.log(res);
                        })
                    break;
                }
                else {
                    r = 0;
                }
            }
            if (r == 0) {
                this.cajas.push({
                    nombre: nombre,
                    precio: precio,
                    cantidad: canti,
                });
                axios.post('./../../core/controllers/php/caja.php?action=agregarCarrito&nombre=' + nombre +
                    '&precio=' + precio + '&cantidad=' + canti)
                    .then(res => {
                        console.log(res);
                    })
            }


        },
        sumar(nombre) {
            axios.post('./../../core/controllers/php/caja.php?action=sumar&nombre=' + nombre)
                .then(res => {
                    console.log(res);
                })
        },
        resta(nombre) {
            axios.post('./../../core/controllers/php/caja.php?action=resta&nombre=' + nombre)
                .then(res => {
                    console.log(res);
                })
        },
        quitar(produc, precio, canti, id) {
            this.tcaja -= parseInt(canti);
            this.cajas.splice(produc, 1);
            this.t -= parseFloat(precio) * parseFloat(canti);
            this.total = this.t.toFixed(2);
            axios.post('./../../core/controllers/php/caja.php?action=borrar&nombre=' + id)
                .then(res => {
                    console.log(res);
                })
        },
        /*
            CLIENTE SESIÓN
            en este apartado llevara los diferentes controladores
            para la expericencia de inicair sesion o crear una  cuenta el cliente
            o recuperar la contraseña
        */
        //Verifica que ya haya iniciao sesion
        verificar() {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=verificar')
                .then(res => {
                    console.log(res)
                    if (res.data.clientes != null) {
                        this.clientes = res.data.clientes
                        if (this.clientes.length > 0) {

                            this.sesion = true;
                            this.comen = true;

                        } else {
                            this.comen = false;
                        }
                    }
                })
            this.verificarP()
        },
        //Recuperar contraseña
        recuperar(e) {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=recuperar', new FormData(e.target))
                .then(res => {
                    console.log(res)
                    this.clientes = res.data.clientes
                    if (this.clientes.length > 0) {
                        history.back()
                    }
                    else {

                    }
                })
        },
        //Iniciar sesion en la pagina por parte del cliente
        iniciarsesion(e) {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=iniciar', new FormData(e.target))
                .then(res => {
                    console.log(res)
                    this.clientes = res.data.clientes
                    if (this.clientes.length > 0) {
                        this.error = false;
                        this.sesion = true;
                        this.comen = true;
                    }
                    else {
                        this.error = true;
                        //Borrar datos con jquery
                        $("#contra1").val('');
                        $("#correo1").val('');
                    }
                })
        },
        //Usarios cerrar sesión
        cerrarsesion() {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=cerrar')
                .then(res => {
                    console.log(res)
                })
            this.sesion = false;
            this.clientes = null
            this.comen = false;
        },
        //Modificar datos del ussuario
        modificarsesion(e) {
            axios.post('./../../core/controllers/php/iniciar_sesion.php?action=modificar', new FormData(e.target))
                .then(res => {
                    console.log(res)
                })
        },
    },
});