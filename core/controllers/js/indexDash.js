const app = new Vue({
    el: '#con',
    data:{
        contra: '',
        error: '',
        add: 0,
    },
    methods: {
        sesion(e){
            axios.post('./../../core/controllers/php/indexDash.php?action=verificar', new FormData(e.target))
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.add = 1;
                    window.location.href = "dashboard.php";
                }
                else if(vali[0].val == 1){
                    this.add = 2;
                    this.error = vali[0].mensaje
                }
            })
        },
        olvidarU(e){
            axios.post('./../../core/controllers/php/indexDash.php?action=olvidar&tipo=usu', new FormData(e.target))
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.add = 1;
                    this.error = vali[0].mensaje
                }
                else if(vali[0].val == 1){
                    this.add = 2;
                    this.error = vali[0].mensaje
                }
            })
        },
        olvidarC(e){
            axios.post('./../../core/controllers/php/indexDash.php?action=olvidar&tipo=cli', new FormData(e.target))
            .then(res => {
                console.log(res)
                var vali = res.data.error
                if(vali[0].val == 0)
                {
                    this.add = 1;
                    window.location.href = "index.php";
                }
                else if(vali[0].val == 1){
                    this.add = 2;
                    this.error = vali[0].mensaje
                }
            })
        }
    },
});